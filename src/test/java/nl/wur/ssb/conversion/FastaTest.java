package nl.wur.ssb.conversion;

import junit.framework.TestCase;

/**
 * SAPP Conversion test cases
 */
public class FastaTest extends TestCase {

  public void testFile1() throws Exception {
    String input = "/Users/jasperkoehorst/Desulfoluna_spongiiphila_AA1.fna";
//    String input = "./src/test/resources/input/FASTA/testfile1.fasta";
    String[] args = {"-i", input, "-o",
        "./src/test/resources/output/testfile1.fasta.hdt", "-fasta2rdf", "-codon", "11",
        "-organism", "Streptoccus pyogenes", "-identifier", "TestFile1", "-genome","-contig","-debug", "-format","ttl"};
    App.main(args);
  }

  public void testFile2() throws Exception {
    String[] args = {"-i", "./src/test/resources/input/FASTA/testfile2.fasta", "-o",
        "./src/test/resources/output/testfile2.fasta.hdt", "-fasta2rdf", "-codon", "11",
        "-organism", "Streptococcys pyogenes", "-identifier", "TestFile2", "-gene"};
    App.main(args);
  }

  public void testProteinFastaMycoplasma() throws Exception {
    String[] args = {"-i", "./src/test/resources/input/FASTA/testfile3.fasta", "-o",
        "./src/test/resources/output/testfile3.fasta.hdt", "-fasta2rdf", "-codon", "11",
        "-organism", "Streptococcus pyogenes", "-identifier", "TestFile3", "-protein"};
    App.main(args);
  }

  public void testFunnyGenome() throws Exception {
    String[] args = {"-i", "/Users/jasperkoehorst/Dropbox/EMBL/Desulfoluna_spongiiphila_AA1.fna", "-o",
        "./src/test/resources/output/testfile3.fasta.hdt", "-fasta2rdf", "-codon", "11",
        "-organism", "Streptococcus pyogenes", "-identifier", "TestFile3", "-genome","-contig", "-format","ttl","-debug"};
    App.main(args);
  }
}
