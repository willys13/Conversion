package nl.wur.ssb.conversion;

import java.io.File;
import junit.framework.TestCase;

/**
 * SAPP Conversion test cases
 */

public class ToEmblTest extends TestCase {

  public void testFile4() throws Exception {
    File inputFile = new File("./src/test/resources/output/HDT2EMBL/example.embl");
    inputFile.delete();
    String[] args = {"-i", "./src/test/resources/input/HDT2EMBL/example2.hdt", "-o",
        "./src/test/resources/output/HDT2EMBL/example.embl", "-rdf2embl"};
     App.main(args);
  }
}
