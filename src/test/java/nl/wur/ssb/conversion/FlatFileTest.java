package nl.wur.ssb.conversion;

import java.io.File;
import java.util.Collection;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;

/**
 * Unit test for FlatFile conversion
 */

public class FlatFileTest extends TestCase {

  public void testFile1() throws Exception {
    String[] args = {"-i", "./src/test/resources/input/EMBL/testfile1.txt.gz", "-embl2rdf", "-o",
        "./src/test/resources/output/testfile1.txt.ttl", "-uniqueidentifier", "AFCG01000000",
        "-format", "ttl", "-debug"};
    App.main(args);
  }

  public void testFile1A() throws Exception {
    String[] args = {"-i", "./src/test/resources/input/EMBL/testfile1A.txt.gz", "-embl2rdf", "-o",
        "./src/test/resources/output/testfile1.txt.ttl", "-uniqueidentifier", "AFCG01000000",
        "-format", "ttl", "-debug", "-codon", "1"};
    App.main(args);
  }

  public void testFile2() throws Exception {
    String[] args = {"-i", "./src/test/resources/input/EMBL/testfile2.txt.gz", "-embl2rdf", "-o",
        "./src/test/resources/output/testfile2.txt.ttl", "-uniqueidentifier", "AFCG01000000",
        "-format", "ttl"};
    App.main(args);
  }

  public void testFile3() throws Exception {
    String[] args = {"-i", "./src/test/resources/input/EMBL/testfile3.txt.gz", "-embl2rdf", "-o",
        "./src/test/resources/output/testfile3.txt.ttl", "-uniqueidentifier", "AFCG01000000",
        "-format", "hdt"};
    App.main(args);
  }


  public void testFile4() throws Exception {
    String[] args = {"-i", "./src/test/resources/input/EMBL/testfile4.txt.gz", "-embl2rdf", "-o",
        "./src/test/resources/output/testfile4.txt.ttl", "-uniqueidentifier", "GCA_000007125",
        "-debug", "-format", "ttl"};
    App.main(args);
  }

  public void testFile5() throws Exception {
    // Duplication test
    String[] args = {"-i", "./src/test/resources/input/EMBL/testfile5.txt.gz", "-embl2rdf", "-o",
        "./src/test/resources/output/testfile4.txt.ttl", "-uniqueidentifier", "GCA_000007125",
        "-debug", "-format", "ttl"};
    App.main(args);
  }

  public void testGenbank() throws Exception {
    String[] args = {"-i",
        "./src/test/resources/input/GENBANK/GCA_001007995.1_ASM100799v1_genomic.gbff",
        "-genbank2rdf", "-o", "./src/test/resources/output/GCA_001007995.1_ASM100799v1_genomic.ttl",
        "-uniqueidentifier", "GCA_001007995.1_ASM100799v1", "-debug", "-format", "ttl"};
    App.main(args);
  }

//  public void testRandom() throws Exception {
//    String[] tests = {"GCA_000005845.2.embl.gz","GCA_000006175.2.embl.gz","GCA_000006605.1.embl.gz"};
//    for (String test : tests) {
//      String input =
//          "/Users/jasperkoehorst/GitLab/enaBrowserTools/python3/COMPLETE/GCA_0000/" + test;
//      if (!new File(input + ".testing").exists()) {
//        String[] args = {"-i", input, "-embl2rdf", "-o",
//            input + ".testing", "-uniqueidentifier", "GCA_000092645",
//            "-debug", "-format", "hdt"};
//        App.main(args);
//      }
//    }
//  }

  public void testFolder() throws Exception {
    // Local testing only...
    // File folder =
    // new File("/Users/jasperkoehorst/OneDrive -
    // WageningenUR/Projects/Conversions/Human/");
    File folder =
        new File("/Users/jasperkoehorst/OneDrive - WageningenUR/Projects/SAPP/Genomes");
    // File folder = new File("/something/");
    if (folder.exists()) {
      String[] extensions = null;
      Collection<File> collection = FileUtils.listFiles(folder, extensions, true);
      for (File file : collection) {
        String filename = file.getAbsolutePath();
        if (!file.getName().startsWith(".") && !filename.endsWith("ttl")) {
          System.out.println("RUNNING: " + filename);
          String uniqueidentifier = file.getName().replace("SOMETHING", "");
          if (!new File(filename + ".ttl").exists()) {
            String[] args = {"-i", filename, "-embl2rdf", "-o", filename + ".ttl",
                "-uniqueidentifier", uniqueidentifier,"-format","ttl"}; // "-debug"
            App.main(args);
          }
        } else {
          System.out.println("Skipping..." + filename);
        }
      }
    }
  }
}
