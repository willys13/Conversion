package nl.wur.ssb.conversion;

import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class SparkTest extends TestCase {

  public void testFiles() throws Exception {
    String[] args = {"-i", "/Users/jasperkoehorst/EMBL2/None/AB*.gz", "-spark", "-embl2rdf", "-debug", "-jobs","12"};
    App.main(args);
  }
}
