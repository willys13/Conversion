package nl.wur.ssb.conversion;

import junit.framework.TestCase;

/**
 * SAPP Conversion test cases
 */

public class ToFastaTest extends TestCase {


  public void testFile3() throws Exception {
    String[] args = {"-i", "./src/test/resources/input/HDT/testfile3.txt.hdt", "-rdf2fasta", "-gene",
        "gene.fasta", "-protein", "protein.fasta", "-genome", "genome.fasta", "-trna", "trna.fasta",
        "-rrna", "rrna.fasta"};
    App.main(args);
  }
}
