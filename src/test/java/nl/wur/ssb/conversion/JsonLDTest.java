package nl.wur.ssb.conversion;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import com.github.jsonldjava.core.JsonLdApi;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.core.RDFDataset;
import com.github.jsonldjava.utils.JsonUtils;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;


public class JsonLDTest
{
  @Test
  public void testJsonLD() throws Exception {  
    
    String[] args = {"-i", "./src/test/resources/input/EMBL/testfile1.txt.gz", "-embl2rdf", "-o",
        "./src/test/resources/output/testfile1.txt.ttl", "-uniqueidentifier", "AFCG01000000",
        "-format", "ttl", "-debug"};
    App.main(args);
    
    args = new String[]{"-o", "./src/test/resources/output/testfile1.json", "-rdf2json", "-i",
        "./src/test/resources/output/testfile1.txt.ttl", "-debug"};
    App.main(args);
    
    args = new String[]{"-o", "./src/test/resources/output/testfile1.yaml", "-rdf2yaml", "-i",
        "./src/test/resources/output/testfile1.txt.ttl", "-debug"};
    App.main(args);
    
    args = new String[]{"-i", "./src/test/resources/output/testfile1.yaml", "-yaml2rdf", "-o",
        "./src/test/resources/output/testfile1_2.txt.ttl", "-debug"};
    App.main(args);
    
    RDFSimpleCon con1 = new RDFSimpleCon("file://./src/test/resources/output/testfile1.txt.ttl");
    RDFSimpleCon con2 = new RDFSimpleCon("file://./src/test/resources/output/testfile1_2.txt.ttl");
    
    con1.getModel().remove(con2.getModel().listStatements());
    con1.save("./src/test/resources/output/testfile1_diff.txt.ttl");
  }
  
  private void fixNullAndSequence(HashMap<String,Object> obj,boolean forward)
  {
    for(String key : new LinkedList<String>(obj.keySet()))
    {
      Object val = obj.get(key);
      if(val instanceof HashMap<?,?>)
        fixNullAndSequence((HashMap<String,Object>)val,forward);
      else if(val instanceof List<?>)
        fixNullAndSequence((List<?>)val,forward);
      else if(val == null || (val instanceof String && ((String)val).trim().equals("null")))
        obj.remove(key);
      else if(key.equals("sequence") && val instanceof String)
      {
        String sequence = (String)val;
        if(forward)
        {          
          if(sequence.length() > 80)
          {
            StringBuilder builder = new StringBuilder();
            for(int i = 0;i < sequence.length();)
            {
              int begin = i;
              i = Math.min(i += 80,sequence.length());
              builder.append(sequence.substring(begin,i));
              builder.append('\n');
            }
            obj.put(key,builder.toString());
          }
        }
        else
        {
          obj.put(key,sequence.replaceAll("\n",""));
        }
      }
    }
  }
  private void fixNullAndSequence(List<?> obj,boolean forward)
  {
    for(Object item : obj)
    {
      if(item instanceof HashMap<?,?>)
        fixNullAndSequence((HashMap<String,Object>)item,forward);
      else if(item instanceof List<?>)
        fixNullAndSequence((List<?>)item,forward);
    }
  }
  
}
