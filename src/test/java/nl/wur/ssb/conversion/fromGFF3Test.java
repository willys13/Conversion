package nl.wur.ssb.conversion;

import java.io.File;

import junit.framework.TestCase;

/**
 * SAPP Conversion test cases
 */

public class fromGFF3Test extends TestCase {

  public void testAspergillus() throws Exception {

    String turtle = "./src/test/resources/output/Saccharomyces_cerevisiae.ttl";
    String fasta =
        "./src/test/resources/input/GFF3/Saccharomyces_cerevisiae.R64-1-1.dna.chromosome.I.fa";
    String gff3 =
        "./src/test/resources/input/GFF3/Saccharomyces_cerevisiae.R64-1-1.35.chromosome.I.gff3";
    String[] args = {"-f", fasta, "-gff2rdf", "-i", gff3, "-o", turtle, "-id",
        "Saccharomyces_cerevisiae.R64-1-1.35", "-debug", "-codon", "1", "-topology", "linear",
        "-format", "ttl"};
    App.main(args);
  }

//  public void testNannochloropsisGaditana() throws Exception {
//    String turtle = "./src/test/resources/output/NannochloropsisGaditana.ttl";
//
//    if (new File(turtle).exists())
//      new File(turtle).delete();
//
//    String fasta =
//        "./src/test/resources/input/GFF3/naga_genome.fsa";
//    String gff3 =
//        "./src/test/resources/input/GFF3/naga_annot.gff";
//    String[] args = {"-f", fasta, "-gff2rdf", "-i", gff3, "-o", turtle, "-id",
//        "naga", "-debug", "-codon", "1", "-topology", "linear",
//        "-format", "ttl"};
//    App.main(args);
//  }
//
//  public void testAugustus() throws Exception {
//
//    String turtle = "./src/test/resources/output/GFF3Augustus.ttl";
//    String fasta = "./src/test/resources/input/GFF3Augustus/testfile3.hdt.fasta";
//    String gff3 = "./src/test/resources/input/GFF3Augustus/testfile3.hdt.gff3";
//
//    String[] args = {"-f", fasta, "-gff2rdf", "-i", gff3, "-o", turtle, "-id",
//        "AFCG01000000", "-debug", "-codon", "1", "-topology", "linear",
//        "-format", "ttl"};
//    App.main(args);
//  }

  public void testSalmonOrdering() throws Exception {
    String turtle = "./src/test/resources/output/Salmon.ttl";
    String fasta =
        "/Users/jasperkoehorst/OneDrive - WageningenUR/Projects/Digisal/SalmonGenome/GCF_000233375.1_ICSASG_v2_genomic.fna"; // test.fna

    String gff3 =
        "/Users/jasperkoehorst/OneDrive - WageningenUR/Projects/Digisal/SalmonGenome/GCF_000233375.1_ICSASG_v2_genomic.fna"; // test.fna

    String[] args =
        {"-f", fasta, "-gff2rdf", "-i", gff3, "-o", turtle, "-id", "GCF_000233375.1_ICSASG_v2",
            "-debug", "-codon", "1", "-topology", "linear", "-format", "ttl"};

    // Preventing the continous runner to crash
    // if (new File(gff3).exists())
    // App.main(args);
  }
}
