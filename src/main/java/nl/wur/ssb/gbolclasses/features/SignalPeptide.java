package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class SignalPeptide<T extends life.gbol.domain.SignalPeptide> extends ProteinFeature<T> {

  public SignalPeptide(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * If known specify target, use a 'cellelar component' from the GO terms ontology
   * 
   * signalTarget IRI
   */

  public void handleSignalTarget(String value) {
    // TODO ? GO uri listing
    throw new RuntimeException(
        "handleSignalTarget for Feature not done, jesse's fancy lookup function here");

  }
}
