package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MiscFeature<T extends life.gbol.domain.MiscFeature> extends NAFeature<T> {

  public MiscFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
