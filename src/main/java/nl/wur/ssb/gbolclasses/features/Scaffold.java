package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.gbolclasses.sequences.UncompleteNASequence;

public class Scaffold<T extends life.gbol.domain.UncompleteNASequence>
    extends UncompleteNASequence<T> {

  public Scaffold(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}

