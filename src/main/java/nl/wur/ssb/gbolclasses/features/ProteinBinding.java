package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ProteinBinding<T extends life.gbol.domain.ProteinBinding>
    extends BiologicalRecognizedRegion<T> {

  public ProteinBinding(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}