package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class GeneralFeature<T extends life.gbol.domain.GeneralFeature> extends Feature<T> {

  public GeneralFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
