package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class AssemblyGap<T extends life.gbol.domain.AssemblyGap> extends AssemblyAnnotation<T> {
  public AssemblyGap(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  public void handleEstimatedLength(String value) {
    //Ignored...
    //feature.setEstimatedLength(Integer.parseInt(value));
  }
  }
