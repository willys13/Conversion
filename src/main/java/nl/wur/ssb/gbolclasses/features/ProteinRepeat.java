package nl.wur.ssb.gbolclasses.features;

import life.gbol.domain.Region;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ProteinRepeat<T extends life.gbol.domain.ProteinRepeat> extends ProteinFeature<T> {

  public ProteinRepeat(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * Base range of the sequence that constitutes a repeated sequence
   * 
   * rptUnitRange @:Region?;
   */

  public void handleRptUnitRange(String value) {
    //TODO ? RptUnitRange
    Region region = null;
    feature.setRptUnitRange(region);
  }

  /**
   * Pattern of the repeat. For example (NAG)8(WN)2
   * 
   * rptUnitSeq xsd:String?
   */
  public void handleRptUnitSeq(String value) {
    feature.setRptUnitSeq(value);
  }
}

