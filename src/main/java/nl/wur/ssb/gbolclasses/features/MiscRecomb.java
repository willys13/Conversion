package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MiscRecomb<T extends life.gbol.domain.MiscRecomb>
    extends BiologicalRecognizedRegion<T> {
  public MiscRecomb(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
