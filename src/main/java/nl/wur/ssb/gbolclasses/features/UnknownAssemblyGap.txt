package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import org.apache.commons.lang.StringUtils;

public class UnknownAssemblyGap<T extends life.gbol.domain.UnknownAssemblyGap> extends Gap<T> {

  public UnknownAssemblyGap(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  public void handleEstimatedLength(String value) {
    if (StringUtils.isNumeric(value))
      feature.setEstimatedLength(Integer.parseInt(value));
  }

}
