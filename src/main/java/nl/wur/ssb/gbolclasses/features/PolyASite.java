package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class PolyASite<T extends life.gbol.domain.PolyASite>
    extends TranscriptFeature<T> {

  public PolyASite(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
