package nl.wur.ssb.gbolclasses.features;

import life.gbol.domain.RegulatoryClass;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;


public class RegulationSite<T extends life.gbol.domain.RegulationSite>
    extends BiologicalRecognizedRegion<T> {

  public RegulationSite(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * The type of regulation
   * 
   * regulatoryClass type::RegulatoryClass?;"@en
   * @throws Exception 
   */
  
  public void handleRegulatoryClass(String value) throws Exception {
    RegulatoryClass regClass = (RegulatoryClass ) GBOLUtil.getEnum(life.gbol.domain.RegulatoryClass.class, value);
    feature.setRegulatoryClass(regClass);
  }   
}
