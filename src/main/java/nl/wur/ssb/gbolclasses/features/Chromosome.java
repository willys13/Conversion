package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.gbolclasses.sequences.CompleteNASequence;

public class Chromosome<T extends life.gbol.domain.CompleteNASequence>
    extends CompleteNASequence<T> {

  public Chromosome(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}

