package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

/**
 * Created by jasperk on 05/04/2017.
 */
public class GenomicFeature<T extends life.gbol.domain.GenomicFeature> extends NAFeature<T> {

  public GenomicFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  public void handleOperon(String value) {
    //TODO ! operon creation  
    life.gbol.domain.Operon operon = parent.getDomain().make(life.gbol.domain.Operon.class, this.feature.getResource().getURI() + "/operon");
    feature.setOperon(operon);
    feature.setFunction(value);
  }
}
