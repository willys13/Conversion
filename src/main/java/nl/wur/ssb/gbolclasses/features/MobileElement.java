package nl.wur.ssb.gbolclasses.features;

import life.gbol.domain.MobileElementType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MobileElement<T extends life.gbol.domain.MobileElement> extends RepeatFeature<T> {

  public MobileElement(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * Mobile element type
   * 
   * mobileElementType type::MobileElementType;
   * 
   * @throws Exception
   */
  public void handleMobileElementType(String value) throws Exception {
    String type = value;
    String name = "unknown";
    if (value.contains(":")) {
      type = value.split(":")[0];
      name = value.split(":")[1];
    }
    feature
        .setMobileElementType((MobileElementType) GBOLUtil.getEnum(MobileElementType.class, type));
    feature.setMobileElementName(name);
  }

  /**
   * The name of the mobile element
   * 
   * mobileElementName xsd:String;
   */

  public void handleMobileElementName(String value) {
    feature.setMobileElementName(value);
  }
}
