package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class AssemblyAnnotation<T extends life.gbol.domain.AssemblyAnnotation>
    extends SequenceAnnotation<T> {

  public AssemblyAnnotation(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

}
