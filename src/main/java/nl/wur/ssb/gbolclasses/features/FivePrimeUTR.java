package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class FivePrimeUTR<T extends life.gbol.domain.FivePrimeUTR> extends TranscriptFeature<T> {

  public FivePrimeUTR(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
