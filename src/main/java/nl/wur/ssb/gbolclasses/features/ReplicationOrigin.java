package nl.wur.ssb.gbolclasses.features;

import life.gbol.domain.Direction;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ReplicationOrigin<T extends life.gbol.domain.ReplicationOrigin>
    extends BiologicalRecognizedRegion<T> {

  public ReplicationOrigin(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * The direction of the replication can be forward, reverse or both
   * 
   * replicationDirection type::Direction?;
   * @throws Exception 
   */

  public void handleReplicationDirection(String value) throws Exception {
    feature.setReplicationDirection((Direction) GBOLUtil.getEnum(Direction.class, value));
  }

  public void handleDirection(String value) throws Exception {
    handleReplicationDirection(value);
  }
}
