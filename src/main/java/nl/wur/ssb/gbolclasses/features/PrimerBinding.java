package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class PrimerBinding<T extends life.gbol.domain.PrimerBinding>
    extends ArtificialRecognizedRegion<T> {

  public PrimerBinding(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * A textual description of the PCR conditions needed for this primer
   * 
   * PCRConditions xsd:String?
   */
  public void handlePCRConditions(String value) {
    feature.setPCRConditions(value);
  }
}
