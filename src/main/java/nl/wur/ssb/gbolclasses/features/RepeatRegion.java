package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class RepeatRegion<T extends life.gbol.domain.RepeatRegion> extends RepeatFeature<T> {

  public RepeatRegion(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

 
  public void handleMobileElement(String value) {
    //TODO ? what to do with mobile element
    //this.delayedProps.put("mobile_element", value);
  }

}
