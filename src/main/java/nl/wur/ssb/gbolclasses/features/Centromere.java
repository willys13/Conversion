package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Centromere<T extends life.gbol.domain.Centromere> extends RepeatFeature<T> {
  
  public Centromere(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
