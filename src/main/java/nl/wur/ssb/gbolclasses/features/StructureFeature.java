package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.App;

public class StructureFeature<T extends life.gbol.domain.StructureFeature>
    extends GenomicFeature<T> {

  public StructureFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);

  }

  public void handleGene(String value) {
    App.logger.warn("invalid gene tag on " + this.getClass().getName() + ", converting it to accession string");
    feature.addAccession(value);
  }

}
