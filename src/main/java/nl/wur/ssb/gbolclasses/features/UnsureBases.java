package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class UnsureBases<T extends life.gbol.domain.UnsureBases> extends AssemblyAnnotation<T> {

  public UnsureBases(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

}
