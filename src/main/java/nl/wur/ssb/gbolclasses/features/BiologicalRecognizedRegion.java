package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class BiologicalRecognizedRegion<T extends life.gbol.domain.BiologicalRecognizedRegion>
    extends RecognizedRegion<T> {

  public BiologicalRecognizedRegion(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }


  /**
   * Textual description of the moiety recognizing the region
   */
  public void handleBoundMoiety(String value) {
    feature.setBoundMoiety(value);
  }
}
