package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Telomere<T extends life.gbol.domain.Source>
    extends Feature<T> {
  public Telomere(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
