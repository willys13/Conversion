package nl.wur.ssb.gbolclasses.features;

import life.gbol.domain.Region;
import life.gbol.domain.RepeatType;
import life.gbol.domain.Satellite;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.App;

public class RepeatFeature<T extends life.gbol.domain.RepeatFeature> extends GenomicFeature<T> {

  public RepeatFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }


  /**
   * The familiy of the repeat
   * 
   * rptFamily xsd:String?;
   */

  public void handleRptFamily(String value) {
    feature.setRptFamily(value);
  }

  /**
   * The type of the repeat
   * 
   * rptType type::RepeatType?;
   * @throws Exception 
   */

  public void handleRptType(String value) throws Exception {
    feature.setRptType((RepeatType) GBOLUtil.getEnum(RepeatType.class, value));
  }
  
  /**
   * Base range of the sequence that constitutes a repeated sequence
   * 
   * rptUnitRange @:Region?;
   */

  public void handleRptUnitRange(String value) {
     String pos = value.replaceAll("([0-9]+..[0-9]+).*", "$1");
     int begin = Integer.parseInt(pos.split("\\.\\.")[0]);
     int end = Integer.parseInt(pos.split("\\.\\.")[1]);
     Region region = this.parent.makeRegion(begin, end, "translexcept", feature);
     feature.setRptUnitRange(region);
  }
  
  /**
   * Pattern of the repeat. For example (ATCGA)8(ATG)2
   * 
   * rptUnitSeq xsd:String?;
   * 
   */

  public void handleRptUnitSeq(String value) {
    feature.setRptUnitSeq(value);
  }
  /**
   * If repeat contains on or more satellites specify with this property the types of the satellites
   * 
   * satellite @:Satellite*;"
   * @throws Exception 
   */

  public void handleSattelite(String value) throws Exception {
    Satellite sattelite = (Satellite ) GBOLUtil.getEnum(life.gbol.domain.Satellite.class, value);
    feature.addSatellite(sattelite);
  }
  
  public void handleGene(String value) {
    App.logger.warn("invalid gene tag on " + this.getClass().getName() + ", converting it to accession string");
    feature.addAccession(value);
  }
}
