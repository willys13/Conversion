package nl.wur.ssb.gbolclasses.features;

import life.gbol.domain.AnnotationLinkSet;
import life.gbol.domain.AnnotationResult;
import life.gbol.domain.Note;
import life.gbol.domain.Provenance;
import life.gbol.domain.ReasonArtificialLocation;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.App;
import nl.wur.ssb.gbolclasses.Thing;

public class Feature<T extends life.gbol.domain.Feature> extends Thing {
  protected T feature;

  public Feature(SequenceBuilder domain, T feature) {
    super(domain);
    this.feature = feature;
  }

  /**
   * Feature specific things location @:Location;
   */

  /**
   * Any publication associated to feature anotation (numbered in Genbank)
   * 
   * citation @bibo:Document*;
   */

  /**
   * Short description of the function
   * 
   * function xsd:String?;
   */

  public void handleFunction(String value) {
    feature.setFunction(value);
  }

  /**
   * 
   * Provenance associated the feature
   * 
   * provenance @:FeatureProvenance*;
   * 
   * Descriptive functionally link of the feature to external resource
   * 
   * xref @:XRef*;
   * 
   * Notes on the feature
   * 
   * note @:Note*;
   */

  public void handleNote(String value) {
    // TODO AnnotationResult rootURI not present... Same as Sequence.java Note
    Note note = this.parent.getDomain().make(life.gbol.domain.Note.class, feature.getResource().getURI() + "/note" );
    note.setText(value);
    Provenance noteProv = this.parent.getDomain().make(Provenance.class, note.getResource().getURI() +"/noteprov");
    AnnotationResult annotResult = this.parent.getDomain().make(AnnotationLinkSet.class, note.getResource().getURI() +"/noteprov/" + "AnnotationResult");
    noteProv.setOrigin(annotResult);

    note.setProvenance(noteProv);
    feature.addNote(note);
  }

  public void handleDescription(String value) {
    handleNote(value);
  }

/*
   * 
   * Indicate that the location is artificial because of a heterogeous population for low quality*
   * sequence region**
   * 
   * artificialLocation type::ReasonArtificialLocation?;
   */
  public void handleArtificialLocation(String value) throws Exception {
    ReasonArtificialLocation art = (ReasonArtificialLocation ) GBOLUtil.getEnum(life.gbol.domain.ReasonArtificialLocation.class, value);
    feature.setArtificialLocation(art);
  }
  
  /*
   * Associated phenotipic property
   * 
   * phenotype xsd:String?;
   */
  public void handlePhenotype(String value) {
    feature.setPhenotype(value);
  }

  //--------------
  
  /*
   * Captured exceptions...
   */  
  public void handleTransSplicing(String value) {
    /*
     * Ignored 
     * Removed in GBOL, transcript are consistently stored as a list of exons
     */    
  }
  //-
  
  public void handleDelayedCodonStart(String value) {
    //TODO
  }
  

  
  public void handleCitation(String value) throws Exception {
    int num = Integer.parseInt(value.replaceAll("\\[|\\]",""));
    life.gbol.domain.Citation ref = this.parent.getRef(num);
    if(ref != null)
      this.feature.addCitation(ref);
    else
      App.logger.warn("ref with reference number " + num + " not found");
  }
 

  
  public void handleName(String value) {
    feature.addAccession(value);
  }
  
  public void handleId(String value) {
    feature.addAccession(value.replaceAll("^[gene|transcript]:", ""));
  }
}


