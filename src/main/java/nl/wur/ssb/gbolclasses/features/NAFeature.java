package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class NAFeature<T extends life.gbol.domain.NAFeature> extends Feature<T> {

  public NAFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }


  /**
   * Chromosomal mapping position of the feature
   * 
   * map xsd:String?;"
   */

  public void handleMap(String value) {
    feature.setMap(value);
  }
 
}
