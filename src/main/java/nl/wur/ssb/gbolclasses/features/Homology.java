package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.App;

public class Homology<T extends life.gbol.domain.Homology> extends GenomicFeature<T> {

  public Homology(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  public void handleHomology(String value) {
    //TODO ! homology
    throw new RuntimeException(
        "handleHomology for Feature not done, jesse's fancy lookup function here");
  }
  
  public void handleGene(String value) {
    App.logger.warn("invalid gene tag on " + this.getClass().getName() + ", converting it to accession string");
    feature.addAccession(value);
  }
}
