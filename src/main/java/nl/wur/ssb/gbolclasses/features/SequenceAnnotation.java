package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class SequenceAnnotation<T extends life.gbol.domain.SequenceAnnotation>
    extends NAFeature<T> {

  public SequenceAnnotation(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
