package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MiscVariation<T extends life.gbol.domain.MiscVariation> extends VariationFeature<T> {


  public MiscVariation(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
  // #misc_difference old_sequence unsure variation
  // replace xsd:String?;
  // public void handleReplace(String value) {
  // miscd.setReplace(value);
  // }

  // compare xsd:String?;
  // public void handleCompare(String value) {
  // System.err.println("Miscvariation does not support compare?");
  // }
}

