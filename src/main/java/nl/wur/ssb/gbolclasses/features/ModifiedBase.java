package nl.wur.ssb.gbolclasses.features;

import life.gbol.domain.BaseType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ModifiedBase<T extends life.gbol.domain.ModifiedBase> extends SequenceAnnotation<T> {

  public ModifiedBase(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * modBase
   * 
   * type::BaseType;
   * 
   * @throws Exception
   */

  public void handleModBase(String value) throws Exception {
    BaseType baseType = (BaseType) GBOLUtil.getEnum(life.gbol.domain.BaseType.class, value);
    feature.setModBase(baseType);
  }
}
