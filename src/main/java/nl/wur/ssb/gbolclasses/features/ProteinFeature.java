package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ProteinFeature<T extends life.gbol.domain.ProteinFeature> extends Feature<T> {

  public ProteinFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
