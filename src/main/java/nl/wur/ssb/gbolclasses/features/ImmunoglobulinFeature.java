package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ImmunoglobulinFeature<T extends life.gbol.domain.ImmunoglobulinFeature>
    extends ProteinFeature<T> {

  public ImmunoglobulinFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
