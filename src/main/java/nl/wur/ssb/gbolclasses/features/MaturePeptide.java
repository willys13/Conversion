package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MaturePeptide<T extends life.gbol.domain.MaturePeptide> extends ProteinFeature<T> {

  public MaturePeptide(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
}
