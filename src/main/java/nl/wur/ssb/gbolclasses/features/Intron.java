package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Intron<T extends life.gbol.domain.Intron> extends TranscriptionElement<T> {

  public Intron(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
  
  public life.gbol.domain.Gene getGene() throws Exception
  {
    ResultLine res = this.parent.getDomain().getRDFSimpleCon().runQuerySingleRes("getGeneForIntron.sparql",false,this.feature.getResource().getURI());
    return this.parent.getDomain().make(life.gbol.domain.Gene.class,res.getIRI("gene"));
  }
}
