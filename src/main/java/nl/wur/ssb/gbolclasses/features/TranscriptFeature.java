package nl.wur.ssb.gbolclasses.features;

import java.util.Iterator;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.App;

public class TranscriptFeature<T extends life.gbol.domain.TranscriptFeature> extends NAFeature<T> {

  public TranscriptFeature(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
 
  public void handleOperon(String value) throws Exception {
    //TODO implement operon
    //this.delayedProps.put("handleOperon", value);
  }
  
  public life.gbol.domain.Gene getGene() throws Exception
  {
    // Allows multiple results but throws warnings when multiple hits are detected...
    
    Iterable<ResultLine> results = this.parent.getDomain().getRDFSimpleCon()
        .runQuery("getGeneForTransFeat.sparql", false, this.feature.getResource().getURI());
    
    Iterator<ResultLine> itt = results.iterator();

    ResultLine result = itt.next();
    if (itt.hasNext()) {
      App.logger.warn("More than one gene detected returning the first one...\nMost likely an mRNA has been assigned to two different genes");
    }
    return this.parent.getDomain().make(life.gbol.domain.Gene.class,result.getIRI("gene"));

  }
}
