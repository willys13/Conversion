package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class Exon<T extends life.gbol.domain.Exon> extends TranscriptionElement<T> {

  public Exon(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }
  
  public void handleExonId(String value) {
    feature.addAccession(value);
  }

  public life.gbol.domain.Gene getGene() throws Exception
  {
    ResultLine res = this.parent.getDomain().getRDFSimpleCon().runQuerySingleRes("getGeneForExon.sparql",false,this.feature.getResource().getURI());
    return this.parent.getDomain().make(life.gbol.domain.Gene.class,res.getIRI("gene"));
  }

}
