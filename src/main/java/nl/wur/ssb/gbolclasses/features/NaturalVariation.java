package nl.wur.ssb.gbolclasses.features;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class NaturalVariation<T extends life.gbol.domain.NaturalVariation>
    extends VariationFeature<T> {

  public NaturalVariation(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * The frequency of this variation within the population
   * 
   * frequency xsd:Double?
   */

  public void handleFrequency(String value) {
    feature.setFrequency(Double.parseDouble(value));
  }


  // public void handleReplace(String value) {
  // System.err.println("Replace type not supported for variation: " + value);
  // }

  // public void handleCompare(String value) {
  // System.err.println("Compare type not supported for variation: " + value);
  // }

}

