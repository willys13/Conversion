package nl.wur.ssb.gbolclasses;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import life.gbol.domain.Gene;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.App;
import org.apache.commons.lang.StringUtils;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.qualifier.Qualifier;

public class Thing {
  // All very generic things reside here.. Such as Xrefs or things that are in SequenceObject /
  // Feature... however need to find a way to get around the types...?
  protected SequenceBuilder parent;
  private HashMap<String, LinkedList<String>> delayedProps = new HashMap<String, LinkedList<String>>();

  public Thing(SequenceBuilder domain) {
    this.parent = domain;
  }

  public void parseQualifiers(Feature feature) throws Exception {
    if (feature != null) {
      for (Qualifier qual : feature.getQualifiers()) {
        String parts[] = qual.getName().toLowerCase().replace("-", "_").split("_");
        for(int i = 0;i< parts.length;i++)
          parts[i] = StringUtils.capitalize(parts[i]);
        String funcName = "handle" + StringUtils.capitalize(StringUtils.join(parts));
        String value = qual.getValue();
        mapQualifiers(funcName, value);
      }
    }
  }
  
  protected void delayProp(String name,String value)
  {
    LinkedList<String> list = this.delayedProps.get(name);
    if(list == null)
    {
      list = new LinkedList<String>();
      this.delayedProps.put(name,list);
    }
    list.add(value);
  }
  
  public boolean hasDelayedProps()
  {
    return this.delayedProps.size() != 0;
  }
  
  public void handleDelayedProps() throws Exception
  {
    HashMap<String, LinkedList<String>> delayedProps = this.delayedProps;
    this.delayedProps = null;
    for (String qual : delayedProps.keySet()) {
      String funcName = "handleDelayed" + qual;
      for(String value : delayedProps.get(qual))
        mapQualifiers(funcName, value);
    }  
  }

  public void mapQualifiers(String funcName, String value) throws Exception {
    @SuppressWarnings("rawtypes")
    Class walker = this.getClass();

    while (walker != null) {
      Method method = null;
      try {
        method = walker.getMethod(funcName, String.class);
        method.invoke(this, value);
        return;
      } catch (NoSuchMethodException e) {
      } catch (java.lang.reflect.InvocationTargetException e) {
        App.logger.error("Method failure: " + method);
        App.logger.error("InvocationTargetException: " + e);
        App.logger.error("Cause: " + e.getCause());
        throw new Exception(e);
      }
      walker = walker.getSuperclass();
    }
    String output = "qualifier: " + funcName + " not supported for class: " + this.getClass().getName() + "\n";
    output = output + "FAILED: " + funcName + "\n";
    output = output + "FAILED: " + walker + "\n";
    output = output + "FAILED: " + value + "\n";
        throw new Exception(output);
//    try {
//      System.err.println("Throwing exceptions: \n" + output);
//      Files.write(Paths.get("exceptions_by_thing.txt"), output.getBytes(), StandardOpenOption.APPEND);
//    }catch (IOException e) {
//      exception handling left as an exercise for the reader
//  }
  }
  
  public life.gbol.domain.NASequence getRootSequence() throws Exception
  {
    App.logger.warn("Cannot find associated root seq for: " + this.getClass().getName());
    return null;
  }
  
  public life.gbol.domain.Gene getGene() throws Exception
  {
    App.logger.warn("Cannot find associated gene for: " + this.getClass().getName());
    return null;
  }
  
  /*public List<life.gbol.domain.Transcript> getTranscripts()
  {
    return new LinkedList();
  }*/
  
  public List<life.gbol.domain.CDS> getAllCDS() throws Exception
  {
    life.gbol.domain.Gene gene = this.getGene();
    LinkedList<life.gbol.domain.CDS> toRet = new LinkedList<life.gbol.domain.CDS>();
    if(gene == null)
      return toRet;
    
    for(life.gbol.domain.Transcript transcript : gene.getAllTranscript())
    {
      for(life.gbol.domain.NAFeature feature : transcript.getAllFeature())
      {
        if(feature instanceof life.gbol.domain.CDS)
          toRet.add((life.gbol.domain.CDS)feature);
      }  
    }  
    return toRet;
  }

  public void handleBiotype(String value) {}

  public void handleLogicName(String value) {}

  public void handleConstitutive(String value) {}

  public void handleEnsemblPhase(String value) {}

  public void handleEnsemblEndPhase(String value) {}

  public void handleRank(String value) {}

  public void handleParent(String value) {}

  public void handleTranscriptId(String value) {}

  public void handleCollection_date(String value) {
    // TODO !! handleCollection_date
//     Sample.handleCollection_date(value);
  }
  
 
  public void handleExperiment(String value) {
    //TODO ! fix handle experiment
  }
  
  public void handleInference(String value) {
    //TODO ! fix handle inference
  }
  
  public void handleProduct(String value) {
    this.delayProp("Product",value);
  }
  public void handleDelayedProduct(String value) throws Exception {
    for(life.gbol.domain.CDS cds : this.getAllCDS())
    {
      if(cds.getProduct() != null && !cds.getProduct().equals(value))
        App.logger.warn("product feature not same on all related features: " + cds.getProduct() + " <-> " + value);
      cds.setProduct(value);
    }
  }
  
  /**
   * If this gene is a pseudo gene, indicate the type
   * 
   * pseudogene type::PseudoGeneType?;
   * 
   * @throws Exception
   */
  public void handlePseudogene(String value) throws Exception {
    this.delayProp("Pseudogene", value);
  }
    
  public void handleDelayedPseudogene(String value) throws Exception {
    life.gbol.domain.Gene gene = this.getGene();
    if(gene != null)
      gene.setPseudogene((life.gbol.domain.PseudoGeneType)GBOLUtil.getEnum(life.gbol.domain.PseudoGeneType.class,value));
  }

  /*
   * Use pseudogene with type unknown
   */
  public void handlePseudo(String value) {
    this.delayProp("Pseudo", value);
  }
  
  public void handleDelayedPseudo(String value) throws Exception {
    life.gbol.domain.Gene gene = this.getGene();
    if(gene != null)
      gene.setPseudogene(life.gbol.domain.PseudoGeneType.PseudoGeneTypeUnknown);
  }

  public void handleDelayedStandardName(String value) throws Exception {
    life.gbol.domain.Gene gene = this.getGene();
    if (gene != null)
      gene.setStandardName(value);
  }
  /**
   * List of names of known allele variants of the gene
   * 
   * allele xsd:String*;
   */
  public void handleAllele(String value) {
    this.delayProp("Allele", value);;
  }
  
  public void handleDelayedAllele(String value) throws Exception {
    life.gbol.domain.Gene gene = this.getGene();
    if(gene != null)
      gene.addAllele(value);
  }
  
  public void handleGene(String value) {
    this.delayProp("Gene", value);
  }
  
  public void handleDelayedGene(String value) throws Exception {
    Gene gene;
    try {
      gene = this.getGene();
    } catch (NoSuchElementException e) {
      gene = null;
    } catch (Exception e) {
      // More than one result...
      gene = null;
    }
      if(gene != null)
      {
        if(gene.getGeneSymbol() != null && !gene.getGeneSymbol().equals(value))
          App.logger.warn("gene symbol feature not same on gene: " + gene.getGeneSymbol() + " <-> " + value);

          gene.setGeneSymbol(value);
      }
  }
  
  public void handleGeneSynonym(String value) {
    this.delayProp("GeneSynonym", value);
  }
  
  public void handleDelayedGeneSynonym(String value) throws Exception {
    Gene gene;
    try {
      gene = this.getGene();
    } catch (NoSuchElementException e) {
      gene = null;
    }
      if (gene != null) {
        gene.addGeneSymbolSynonym(value);
      }
    }

  /**
   * Locus tag of the gene
   * 
   * locusTag xsd:String?;
   */
  public void handleLocusTag(String value) {
    this.delayProp("LocusTag", value);
  }
  
  public void handleDelayedLocusTag(String value) throws Exception {
    Gene gene;
    try {
      gene = this.getGene();
    } catch (NoSuchElementException e) {
      gene = null;
    } catch (Exception e) {
      App.logger.error(e);
      // More than one found
      gene = null;
    }

    if(gene != null) {
      if(gene.getLocusTag() != null && !gene.getLocusTag().equals(value)) {
        App.logger
            .warn("locus tag feature not same on gene: " + gene.getLocusTag() + " <-> " + value);
      }
      gene.setLocusTag(value);
    }
  }
  
  /**
   * Old locus tag of the gene
   * 
   * oldLocusTag xsd:String?;
   */
  public void handleOldLocusTag(String value) {
    this.delayProp("OldLocusTag", value);
  }
  
  public void handleDelayedOldLocusTag(String value) throws Exception {
    Gene gene;
    try {
      gene = this.getGene();
    } catch (NoSuchElementException e) {
      gene = null;
    }

    if(gene != null)
    {
      if(gene.getOldLocusTag() != null && !gene.getOldLocusTag().equals(value))
        App.logger.warn("old locus tag feature not same on gene: " + gene.getOldLocusTag() + " <-> " + value);
      gene.setOldLocusTag(value);
    }
  }
  
  /**
   * The standard name of the Gene
   * 
   * standardName xsd:String?;
   */
  public void handleStandardName(String value) {
    this.delayProp("StandardName",value);
  }
  
  public void handleDealyedStandardName(String value) throws Exception {
    Gene gene;
    try {
      gene = this.getGene();
    } catch (NoSuchElementException e) {
      gene = null;
    }

    if(gene != null)
    {
      if(gene.getStandardName() != null && !gene.getStandardName().equals(value))
        App.logger.warn("standard name feature not same on gene: " + gene.getStandardName() + " <-> " + value);
      gene.setStandardName(value);
    }
  }
  
  public void handlePartial(String value) {
    //do nothing already captured by the location object
  }
}
