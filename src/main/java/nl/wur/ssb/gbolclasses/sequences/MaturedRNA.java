package nl.wur.ssb.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class MaturedRNA<T extends life.gbol.domain.MaturedRNA> extends Transcript<T> {
  public MaturedRNA(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  

}
