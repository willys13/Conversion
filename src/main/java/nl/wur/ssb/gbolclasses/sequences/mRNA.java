package nl.wur.ssb.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;


public class mRNA<T extends life.gbol.domain.mRNA> extends MaturedRNA<T> {
  public mRNA(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

}
