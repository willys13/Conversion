package nl.wur.ssb.gbolclasses.sequences;

import life.gbol.domain.Topology;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class NASequence<T extends life.gbol.domain.NASequence> extends Sequence<T> {


  public NASequence(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }



  /**
   * The topoplogy of the sequence (linear, circular)
   * 
   * topology type::Topology?;
   * 
   * @throws Exception
   */

  public void handleTopology(String value) throws Exception {
    Topology topology = (Topology) GBOLUtil.getEnum(life.gbol.domain.Topology.class, value);
    feature.setTopology(topology);
  }

  /**
   * The set of features annotating the sequence
   * 
   * feature @:NAFeature*;
   */

  public void handleFeature(String value) {
    throw new RuntimeException("Not needed right?");
  }

  /**
   * #* Indicates that this sequence is obtained from a virus or phage that is integrated into the
   * genome of another organism
   * 
   * proviral xsd:boolean?;
   */

  public void handleProviral(String value) {
    feature.setProviral(true);
  }

  /**
   * #* If a clone library was used for sequencing, the sequence can orginate from one or more
   * clones
   * 
   * clone xsd:String*;
   */

  public void handleClone(String value) {
    this.feature.addClone(value);
  }

  /**
   * #* If a clone library was used for sequencing, specify the clone library
   * 
   * cloneLib xsd:String?;
   */
  public void handleCloneLib(String value) {
    this.feature.setCloneLib(value);
  }

  /**
   * #* Indicates if the sequence has undergone somatic rearrangement as part of an adaptive immune
   * response. If equal to false it is the unrearranged sequence that was inherited from the parenta
   * germ line. If equal to true the sequence has undergone somatic rearrangement as part of an
   * adaptive immune response. If not defined it is not applicable to the sequence or unknown.
   * 
   * rearranged xsd:boolean?;
   */

  public void handleRearranged(String value) {
    this.feature.setRearranged(true);
  }

  /**
   * #* If this NA object is integrated into another NA object, for example a phage genome which is
   * integrated into a bacterial genome or a plasmid which is integrated into a yeast genome.
   * 
   * integratedInto @:NASequence?;
   * 
   * #*- taxonomy @:TaxonomyRef;
   */

  public void handleIntegratedInto(String value) {
    life.gbol.domain.NASequence na =
        this.parent.getDomain().make(life.gbol.domain.NASequence.class, value);
    feature.setIntegratedInto(na);
  }
}
