package nl.wur.ssb.gbolclasses.sequences;

import life.gbol.domain.StrandType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class CompleteNASequence<T extends life.gbol.domain.CompleteNASequence>
    extends NASequence<T> {

  public CompleteNASequence(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  public void handleStrandType(String value) throws Exception {
    StrandType strandType = (StrandType) GBOLUtil.getEnum(life.gbol.domain.StrandType.class, value);
    feature.setStrandType(strandType);
  }
}
