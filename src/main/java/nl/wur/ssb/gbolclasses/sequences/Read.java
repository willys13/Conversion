package nl.wur.ssb.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

// public class Read extends NASequence {
public class Read<T extends life.gbol.domain.Read> extends NASequence<T> {
  public Read(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

}
