package nl.wur.ssb.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

// public class PrecursorRNA extends Transcript {
public class PrecursorRNA<T extends life.gbol.domain.PrecursorRNA> extends Transcript<T> {
  public PrecursorRNA(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

}
