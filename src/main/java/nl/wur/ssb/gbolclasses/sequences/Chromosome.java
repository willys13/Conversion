package nl.wur.ssb.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;


public class Chromosome<T extends life.gbol.domain.Chromosome> extends CompleteNASequence<T> {
  public Chromosome(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  public void handleChromosome(String value) {
    feature.setChromosome(value);
  }
}
