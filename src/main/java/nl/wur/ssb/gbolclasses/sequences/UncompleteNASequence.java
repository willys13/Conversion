package nl.wur.ssb.gbolclasses.sequences;

import life.gbol.domain.StrandType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;


public class UncompleteNASequence<T extends life.gbol.domain.UncompleteNASequence>
    extends NASequence<T> {
  public UncompleteNASequence(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * The type of the strand
   *
   * strandType type::StrandType;
   *
   * @throws Exception
   */

  public void handleStrandType(String value) throws Exception {
    StrandType strandType = (StrandType) GBOLUtil.getEnum(life.gbol.domain.StrandType.class, value);
    feature.setStrandType(strandType);
  }

}
