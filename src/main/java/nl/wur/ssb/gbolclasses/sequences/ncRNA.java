package nl.wur.ssb.gbolclasses.sequences;

import life.gbol.domain.ncRNAType;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class ncRNA<T extends life.gbol.domain.ncRNA> extends MaturedRNA<T> {
  public ncRNA(SequenceBuilder domain, T feature) {
    super(domain, feature);
    // Sets the default value
    feature.setNcRNAClass(life.gbol.domain.ncRNAType.ncRNATypeOther);
  }

  /**
   * The type of the non coding RNA
   * 
   * ncrnaClass type::ncRNAType;
   * 
   * @throws Exception
   */
  public void handleNcrnaClass(String value) throws Exception {
    if (value.matches("other"))
      value = "ncRNATypeOther";
    ncRNAType ncrnaclass = (ncRNAType) GBOLUtil.getEnum(life.gbol.domain.ncRNAType.class, value);
    feature.setNcRNAClass(ncrnaclass);
  }
}
