package nl.wur.ssb.gbolclasses.sequences;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import life.gbol.domain.Gene;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.App;


public class Transcript<T extends life.gbol.domain.Transcript> extends NASequence<T> {
  public Transcript(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  public void handleTranscriptId(String value) {
    feature.addAccession(value);
  }
  
  public life.gbol.domain.Gene getGene() throws Exception
  {
    Iterable<ResultLine> results = this.parent.getDomain().getRDFSimpleCon()
        .runQuery("getGeneForTranscript.sparql", false,this.feature.getResource().getURI());

    Iterator<ResultLine> itt = results.iterator();

    ResultLine result = itt.next();
    Gene gene = this.parent.getDomain().make(Gene.class, result.getIRI("gene"));

    if (itt.hasNext()) {
      App.logger.warn("More than one gene detected returning the first one...\nMost likely an mRNA has been assigned to two different genes");
    }
    return gene;

  }
   
  public List<life.gbol.domain.CDS> getAllCDS() throws Exception
  {
    LinkedList<life.gbol.domain.CDS> toRet = new LinkedList<life.gbol.domain.CDS>();
    for(life.gbol.domain.NAFeature feature : this.feature.getAllFeature())
    {
      if(feature instanceof life.gbol.domain.CDS)
        toRet.add((life.gbol.domain.CDS)feature);
    }  
    return toRet;
  }
  
}
