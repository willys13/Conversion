package nl.wur.ssb.gbolclasses.sequences;

import life.gbol.domain.AminoAcid;
import life.gbol.domain.AntiCodon;
import life.gbol.domain.Region;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

public class tRNA<T extends life.gbol.domain.tRNA> extends MaturedRNA<T> {
  public tRNA(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  public void handleProduct(String value) {
    feature.setProduct(value);
  }

  /**
   * propertyDefinitions "
   * 
   * #* The anti codon of the TRNA anticodon @:AntiCodon?;
   * 
   * #* The functional name product xsd:String?"@en
   */


  /**
   * 
   * @param value
   * @throws Exception
   */

  public void handleAnticodon(String value) throws Exception {
    value = value.replaceAll("complement", "");
    value = value.replaceAll("\\(", "");
    value = value.replaceAll("\\)", "");

    // Getting AA
    String aa = value.replaceAll(".*aa:([A-z]+),.*", "$1");
    
    // Getting codon
    String codon = value.replaceAll(".*seq:([A-z]+).*", "$1");

    // Getting position
    String pos = value.replaceAll(".*pos:([0-9]+..[0-9]+).*", "$1");
    int begin = Integer.parseInt(pos.split("\\.\\.")[0]);
    int end = Integer.parseInt(pos.split("\\.\\.")[1]);

    String antiUri = "/" + begin + "-" + end + "/" + codon + "/" + aa;
    
    AntiCodon anticodon = parent.getDomain().make(life.gbol.domain.AntiCodon.class,
        this.feature.getResource().getURI() + antiUri);


    Region region = this.parent.makeRegion(begin, end, "anticodon", this.feature);

    anticodon.setAminoAcid((AminoAcid) GBOLUtil.getEnum(AminoAcid.class, aa));
    anticodon.setAntiCodonSequence(codon);
    anticodon.setAntiCodingRegion(region);

    this.feature.setAnticodon(anticodon);

  }
}
