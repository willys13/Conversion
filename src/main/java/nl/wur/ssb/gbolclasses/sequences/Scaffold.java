package nl.wur.ssb.gbolclasses.sequences;

import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;

// public class Scaffold extends NASequence {
public class Scaffold<T extends life.gbol.domain.Scaffold> extends NASequence<T> {
  public Scaffold(SequenceBuilder domain, T feature) {
    super(domain, feature);
  }

  /**
   * propertyDefinitions "#TODO further define
   * 
   * contig @:Contig+;
   */

  public void handleContig(String value) {
    life.gbol.domain.Contig contig = parent.getDomain().make(life.gbol.domain.Contig.class, value);
    feature.addSourceContigs(contig);
  }

  public void handleName(String value) {
    feature.addAccession(value);
  }

}
