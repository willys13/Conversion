package nl.wur.ssb.rdfconversion;

import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.rdfhdt.hdtjena.HDTGraph;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Save;
import nl.wur.ssb.conversion.options.CommandOptionsHDT2RDF;

public class HDT2RDF {

  public static void app(CommandOptionsHDT2RDF arguments) throws Exception {
    if (CommandOptionsHDT2RDF.hdt2rdf) {
      org.rdfhdt.hdt.hdt.HDT hdt =
          org.rdfhdt.hdt.hdt.HDTManager.mapHDT(arguments.input.getAbsolutePath(), null);
      HDTGraph graph = new HDTGraph(hdt);
      Model model = ModelFactory.createModelForGraph(graph);
      model = setPrefixes(model, arguments);
      OutputStream os = new FileOutputStream(arguments.output);
      System.out.println("HDT2RDF: " + arguments.output);
      model.write(os, CommandOptionsHDT2RDF.format.toLowerCase());
    } else if (CommandOptionsHDT2RDF.rdf2hdt) {
      Domain tmp = new Domain(
          "file://" + arguments.input.getAbsolutePath() + "{" + CommandOptionsHDT2RDF.format + "}");
      Save.save(tmp, arguments.output);
    } else {
      // TODO autodetect this...
      throw new Exception("Option missing either -hdt2rdf or -rdf2hdt");
    }
  }

  private static Model setPrefixes(Model model, CommandOptionsHDT2RDF arguments) {
    for (String p : CommandOptionsHDT2RDF.prefix.split(" ")) {
      model.setNsPrefix(p.split("@")[0], p.split("@")[1]);
    }
    return model;
  }
}
