package nl.wur.ssb.proteinAnnotation;

import java.util.ArrayList;
import java.util.List;

public class ProteinDomain {
  private String DomainURI;
  private List<String> descriptions = new ArrayList<String>();

  private String IDescription;
  private String SDescription;
  private int Score;

  public void addDomainURI(String DomainURI) {
    this.setDomainURI(DomainURI);
  }

  public String getDomainURI() {
    return DomainURI;
  }

  public void setDomainURI(String domainURI) {
    this.DomainURI = domainURI;
  }

  public String getSDescription() {
    return SDescription;
  }

  public void setSDescription(String sDescription) {
    this.SDescription = sDescription;
  }

  public String getIDescription() {
    return IDescription;
  }

  public void setIDescription(String iDescription) {
    this.IDescription = iDescription;
  }

  public int getScore() {
    return Score;
  }

  public void setScore(int i) {
    this.Score = i;
  }

  public List<String> getDescriptions() {
    return descriptions;
  }

  public void setDescriptions(List<String> descriptions) {
    this.descriptions = descriptions;
  }

}
