package nl.wur.ssb.proteinAnnotation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import nl.wur.ssb.HDT.HDT;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Save;
import nl.wur.ssb.conversion.options.CommandOptionsToEmbl;

public class App {

  private static RDFSimpleCon SAPPSource;
  static CommandOptionsToEmbl arguments;
  static Domain domain;

  public static void main() throws Exception {
    System.out.println("Performing protein annotation...");
    arguments = nl.wur.ssb.conversion.toembl.ToEmbl.arguments;
    loadRDF();
    deletePreviousProteinAnnotation();
    proteinAnnotation();
    // SAVE... to a temporary HDT file
    File temp = File.createTempFile("temp-hdt-annotation", ".tmp");
    Domain domain = new Domain(SAPPSource);
    Save.save(domain, temp);
    nl.wur.ssb.conversion.toembl.ToEmbl.arguments.gbol = new File(temp.getAbsolutePath());
  }

  private static void deletePreviousProteinAnnotation() throws Exception {
    // Removes previous annotation performed by previous annotation runs at protein level.
    SAPPSource.runUpdateQuery("removePreviousannotation.txt");
  }

  private static void proteinAnnotation() throws Exception {

    List<Protein> proteinList = new ArrayList<Protein>();
    String proteinIRI = "";
    Protein proteinObject = null;
    List<ProteinDomain> domainList = new ArrayList<ProteinDomain>();
    for (ResultLine item : SAPPSource.runQuery("getProteinInformation.txt", true)) {
      if (!proteinIRI.equals(item.getIRI("cds"))) {
        proteinObject = new Protein();
        proteinObject.setProteinScore(-10);
        proteinObject.setProteinName("hypothetical protein");
        proteinList.add(proteinObject);
      }
      proteinIRI = item.getIRI("cds");
      String interpro = item.getIRI("interpro");
      String idesc = item.getLitString("idesc");
      String sdesc = item.getLitString("sdesc");
      String bdesc = item.getLitString("bdesc");
      String pdesc = item.getLitString("pdesc");

      Float score = null;
      if (item.getLitString("score") != null) {
        score = Float.parseFloat(item.getLitString("score"));
      }

      if (proteinObject.getProteinName() == "hypothetical protein") {
        if (score != null && score < 0.00005) {
          // Should always be the case as only blast score is used
          // but placed here if query changes
          if (bdesc != null) {
            // Formatting..
            bdesc = bdesc.replaceAll("(OS=.*|GN=.*|PE=[0-9]+|SV=[0-9]+)", "");
            proteinObject.setProteinName(bdesc);
            proteinObject.setProteinURI(proteinIRI);
            System.out.println("protein name set...");
          }
        }

        ProteinDomain domainX = new ProteinDomain();
        proteinObject.addProteinURI(proteinIRI);

        proteinObject.addDomain(domainX);

        domainX.addDomainURI(interpro);

        // Make the descriptions
        List<String> descriptionList = domainX.getDescriptions();

        if (bdesc != null) {
          descriptionList.add(bdesc);
        }
        if (idesc != null) {
          descriptionList.add(idesc);
        }
        if (sdesc != null) {
          descriptionList.add(sdesc);
        }
        if (pdesc != null) {
          descriptionList.add(pdesc);
        }

        domainX.setDescriptions(descriptionList);

        domainX.setScore(-1);

        domainList.add(domainX);
      }
    }
    for (Protein protein : proteinList) {

      List<ProteinDomain> domains = protein.getDomains();
      functionCalculator(domains, protein);

      String cdsURI = protein.getProteinURI();
      String product = protein.getProteinName();
      // issue here...
      // RDFSubject proteinRDF = new RDFSubject(domain, proteinURI, "ssb:Protein");
      // proteinRDF.addLit("ssb:product", product.replace("_", " "));

      life.gbol.domain.CDS gbol_cds = domain.make(life.gbol.domain.CDS.class, cdsURI);
      gbol_cds.setProduct(product.replaceAll("_+", " "));
    }
  }

  private static void functionCalculator(List<ProteinDomain> domains, Protein protein) {
    // Priority naming... based on local knowledge
    String[] no_priority = {"p-loop", "domain-like", "conserved", "binding", "like", "family",
        "enzyme", "containing", "unknown", "uncharacterized", "UPF", "(duf)", "terminal",
        "helix-turn-helix", "repeat", "coil", "domain"};
    String[] low_priority = {"predicted", "homolog", "putative", "related", "associated"};
    String[] higher_priority_spores = {"spore", "spoI", "sporulation", "germination", "capsule"};
    String[] higher_priority_virus = {"virus", "viral", "phage", "transposase", "transposon",
        "transposition", "integrase", "resolvase", "dde superfamily endonuclease"};
    String[] higher_priority_transporter = {"transport", "influx", "efflux", "permease", "intake",
        "uptake", "symport", "antiport", "import", "pump", "exchanger", "channel", "translocase"};
    String[] higher_priority_transcription_factors =
        {"regul", "repress", "transcription", "zinc-finger", "zinc finger"};
    String[] higher_priority_chaperones =
        {"chaperone", "chaperonin", "heat shock", "heat-shock", "cold-shock", "cold shock"};

    // int pscore = -9;
    for (ProteinDomain domain : domains) {
      int annotationScore = protein.getProteinScore();
      List<String> descriptions = domain.getDescriptions();
      // For each description found.. check the best information // Order
      // of description based on coverage?
      for (String description : descriptions) {

        if (description != null) {
          int score = 0;

          if (description.equals("")) {
            score = -10;
          } else {
            description = description.toLowerCase().replace(",", " ");

            score = matching(description, no_priority, -2, score);
            score = matching(description, low_priority, -1, score);
            score = matching(description, higher_priority_spores, +1, score);
            score = matching(description, higher_priority_virus, +1, score);
            score = matching(description, higher_priority_transporter, +1, score);
            score = matching(description, higher_priority_transcription_factors, +1, score);
            score = matching(description, higher_priority_chaperones, +1, score);
            // Setting the name...
            if (score > annotationScore) {
              protein.setProteinName(description);
              protein.setProteinScore(score);
            }
          }
        }
      }
    }

  }

  private static int matching(String description, String[] elements, int value, int score) {
    for (String element : elements) {
      if (description.contains(element)) {
        score += value;
      }
    }
    return score;
  }

  private static void loadRDF() throws Exception {
    File input = arguments.gbol;

    File temp = File.createTempFile("turtle", ".tmp");
    HDT hdt = new HDT();
    hdt.hdt2rdf(input, temp);

    System.out.println("TEMP FILE:" + input);
    SAPPSource = new RDFSimpleCon("file://" + temp + "{NT}");
    SAPPSource.setNsPrefix("gbol", "http://gbol.life/0.1/");
    domain = new Domain(SAPPSource);
  }
}
