package nl.wur.ssb.proteinAnnotation;

import java.util.ArrayList;
import java.util.List;

public class Protein {

  private String ProteinURI;
  private List<ProteinDomain> domains = new ArrayList<ProteinDomain>();
  private String ProteinName;
  private int ProteinScore;

  public void addProteinURI(String protein) {
    this.setProteinURI(protein);
  }

  public String getProteinURI() {
    return ProteinURI;
  }

  public void setProteinURI(String proteinURI) {
    this.ProteinURI = proteinURI;
  }

  public List<ProteinDomain> getDomains() {
    return domains;
  }

  public void addDomain(ProteinDomain domainURI) {
    this.domains.add(domainURI);
  }

  public String getProteinName() {
    return ProteinName;
  }

  public void setProteinName(String proteinName) {
    ProteinName = proteinName;
  }

  public int getProteinScore() {
    return ProteinScore;
  }

  public void setProteinScore(int proteinScore) {
    ProteinScore = proteinScore;
  }
}
