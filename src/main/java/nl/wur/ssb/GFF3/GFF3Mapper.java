package nl.wur.ssb.GFF3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.ImportProv;
import nl.wur.ssb.SappGeneric.Save;
import nl.wur.ssb.SappGeneric.Store;
import nl.wur.ssb.conversion.LocusTagger;
import nl.wur.ssb.conversion.flatfile.FlatFileEntry;
import nl.wur.ssb.conversion.options.CommandOptionsGFF3;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.entry.EntryFactory;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.feature.FeatureFactory;
import uk.ac.ebi.embl.api.entry.location.Location;
import uk.ac.ebi.embl.api.entry.location.LocationFactory;
import uk.ac.ebi.embl.api.entry.location.Order;
import uk.ac.ebi.embl.api.entry.qualifier.QualifierFactory;
import uk.ac.ebi.embl.api.entry.sequence.Sequence;
import uk.ac.ebi.embl.api.gff3.GFF3Record;
import uk.ac.ebi.embl.api.gff3.GFF3RecordSet;
import uk.ac.ebi.embl.fasta.reader.FastaFileReader;
import uk.ac.ebi.embl.fasta.reader.FastaLineReader;
import uk.ac.ebi.embl.gff3.reader.GFF3FlatFileEntryReader;

public class GFF3Mapper {

  private String[] gbolClasses = {"nl.wur.ssb.gbolclasses.features.ArtificialRecognizedRegion",
      "nl.wur.ssb.gbolclasses.features.AssemblyAnnotation",
//      "nl.wur.ssb.gbolclasses.features.AssemblyGap",
      "nl.wur.ssb.gbolclasses.features.BiologicalRecognizedRegion",
      "nl.wur.ssb.gbolclasses.features.CDS", "nl.wur.ssb.gbolclasses.features.Centromere",
      "nl.wur.ssb.gbolclasses.features.ConstantRegion",
      "nl.wur.ssb.gbolclasses.features.CRISPRCassette",
      "nl.wur.ssb.gbolclasses.features.DiversitySegment", "nl.wur.ssb.gbolclasses.features.DLoop",
      "nl.wur.ssb.gbolclasses.features.Exon", "nl.wur.ssb.gbolclasses.features.Feature",
      "nl.wur.ssb.gbolclasses.features.FivePrimeUTR",
//      "nl.wur.ssb.gbolclasses.features.Gap",
      "nl.wur.ssb.gbolclasses.features.GapRegion", "nl.wur.ssb.gbolclasses.features.Gene",
      "nl.wur.ssb.gbolclasses.features.GeneralFeature",
      "nl.wur.ssb.gbolclasses.features.GenomicFeature", "nl.wur.ssb.gbolclasses.features.Homology",
      "nl.wur.ssb.gbolclasses.features.ImmunoglobulinFeature",
      "nl.wur.ssb.gbolclasses.features.IntegratedVirus", "nl.wur.ssb.gbolclasses.features.Intron",
      "nl.wur.ssb.gbolclasses.features.JoiningSegment",
      "nl.wur.ssb.gbolclasses.features.MaturePeptide",
      "nl.wur.ssb.gbolclasses.features.MiscBinding", "nl.wur.ssb.gbolclasses.features.MiscFeature",
      "nl.wur.ssb.gbolclasses.features.MiscRecomb", "nl.wur.ssb.gbolclasses.features.MiscStructure",
      "nl.wur.ssb.gbolclasses.features.MiscVariation",
      "nl.wur.ssb.gbolclasses.features.MobileElement",
      "nl.wur.ssb.gbolclasses.features.ModifiedBase",
      "nl.wur.ssb.gbolclasses.features.ModifiedResidue",
      "nl.wur.ssb.gbolclasses.features.NAFeature",
      "nl.wur.ssb.gbolclasses.features.NaturalVariation", "nl.wur.ssb.gbolclasses.features.Operon",
      "nl.wur.ssb.gbolclasses.features.PolyASite", "nl.wur.ssb.gbolclasses.features.PrimerBinding",
      "nl.wur.ssb.gbolclasses.features.ProteinBinding",
      "nl.wur.ssb.gbolclasses.features.ProteinFeature",
      "nl.wur.ssb.gbolclasses.features.ProteinHomology",
      "nl.wur.ssb.gbolclasses.features.ProteinRepeat",
      "nl.wur.ssb.gbolclasses.features.ProteinStructure",
      "nl.wur.ssb.gbolclasses.features.RecognizedRegion",
      "nl.wur.ssb.gbolclasses.features.RegulationSite",
      "nl.wur.ssb.gbolclasses.features.RepeatFeature",
      "nl.wur.ssb.gbolclasses.features.RepeatRegion",
      "nl.wur.ssb.gbolclasses.features.ReplicationOrigin",
      "nl.wur.ssb.gbolclasses.features.SequenceAnnotation",
      "nl.wur.ssb.gbolclasses.features.SequenceTaggedSite",
      "nl.wur.ssb.gbolclasses.features.SignalPeptide", "nl.wur.ssb.gbolclasses.features.Source",
      "nl.wur.ssb.gbolclasses.features.StemLoop",
      "nl.wur.ssb.gbolclasses.features.StructureFeature",
      "nl.wur.ssb.gbolclasses.features.SwitchRegion", "nl.wur.ssb.gbolclasses.features.Telomere",
      "nl.wur.ssb.gbolclasses.features.ThreePrimeUTR",
      "nl.wur.ssb.gbolclasses.features.TranscriptFeature",
      "nl.wur.ssb.gbolclasses.features.TranscriptionElement",
      "nl.wur.ssb.gbolclasses.features.TransferOrigin",
      "nl.wur.ssb.gbolclasses.features.TransMembraneRegion",
//      "nl.wur.ssb.gbolclasses.features.UnknownAssemblyGap",
      "nl.wur.ssb.gbolclasses.features.UnsureBases",
      "nl.wur.ssb.gbolclasses.features.UpdatedSequence",
      "nl.wur.ssb.gbolclasses.features.VariableRegion",
      "nl.wur.ssb.gbolclasses.features.VariableSegment",
      "nl.wur.ssb.gbolclasses.features.VariationFeature", "nl.wur.ssb.gbolclasses.Sample",
      "nl.wur.ssb.gbolclasses.sequences.Chromosome",
      "nl.wur.ssb.gbolclasses.sequences.CompleteNASequence",
      "nl.wur.ssb.gbolclasses.sequences.Contig", "nl.wur.ssb.gbolclasses.sequences.MaturedRNA",
      "nl.wur.ssb.gbolclasses.sequences.MiscRna", "nl.wur.ssb.gbolclasses.sequences.mRNA",
      "nl.wur.ssb.gbolclasses.sequences.NASequence", "nl.wur.ssb.gbolclasses.sequences.ncRNA",
      "nl.wur.ssb.gbolclasses.sequences.Plasmid", "nl.wur.ssb.gbolclasses.sequences.PrecursorRNA",
      "nl.wur.ssb.gbolclasses.sequences.Read", "nl.wur.ssb.gbolclasses.sequences.rRNA",
      "nl.wur.ssb.gbolclasses.sequences.Scaffold", "nl.wur.ssb.gbolclasses.sequences.Sequence",
      "nl.wur.ssb.gbolclasses.sequences.tmRNA", "nl.wur.ssb.gbolclasses.sequences.Transcript",
      "nl.wur.ssb.gbolclasses.sequences.tRNA",
      "nl.wur.ssb.gbolclasses.sequences.UncompleteNASequence", "nl.wur.ssb.gbolclasses.Thing"};
  private Entry entry;
  private HashMap<String, String> allowed = new HashMap<String, String>();
  private Set<String> missed = new HashSet<String>();
  private EntryFactory entryFactory = new EntryFactory();
  private FeatureFactory featureFactory = new FeatureFactory();
  private Feature feature;
  QualifierFactory qualifierFactory = new QualifierFactory();
  private uk.ac.ebi.embl.api.entry.qualifier.Qualifier qualifier;
  private LocationFactory locationFactory = new LocationFactory();
  private List<Entry> entryList = new ArrayList<Entry>();
  private HashMap<String, Sequence> fasta = new HashMap<String, Sequence>();
  String resourceBundle = "uk.ac.ebi.embl.gff3.mapping.gffMapper";

  /*
   * # GFF3 FEATURES AND QUALIFIERS MAPPING
   * 
   * gene=gene_embl
   * 
   * TF_binding_site=TF_binding_site_embl
   * 
   * mRNA=mRNA_embl
   * 
   * five_prime_UTR=five_prime_UTR_embl
   * 
   * CDS=CDS_embl
   * 
   * exon=exon_embl
   * 
   * three_prime_UTR=three_prime_UTR_embl
   */

  public GFF3Mapper(String[] args) throws Exception {
    CommandOptionsGFF3 arguments = new CommandOptionsGFF3(args);
    getHandles();
    Domain domain = Store.createMemoryStore();
    File gffFile = arguments.input;
    File fastaFile = arguments.fasta;
    System.err.println("Parsing FASTA");
    parseFasta(fastaFile);
    System.err.println("Parsing GFF");
    BufferedReader fileReader = new BufferedReader(new FileReader(gffFile));
    GFF3FlatFileEntryReader reader = new GFF3FlatFileEntryReader(fileReader);
    reader.read();
    GFF3RecordSet records = reader.getEntry();
    mapGFF3ToEntry(records);

    File[] inputs = {arguments.fasta, arguments.input};

    nl.wur.ssb.SappGeneric.ImportProv importProv = new ImportProv(domain, inputs, arguments.output,
        arguments.commandLine, arguments.toolName, arguments.toolVersion, arguments.repository,
        arguments.userAgentIri, arguments.starttime, LocalDateTime.now());

    if (missed.size() > 0)
      System.err.println("The following properties are not captured by the ontology: "
        + StringUtils.join(missed, " "));

    // Starting the Parser
    for (Entry entry : entryList) {
      System.out.println("Parsing: " + entry.getId() + "\tLength:" + entry.getSequence().getLength());
      new FlatFileEntry(domain, arguments.identifier, entry, importProv);
    }

    // TODO TESTING
//    StringWriter writer = new StringWriter();
//    new EmblEntryWriter(entry).write(writer);
//    Files.write(Paths.get(arguments.output.getAbsolutePath()+".embl"), writer.toString().getBytes());
    // TESTING

    importProv.finished();

    LocusTagger.tag(domain);

    if (arguments.format.matches("ttl"))
      domain.save(arguments.output.getAbsolutePath(), RDFFormat.TURTLE);
    else
      Save.save(domain, arguments.output);

  }

  private void getHandles()
      throws NoSuchMethodException, SecurityException, ClassNotFoundException {
    for (String clazzName : gbolClasses) {
      Class<?> clazz = Class.forName(clazzName);
      Method[] methods = clazz.getDeclaredMethods();
      for (Method method : methods) {
        String name = method.getName();
        if (name.contains("handle"))
          allowed.put(name, "");
      }
    }
  }

  /**
   * 
   * @param fastaFile
   * @throws Exception
   */
  public void parseFasta(File fastaFile) throws Exception {
    BufferedReader bufreader = new BufferedReader(new FileReader(fastaFile));
    FastaFileReader fastaReader = new FastaFileReader(new FastaLineReader(bufreader));

    fastaReader.read();

    while (fastaReader.isEntry()) {
      Entry entry = fastaReader.getEntry();
      String primary = entry.getComment().getText().replaceAll(" .*", "");
      Sequence sequence = entry.getSequence();

      if (fasta.containsKey(primary)) {
        throw new Exception("Sequence accessions are not unique");
      } else {
        fasta.put(primary, sequence);
      }
      fastaReader.read();
    }
  }


  public List<Entry> mapGFF3ToEntry(GFF3RecordSet records) throws Exception {

    List<GFF3RecordElement> gff3RecordElements = new ArrayList<GFF3RecordElement>();
    Set<String> ids = new TreeSet<String>();
    HashMap<String, Entry> entries = new HashMap<String, Entry>();

    System.err.println("Parsing " + records.getRecords().size() + " entries");

    for (GFF3Record record : records.getRecords()) {
      String id = null;
      String parent = null;
      Map<?, ?> attributes = record.getAttributes();
      Iterator<?> attributeIterator = attributes.entrySet().iterator();

      // Retrieval of parent and current identifier

      while (attributeIterator.hasNext()) {
        Map.Entry attributePairs = (Map.Entry) attributeIterator.next();
        String attributeKey = attributePairs.getKey().toString();
        String attributeValue = attributePairs.getValue().toString().toLowerCase();
        String attributeKeyLookup = WordUtils.capitalizeFully(attributeKey.replaceAll("_", " "));
        attributeKeyLookup = attributeKeyLookup.replaceAll(" +", "");

        if (attributeKey.toLowerCase().matches("parent"))
          parent = attributeValue;

        if (attributeKey.toLowerCase().matches("id")) {
          id = attributeValue;
        }
      }

      if (id == null) {
        id = record.toString();
      }

      // Only create new feature if ID does not exist yet... otherwise add the record to the id
      // Hereby CDS need identical names which could give issues in the feature
      // TODO Merge CDS based on parent ID not on element ID
      if (!ids.contains(id)) {
        ids.add(id);
        GFF3RecordElement gff3RecordElement = new GFF3RecordElement();
        gff3RecordElement.setId(id);
        gff3RecordElement.setParent(parent);

        // If exon or cds for mRNA positioning
        if (record.getType().toLowerCase().matches("exon")) {
          gff3RecordElement.addExon(record);
        } else if (record.getType().toLowerCase().matches("cds")) {
          gff3RecordElement.addCds(record);
        }

        // Redundant code but CDS / Exon are used for positioning of parent temporarily
        gff3RecordElement.addGFFRecord(record);

        gff3RecordElement.setSequenceID(record.getSequenceID());
        if (record.getType().toLowerCase().matches("(region|chromosome)"))
          gff3RecordElement.setFeature(featureFactory.createFeature("source"));
        else if (record.getType().toLowerCase().endsWith("gene")) {
          gff3RecordElement.setFeature(featureFactory.createFeature("gene"));
        } else if (record.getType().toLowerCase().matches("(transcript|.*rna)")) {
          //record.getAttributes().get("biotype")
          gff3RecordElement.setFeature(featureFactory.createFeature("mRNA"));
        } else {
          // Interestingly... When creating a CDS object it automatically creates an empty locations
          // object but not for the other types...
          gff3RecordElement.setFeature(featureFactory.createFeature(record.getType()));
        }

        gff3RecordElements.add(gff3RecordElement);

        /*
         * Entry store with fasta sequence...
         */
        entry = entries.get(gff3RecordElement.getSequenceID());

        if (entry == null) {
          entry = entryFactory.createEntry();
          entry.setPrimaryAccession(gff3RecordElement.getSequenceID());
          entry.setId(gff3RecordElement.getSequenceID());
          entry.setSequence(fasta.get(gff3RecordElement.getSequenceID()));
          if (fasta.get(gff3RecordElement.getSequenceID()).getLength() > 0) {
            // For the genbank parser
            entryList.add(entry);
          } else {
            throw new Exception("Sequence is empty");
          }
          // Store sequence object
          entries.put(gff3RecordElement.getSequenceID(), entry);
        }
        entry.addFeature(gff3RecordElement.getFeature());

      } else {
        // If the identifier already exists
        boolean check = false;
        for (GFF3RecordElement element : gff3RecordElements) {
          if (element.getId().equals(id)) {
            element.addGFFRecord(record);
            check = true;
            break;
          }
        }
        if (check != true)
          throw new Exception("Not found??...");
      }
    }

    /*
     * Creation of combined records completed...
     */

    System.err.println("Creation of combined records completed...");

    /*
     * For each child in the elementList (which is a GFF3RecordElement with parentid, id, genbank
     * feature object, gff record(s)
     */

    System.err.println("Parental positioning...");

    // ElementList contains exons separately but CDS joined when same parent is detected
    for (GFF3RecordElement child : gff3RecordElements) {
      // When a child has a parent class like exon? > mrna > gene
      if (child.getParent() != null) {
        GFF3RecordElement parentRecord = null;
        // Getting the parental record
        for (GFF3RecordElement possibleParent : gff3RecordElements) {
          if (possibleParent.getId().equals(child.getParent())) {
            parentRecord = possibleParent;
            break;
          }
          // CDS linkage... join initiated available
          // System.err.println(child.getFeature().getId());
        }
        // When a parent record is found
        if (parentRecord != null) {
          // Gene is ignored as the positioning is going to be fine...
          if (parentRecord.getRecords().get(0).getType().toLowerCase().contains("gene")) {
          } // If parent is of type "RNA"
          else if (parentRecord.getRecords().get(0).getType().toLowerCase()
              .matches("(transcript|.*rna)")) {
            // This is for CDS as well as for EXONS..!!!!

            // Position needs to change based on the childs... when exons available

            // Check if exons are available
            if (child.getExons() != null) {
              records = child.getExons();
            } else if (child.getCds() != null) {
              records = child.getCds();
            }


            // There is an issue with for example exons they do not get an identifier and there for
            // are all unique but the parent needs the positions thus the positions should be
            // updated not overwritten
            
            if (parentRecord.getFeature().getLocations() != null) {
              // We already have set the location object now just add to it
              for (GFF3Record record : records.getRecords()) {
                parentRecord.getFeature().getLocations().addLocation(createLocation(record));
//                System.out.println("Complement: "+ parentRecord.getFeature().getLocations().isComplement());
              }
            } else {
              // If there is no location object yet...
              Order<Location> compoundJoin = new Order<Location>();
              for (GFF3Record record : records.getRecords()) {
                Location location = createLocation(record);
                compoundJoin.addLocation(location);
//                compoundJoin.setComplement(compoundJoin.getLocations().get(0).isComplement());
              }
              parentRecord.getFeature().setLocations(compoundJoin);
            }
          }
        }
      }
    }

    System.err.println("Finalizing locations and qualifiers...");
    // Finalizing each (merged) entry
    for (GFF3RecordElement element : gff3RecordElements) {
//      System.err.println(element.getId()+"\t"+element.getRecords().size());
      // If the position is not set by the childs use its own position to set...
      if (element.getFeature().getLocations() == null || element.getFeature().getLocations().getMinPosition() == null) {
//        System.err.println("THIS: "+element.getId()+"\t"+element.getRecords().size());
//        System.err.println("No pos: "+element.getId());
        Order<Location> compoundJoin = new Order<Location>();
        // Using each record with the same identifier (e.g. multiple CDS belonging to one single CDS)
        for (GFF3Record record : element.getRecords()) {
          Location location = createLocation(record);
          compoundJoin.addLocation(location);
        }
//        compoundJoin.setComplement(compoundJoin.getLocations().get(0).isComplement());
        element.getFeature().setLocations(compoundJoin);
      }

      // Setting the qualifiers
      Collection<uk.ac.ebi.embl.api.entry.qualifier.Qualifier> qualifiers =
          new ArrayList<uk.ac.ebi.embl.api.entry.qualifier.Qualifier>();
      for (GFF3Record record : element.getRecords()) {
        for (uk.ac.ebi.embl.api.entry.qualifier.Qualifier qualifier : setQualifiers(record))
          qualifiers.add(qualifier);
      }
      element.getFeature().addQualifiers(qualifiers);
    }

    // CHECKING
    // for (Feature feature : entryList.get(0).getFeatures()) {
    // if (feature.getLocations().getMinPosition() == 1807)
    // System.err.println(feature.getLocations());
    // }
    return entryList;

  }

  private Location createLocation(GFF3Record record) {
    long start = record.getStart();
    long end = record.getEnd();
    Location location = locationFactory.createLocalRange(start, end);
    location.setComplement(setStrand(record.getStrand()));
    return location;
  }

  private boolean setStrand(int strand) {
    if (strand == -1) {
      return true;
    }
    return false;
  }

  private Collection<uk.ac.ebi.embl.api.entry.qualifier.Qualifier> setQualifiers(
      GFF3Record record) {

    Iterator<?> attributeIterator = record.getAttributes().entrySet().iterator();
    Collection<uk.ac.ebi.embl.api.entry.qualifier.Qualifier> qualifierList =
        new ArrayList<uk.ac.ebi.embl.api.entry.qualifier.Qualifier>();
    while (attributeIterator.hasNext()) {
      Map.Entry attributePairs = (Map.Entry) attributeIterator.next();
      String attributeKey = attributePairs.getKey().toString();
      String attributeValue = attributePairs.getValue().toString().toLowerCase();
      String attributeKeyLookup = WordUtils.capitalizeFully(attributeKey.replaceAll("_", " "));
      attributeKeyLookup = attributeKeyLookup.replaceAll(" +", "");

      // System.err.println(attributeKeyLookup);

      if (attributeKeyLookup.matches("^Id$") && attributeValue.contains(":")) {
        attributeValue = attributeValue.split(":")[1];
      }

      if (allowed.get("handle" + attributeKeyLookup) != null) {
        qualifier = qualifierFactory.createQualifier(attributeKey, attributeValue);
        qualifierList.add(qualifier);
      } else if (attributeKey.toLowerCase().matches("dbxref")) {
        qualifier = qualifierFactory.createQualifier("db_xref", attributeValue);
      } else {
        missed.add(attributeKeyLookup);
      }
    }
    return qualifierList;
  }
}


// Perform parent linking
// for (GFF3Record record : featureContainer.values()) {
// Map attributes = record.getAttributes();
// Iterator attributeIterator = attributes.entrySet().iterator();while(attributeIterator.hasNext())
// {
// Map.Entry attributePairs = (Map.Entry) attributeIterator.next();
// String attributeKey = attributePairs.getKey().toString();
// String attributeValue = attributePairs.getValue().toString().toLowerCase();
// String attributeKeyLookup = WordUtils.capitalizeFully(attributeKey.replaceAll("_", " "));
// attributeKeyLookup = attributeKeyLookup.replaceAll(" +", "");
//
// Collection<Qualifier> qualifierList = new ArrayList<Qualifier>();
// boolean isFeature = false;
// String sequenceId = record.getSequenceID();
// String source = record.getSource();
// String featureType = record.getType();
// long start = record.getStart();
// long end = record.getEnd();
// double score = record.getScore();
// int strand = record.getStrand();
// int phase = record.getPhase();
// Map attributes = record.getAttributes();
//
// // GFF3 Qualifier Mapping
//
//
//
// /*
// * Region matching;
// *
// * NW_012561447.1 RefSeq region 1 1060 . + .
// * ID=id1546220;Dbxref=taxon:8030;Name=Unknown;breed=double
// * haploid;chromosome=Unknown;dev-stage=adult;gbkey=Src;genome=genomic;isolate=Sally;mol_type=
// * genomic DNA;sex=female;tissue-type=muscle
// *
// */
//
// Iterator attributeIterator = attributes.entrySet().iterator();
// String parent = null;
// String iD = null;
// while (attributeIterator.hasNext()) {
// Map.Entry attributePairs = (Map.Entry) attributeIterator.next();
// String attributeKey = attributePairs.getKey().toString();
// String attributeValue = attributePairs.getValue().toString().toLowerCase();
// String attributeKeyLookup = WordUtils.capitalizeFully(attributeKey.replaceAll("_", " "));
// attributeKeyLookup = attributeKeyLookup.replaceAll(" +", "");
//
//
// if (attributeKey.toLowerCase().matches("parent"))
// parent = attributeValue;
//
// if (attributeKey.toLowerCase().matches("id"))
// iD = attributeValue;
//
// if (allowed.get("handle" + attributeKeyLookup) != null) {
// qualifier = qualifierFactory.createQualifier(attributeKey, attributeValue);
// qualifierList.add(qualifier);
// } else if (attributeKey.toLowerCase().matches("dbxref")) {
// qualifier = qualifierFactory.createQualifier("db_xref", attributeValue);
// } else {
// missed.add(attributeKey);
// }
// }

//
//
//
// if (qualifierList.size() == 0) {
// System.out.println("no qualifiers matched");
// }
//
// // GFF3 start and end columns
// Location location = locationFactory.createLocalRange(start, end);
//
// Order<Location> compoundJoin = new Order<Location>();
// compoundJoin.addLocation(location);
//
// // GFF3 Feature Mapping
// if (featureType.equals("region"))
// feature = featureFactory.createFeature("source");
// else if (featureType.toLowerCase().equals("cds")) {
// // Check if feature already exists with correct parent... if so obtain that feature
// feature = cdsContainer.get(parent);
// if (feature != null) {
// feature.setLocations(compoundJoin);
// }
// }
//
// if (featureType.toLowerCase().equals("exon")) {
// System.err.println(parent);
// Feature mrnafeature = mrnaContainer.get(parent);
// mrnafeature.setLocations(compoundJoin);
// }
//
// feature = featureFactory.createFeature(featureType);
// feature.addQualifiers(qualifierList);
// feature.setLocations(compoundJoin);
//
// // mRNA should get the locations of the exons
// if (featureType.toLowerCase().matches("(mrna|snorna|pseudogene|transcript)")) {
// mrnaContainer.put(iD, feature);
// }
//
//
//
// if (featureType.toLowerCase().equals("cds")) {
// cdsContainer.put(parent, feature);
// }
//
//
// if (strand == -1) {
// location.setComplement(true);
// feature.getLocations().setComplement(true);
// }
//
// if (tempSeqid == null || !tempSeqid.equals(sequenceId)) {
// tempSeqid = sequenceId;
// entry = entryFactory.createEntry();
// entry.addFeature(feature);
// entryList.add(entry);
// } else if (tempSeqid.equals(sequenceId)) {
// entry.addFeature(feature);
// }
//
//
// TaxonFactory taxonFactory = new TaxonFactory();
// Taxon taxon = taxonFactory.createTaxon();
// taxon.setScientificName("user input here");
// // entry.getPrimarySourceFeature().setTaxon(taxon);
//
// }
//
// return entryList;

