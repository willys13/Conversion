package nl.wur.ssb.GFF3;

import java.util.List;

import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.gff3.GFF3Record;
import uk.ac.ebi.embl.api.gff3.GFF3RecordSet;

public class GFF3RecordElement {

  private String id;
  private String parent;
  private Feature feature;
  private GFF3RecordSet records = new GFF3RecordSet();
  private GFF3RecordSet exons = new GFF3RecordSet();
  private GFF3RecordSet cds = new GFF3RecordSet();
  private String sequenceID;

  
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getParent() {
    return parent;
  }

  public void setParent(String parent) {
    this.parent = parent;
  }

  public List<GFF3Record> getRecords() {
    return records.getRecords();
  }

  public void addGFFRecord(GFF3Record record) { this.records.addRecord(record); }

  public Feature getFeature() {
    return feature;
  }

  public void setFeature(Feature feature) {
    this.feature = feature;
  }

  public String getSequenceID() {
    return sequenceID;
  }

  public void setSequenceID(String sequenceID) {
    this.sequenceID = sequenceID;
  }

  public GFF3RecordSet getExons() { return exons; }

  public void addExon(GFF3Record exon) { this.exons.addRecord(exon); }

  public GFF3RecordSet getCds() { return cds; }

  public void addCds(GFF3Record cds) {  this.cds.addRecord(cds); }
}
