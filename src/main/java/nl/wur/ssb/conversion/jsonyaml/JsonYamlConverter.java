package nl.wur.ssb.conversion.jsonyaml;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import com.github.jsonldjava.core.JsonLdApi;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.core.RDFDataset;
import com.github.jsonldjava.utils.JsonUtils;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.conversion.options.CommandOptionsJsonYaml;

public class JsonYamlConverter
{
  public static void convert(CommandOptionsJsonYaml args) throws Exception
  {
    DumperOptions options = new DumperOptions();
    options.setPrettyFlow(true);
    Yaml yaml = new Yaml(options);
    Object contextIn = JsonUtils.fromString(Util.readFile("GBOL_JSONLDFrame.json"));
    JsonLdOptions ldoptions = new JsonLdOptions();
    
    if(args.rdf2json || args.rdf2yaml)
    {
      System.out.println("Loading ...");
      JsonLdApi jsonAPI = new JsonLdApi();
      RDFSimpleCon con = new RDFSimpleCon("file://" + args.input);
      Object data = jsonAPI.fromRDF(con.createJSONLDApiGraph());
      System.out.println("RDF file loaded, framing ...");            
      
      data = JsonLdProcessor.frame(data,contextIn,ldoptions);
      System.out.println("Framing done, saving ...");
      data = ((HashMap<String,Object>)data).get("@graph");
      fixNullAndSequence((List<Object>)data,true);

      if(args.rdf2json)
        FileUtils.write(args.output,JsonUtils.toPrettyString(data));
      else
        FileUtils.write(args.output,yaml.dump(data));
      System.out.println("Done");
    }
    else if(args.json2rdf || args.yaml2rdf)
    {
      System.out.println("Loading ...");
      Object dataRetrieved = null;
      if(args.json2rdf)
        dataRetrieved = JsonUtils.fromString(FileUtils.readFileToString(args.input));
      else
        dataRetrieved = yaml.load(FileUtils.readFileToString(args.input));
      System.out.println("Loaded, converting to RDF ...");
      fixNullAndSequence((List<Object>)dataRetrieved,false);
      HashMap<String,Object> rebuild = new HashMap<String,Object>();
      rebuild.put("@graph",dataRetrieved);
      rebuild.put("@context", ((HashMap<String,Object>)contextIn).get("@context"));
      RDFDataset rebuild2 = (RDFDataset)JsonLdProcessor.toRDF(rebuild,ldoptions);
        
      RDFSimpleCon testOut = new RDFSimpleCon("");
      testOut.addAllFromJSONLDApiGraph(rebuild2);
      System.out.println("Converted, writing ...");
      testOut.save(args.output.toString());
      System.out.println("Done");
    }
  }
  
  private static void fixNullAndSequence(HashMap<String,Object> obj,boolean forward)
  {
    for(String key : new LinkedList<String>(obj.keySet()))
    {
      Object val = obj.get(key);
      if(val instanceof HashMap<?,?>)
        fixNullAndSequence((HashMap<String,Object>)val,forward);
      else if(val instanceof List<?>)
        fixNullAndSequence((List<?>)val,forward);
      else if(val == null || (val instanceof String && ((String)val).trim().equals("null")))
        obj.remove(key);
      else if(key.equals("sequence") && val instanceof String)
      {
        String sequence = (String)val;
        if(forward)
        {          
          if(sequence.length() > 80)
          {
            StringBuilder builder = new StringBuilder();
            for(int i = 0;i < sequence.length();)
            {
              int begin = i;
              i = Math.min(i += 80,sequence.length());
              builder.append(sequence.substring(begin,i));
              builder.append('\n');
            }
            obj.put(key,builder.toString());
          }
        }
        else
        {
          obj.put(key,sequence.replaceAll("\n",""));
        }
      }
    }
  }
  private static void fixNullAndSequence(List<?> obj,boolean forward)
  {
    for(Object item : obj)
    {
      if(item instanceof HashMap<?,?>)
        fixNullAndSequence((HashMap<String,Object>)item,forward);
      else if(item instanceof List<?>)
        fixNullAndSequence((List<?>)item,forward);
    }
  }
}
