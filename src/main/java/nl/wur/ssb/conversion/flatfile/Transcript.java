package nl.wur.ssb.conversion.flatfile;

import java.util.LinkedList;

import uk.ac.ebi.embl.api.entry.feature.Feature;

public class Transcript extends GeneElement
{
  public life.gbol.domain.Transcript gbolSequence;
  public Gene gene;
  private LinkedList<Exon> exonList = new LinkedList<Exon>();
  boolean isReverse;

  public Transcript(Feature gbFeature,life.gbol.domain.Transcript gbolFeature)
  {
    super(gbFeature);
    this.gbolSequence = gbolFeature;
  }
  
  public void addExon(Exon toAdd)
  {
    this.exonList.add(toAdd);
   
    //gbolSequence.setExonList(arg0);
  }
  
  public void setGene(Gene gene)
  {
    this.gene = gene;
  }
  
}
