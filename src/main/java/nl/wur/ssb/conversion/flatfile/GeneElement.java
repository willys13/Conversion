package nl.wur.ssb.conversion.flatfile;

import uk.ac.ebi.embl.api.entry.feature.Feature;

public class GeneElement
{
  public Feature gbFeature;
  public Long begin;
  public Long end;
  public GeneElement(Feature gbFeature)
  {
    this.gbFeature = gbFeature;
    if(this.gbFeature != null)
    {
      this.begin = this.gbFeature.getLocations().getMinPosition();
      this.end = this.gbFeature.getLocations().getMaxPosition();
    }
  }
}
