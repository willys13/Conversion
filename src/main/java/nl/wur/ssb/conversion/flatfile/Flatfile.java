package nl.wur.ssb.conversion.flatfile;

import java.io.BufferedReader;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Iterator;

import org.apache.log4j.Logger;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Save;
import nl.wur.ssb.SappGeneric.Store;
import nl.wur.ssb.conversion.App;
import nl.wur.ssb.conversion.options.CommandOptionsFlatfile;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.validation.Origin;
import uk.ac.ebi.embl.api.validation.ValidationMessage;
import uk.ac.ebi.embl.api.validation.ValidationResult;
import uk.ac.ebi.embl.flatfile.reader.EntryReader;
import uk.ac.ebi.embl.flatfile.reader.embl.EmblEntryReader;
import uk.ac.ebi.embl.flatfile.reader.genbank.GenbankEntryReader;

public class Flatfile
{    
  private CommandOptionsFlatfile args;
  private Domain domain;
  // private ConversionImportProv importProv;

  public Flatfile(String args[]) throws Exception
  {
    // this.domain = App.createDiskStore();
    this.domain = Store.createDiskStore();
       
    this.args = new CommandOptionsFlatfile(args);
    
    for(File file : this.args.importFiles)
    {
      RDFSimpleCon con = new RDFSimpleCon("file://" + file.getAbsolutePath());
      this.domain.getRDFSimpleCon().addAll(con);
      con.close();
    }
    
    Logger logger = Generic.Logger(this.args.debug);
    File[] inputs = {this.args.input};
    nl.wur.ssb.SappGeneric.ImportProv importProv = new nl.wur.ssb.SappGeneric.ImportProv(domain, inputs,  this.args.output, this.args.commandLine, this.args.toolName, this.args.toolVersion, this.args.repository, this.args.userAgentIri, this.args.starttime, LocalDateTime.now());
    //new ImportProv(domain, this.args, this.args.toolVersion, this.args.userAgentIri);
    
    BufferedReader reader = App.getStreamFromZip(this.args.input);
    
    EntryReader emblReader = null;
    if(this.args.genbank)
      emblReader = new GenbankEntryReader(reader);
    else if(this.args.embl)
      emblReader = new EmblEntryReader(reader);
    else
      throw new Exception("define either genbank or embl");
    
    ValidationResult validations = emblReader.read();
    
    // IF validation is empty.. good? (so far always been the case)
    if (!validations.getMessages().isEmpty())
    {
      Iterator<ValidationMessage<Origin>> valiter = validations.getMessages().iterator();
      int count = 0;
      while (valiter.hasNext())
      {
        count++;
        ValidationMessage<Origin> validation = valiter.next();
        App.logger.warn(validation.getMessage());
        if (count > 20)
        {
          App.logger.warn("And the list continues...");
          break;
        }
      }
    }
    
    // Starting the parser
    while (emblReader.isEntry())
    {
      logger.debug("Reading new entry");
      Entry entry = emblReader.getEntry();
      logger.debug(entry.getFeatures().size());
      new FlatFileEntry(domain, this.args.identifier,entry,importProv);
      emblReader.read();
    }
    
    importProv.finished();

    if (this.args.format.equals("ttl")) {
      domain.save(this.args.output.getAbsolutePath());
    } else if (this.args.format.equals("hdt")) {
      Save.save(domain, this.args.output);
    } 
    else
    {
      throw new Exception("invalid format");
    }
  }

  public Domain getDomain()
  {
    return domain;
  }

  public CommandOptionsFlatfile getArgs()
  {
    return args;
  }

}
