package nl.wur.ssb.conversion.flatfile;

import java.util.HashMap;
import java.util.LinkedList;

import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.location.Location;

public class Gene extends GeneElement
{
  public life.gbol.domain.Gene gbolFeature;
  private LinkedList<Exon> exonList = new LinkedList<Exon>();
  private LinkedList<Intron> intronList = new LinkedList<Intron>();
  private HashMap<String,Exon> exonMap = new HashMap<String,Exon>();

  public Gene(Feature gbFeature,life.gbol.domain.Gene gbolFeature)
  {
    super(gbFeature);
    this.gbolFeature = gbolFeature;
  }
  
  public void addExon(Exon toAdd)
  {
    this.exonList.add(toAdd);
    this.exonMap.put(toAdd.gbFeature.getLocations().getMinPosition() + "-" + toAdd.gbFeature.getLocations().getMaxPosition(),toAdd);
    //gbolSequence.setExonList(arg0);
  }
  
  public void addIntron(Intron toAdd)
  {
    this.intronList.add(toAdd);
  }
  
  public Exon getExon(Location loc)
  {
    return this.exonMap.get(loc.getBeginPosition() + "-" + loc.getEndPosition());
  }

  public LinkedList<Exon> getExonList()
  {
    return exonList;
  }

  public LinkedList<Intron> getIntronList()
  {
    return intronList;
  }  
}
