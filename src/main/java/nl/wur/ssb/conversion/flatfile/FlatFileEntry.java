package nl.wur.ssb.conversion.flatfile;

import java.time.ZoneId;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.purl.ontology.bibo.domain.Issue;
import org.purl.ontology.bibo.domain.Journal;

import life.gbol.domain.AnnotationLinkSet;
import life.gbol.domain.EntryType;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.GBOLDataSet;
import life.gbol.domain.NAFeature;
import life.gbol.domain.NASequence;
import life.gbol.domain.PublishedGBOLDataSet;
import life.gbol.domain.Region;
import life.gbol.domain.Topology;
import life.gbol.domain.XRefProvenance;
import life.gbol.domain.mRNA;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Xrefs;
import nl.wur.ssb.SappGeneric.GBOL.GBOLUtil;
import nl.wur.ssb.SappGeneric.GBOL.SequenceBuilder;
import nl.wur.ssb.conversion.App;
import nl.wur.ssb.conversion.options.CommandOptionsFlatfile;
import nl.wur.ssb.gbolclasses.Thing;
import uk.ac.ebi.embl.api.entry.AgpRow;
import uk.ac.ebi.embl.api.entry.Assembly;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.entry.Text;
import uk.ac.ebi.embl.api.entry.XRef;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.location.CompoundLocation;
import uk.ac.ebi.embl.api.entry.location.Location;
import uk.ac.ebi.embl.api.entry.qualifier.Qualifier;
import uk.ac.ebi.embl.api.entry.reference.Article;
import uk.ac.ebi.embl.api.entry.reference.Book;
import uk.ac.ebi.embl.api.entry.reference.ElectronicReference;
import uk.ac.ebi.embl.api.entry.reference.Patent;
import uk.ac.ebi.embl.api.entry.reference.Person;
import uk.ac.ebi.embl.api.entry.reference.Publication;
import uk.ac.ebi.embl.api.entry.reference.Reference;
import uk.ac.ebi.embl.api.entry.reference.Submission;
import uk.ac.ebi.embl.api.entry.reference.Thesis;
import uk.ac.ebi.embl.api.entry.reference.Unpublished;
import uk.ac.ebi.embl.api.entry.sequence.Sequence;
import uk.ac.ebi.embl.api.taxonomy.Taxon;

public class FlatFileEntry extends SequenceBuilder {


  private life.gbol.domain.Sample sample;

  private CommandOptionsFlatfile args;
  private Entry entry;
  // private Flatfile parent2;
  private GBOLDataSet dataset;
  private AnnotationLinkSet annotResult;
  private FeatureProvenance featureProv;
  private XRefProvenance xrefProv;

  public FlatFileEntry(Domain domain, String identifier, Entry entry, nl.wur.ssb.SappGeneric.ImportProv importProv)
      throws Exception {
    super(domain, "http://gbol.life/0.1/" + identifier + "/");
    // this.parent = parent;
    // this.args = parent.getArgs();
    this.entry = entry;
    rootIRI = "http://gbol.life/0.1/" + identifier + "/";


    // Create published data set if first public date is given
    if (entry.getFirstPublic() != null) {
      PublishedGBOLDataSet pubDataset =
          domain.make(PublishedGBOLDataSet.class, rootIRI + "GBOLDataSet");
      pubDataset.setLastPublishedDate(
          entry.getLastUpdated().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
      pubDataset.setFirstPublic(
          entry.getFirstPublic().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
      pubDataset.setFirstPublicRelease(entry.getFirstPublicRelease());
      pubDataset.setLastPublishedRelease(entry.getLastUpdatedRelease());

      if (entry.getHoldDate() != null)
        pubDataset.setHolddate(
            entry.getHoldDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
      if (entry.getStatus() != null)
        pubDataset.setReleaseStatus(entry.getStatus().toString());
      this.dataset = pubDataset;
    } else
      dataset = domain.make(GBOLDataSet.class, rootIRI + "GBOLDataSet");

    // TODO ! check if dataclass is correct (should be validated on next run...)
    if (this.entry.getDataClass() != null)
      this.dataset.setEntryType((EntryType) nl.wur.ssb.SappGeneric.GBOL.GBOLUtil
          .getEnum(EntryType.class, this.entry.getDataClass()));
    else
      dataset.setEntryType(life.gbol.domain.EntryType.StandardEntry);

    if (this.entry.getVersion() != null) {
      dataset.setDataSetVersion(this.entry.getVersion());
    } else {
      dataset.setDataSetVersion(-1);
    }

    importProv.linkEntity(dataset);
    annotResult = this.domain.make(AnnotationLinkSet.class, rootIRI + "AnnotationResult");
    importProv.linkEntity(annotResult);
    featureProv = this.domain.make(FeatureProvenance.class, rootIRI + "FeatureProv");
    featureProv.setOrigin(annotResult);
    xrefProv = this.domain.make(XRefProvenance.class, rootIRI + "XRefProv");
    xrefProv.setOrigin(annotResult);

    // Create sequence object
    this.dNASequence = buildSequence();

    // Create sample obejct
    App.logger.debug("Creating sample object");
    this.sample = buildSample();

    // Linking it to the corresponding sample
    this.dNASequence.setSample(sample);
    App.logger.debug("Setting sample: " + this.dNASequence.getResource().getURI() + "\t" + sample.getResource().getURI());

    for (XRef xref : entry.getXRefs()) {
      // As of April 2013, the supported DBLINK cross-reference types are "Project"
      // (predecessor of BioProject), "BioProject", "BioSample", "Trace Assembly Archive",
      // "Sequence Read Archive", and "Assembly".
      life.gbol.domain.XRef gbolXref = this.createXRef(xref);
      if (xref.getDatabase().equals("BioSample")) {
        sample.addXref(gbolXref);
      } else if (xref.getDatabase().equals("Trace Assembly Archive")
          || xref.getDatabase().equals("Sequence Read Archive")
          || xref.getDatabase().equals("Assembly")) {
        this.dNASequence.addXref(gbolXref);
        dataset.addXref(gbolXref);
      } else {
        dataset.addXref(gbolXref);
      }
    }
    // Add the keywords
    if (entry.getKeywords() != null) {
      for (Text key : entry.getKeywords()) {
        dataset.addKeywords(key.getText());
      }
    }

    if (entry.getDivision() != null) {
      App.logger.debug("Division is ignored as infomation is already captured in the taxonomy");
    }

    if (entry.getDataClass() != null) {
      dataset.setEntryType((EntryType) nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.getEnum(EntryType.class,
          entry.getDataClass()));
    }

    for (Reference ref : entry.getReferences()) {
      long seqLength = this.entry.getSequence().getLength();
      life.gbol.domain.Citation cit = this.buildCitation(ref);
      if (ref.getLocations().getMinPosition() != null && 
          !(ref.getLocations().getMinPosition() == 1
          && ref.getLocations().getMaxPosition() == seqLength)) {
        life.gbol.domain.GeneralFeature refFeature = this.domain.make(life.gbol.domain.GeneralFeature.class,this.rootIRI + "refGenFeature/" + ref.getReferenceNumber());
        refFeature.setLocation(this.makeRegion(ref.getLocations().getMinPosition(),ref.getLocations().getMaxPosition(),"loc",refFeature));
        refFeature.addCitation(cit);
        this.dNASequence.addFeature(refFeature);
      }
      dataset.addReferences(cit); // .getReference()
      refs.put(ref.getReferenceNumber(),cit);     
    }

    if (entry.getVersion() != null)
      dataset.setDataSetVersion(entry.getVersion());
    if (entry.getDescription() != null && entry.getDescription().getText() != null)
      if (!entry.getDescription().getText().trim().equals(""))
        dataset.setDescription(entry.getDescription().getText());
    if (entry.getComment().getText() != null && !entry.getComment().getText().trim().equals(""))
      dataset.setComment(entry.getComment().getText());

    dataset.addAnnotationResults(annotResult);
    dataset.addSamples(sample);
    dataset.addSequences(this.dNASequence);

    /*
     * private String id; -> private private
     * 
     * Origin origin; -> private private
     * 
     * String primaryAccession;
     * 
     * TODO ? private Integer version;
     * 
     * V private Date holdDate;
     * 
     * V private Date ffDate; -> not used private Status status;
     * 
     * V private Date firstPublic;
     * 
     * V private Date lastUpdated;
     * 
     * V private Integer firstPublicRelease;
     * 
     * V private Integer lastUpdatedRelease;
     * 
     * V private String dataClass;
     * 
     * V private String division;
     * 
     * V -> derived from taxon private Text description;
     * 
     * V private Text comment;
     * 
     * V private Sequence sequence;
     * 
     * TODO ? private String submitterAccession;
     * 
     * TODO ? private Integer submitterWgsVersion;
     * 
     * TODO ? private boolean deleteEntry;
     * 
     * -> private private long idLineSequenceLength;
     * 
     * TODO ? private List<Text> secondaryAccessions;
     * 
     * TODO ? private List<Text> keywords;
     * 
     * V private List<Text> projectAccessions;
     * 
     * TODO ? private List<Feature> features;
     * 
     * TODO ? private List<Reference> references;
     * 
     * V private List<XRef> xRefs;
     * 
     * V private List<Text> masterConAccessions;
     * 
     * TODO ? private List<Text> masterWgsAccessions;
     * 
     * TODO ? private List<Text> masterTpaAccessions;
     * 
     * TODO ? private List<Text> masterTsaAccessions;
     * 
     * TODO ? protected List<Assembly> assemblies;
     * 
     * TODO ? protected List<AgpRow> agpRows;
     * 
     * TODO ? private boolean isAnnotationOnlyCON=false;
     * 
     * -> private private boolean isSingletonAgp=false;
     * 
     * -> private private boolean isNonExpandedCON=false; -> private
     */

    // Building the features instance on top of the dNASequence of
    // whatever type...
    buildFeatures();
  }

  public life.gbol.domain.Sample getSample() {
    return sample;
  }

  public NASequence getdNASequence() {
    return dNASequence;
  }

  public CommandOptionsFlatfile getArgs() {
    return args;
  }

  public Entry getEntry() {
    return entry;
  }
  
  public life.gbol.domain.XRef createXRef(XRef xref) throws Exception {
    return this.createXRef(xref.getDatabase(), xref.getPrimaryAccession(),
        xref.getSecondaryAccession());
  }

  public life.gbol.domain.XRef createXRef(String dbIdentifier, String primary) throws Exception {
    return this.createXRef(dbIdentifier, primary, null);
  }

  public life.gbol.domain.XRef createXRef(String dbIdentifier, String primary, String secondary)
      throws Exception {
    life.gbol.domain.XRef toRet =
        Xrefs.create(this.domain, this.xrefProv, dbIdentifier, primary, secondary);
    this.annotResult.addTarget(toRet.getDb());
    dataset.addLinkedDataBases(toRet.getDb());
    return toRet;
  }

  public void finishFeature(Feature gbFeature, life.gbol.domain.Feature naFeature)
      throws Exception {
    naFeature.setLocation(makeLocation(gbFeature, naFeature));
    // TODO testing if this adds the featureProv to all the features
    naFeature.addProvenance(featureProv);

    if (gbFeature.getXRefs() != null) {
      for (XRef xref : gbFeature.getXRefs()) {
        naFeature.addXref(this.createXRef(xref));
      }
    }
    dNASequence.addFeature(naFeature);
  }

  public life.gbol.domain.Sample buildSample() throws Exception {

    life.gbol.domain.Sample sample =
        domain.make(life.gbol.domain.Sample.class, this.rootIRI + "sample");

    this.sample = sample;
    // TODO ? find example for AGP files...
    if (entry.getAgpRows().size() > 0) {
      for (AgpRow agp : entry.getAgpRows()) {
        App.logger.debug(agp.toString());
      }
      throw new Exception("What to do with AGP?");
    }

    // TODO ? find example for Assembly files...
    if (!entry.getAssemblies().isEmpty()) {
      for (Assembly assembly : entry.getAssemblies()) {
        App.logger.debug("Assembly getId() " + assembly.getId());
      }
      throw new Exception("What to do with Assembly...");
    }

    nl.wur.ssb.gbolclasses.Sample pSample = new nl.wur.ssb.gbolclasses.Sample(this);
    pSample.parseQualifiers(entry.getPrimarySourceFeature());
    
    //Set the main taxonomy reference
    if (entry.getPrimarySourceFeature() != null) {
      Taxon taxon = entry.getPrimarySourceFeature().getTaxon();
      if (taxon != null) {
        life.gbol.domain.TaxonomyRef taxonRef = (life.gbol.domain.TaxonomyRef)
            Xrefs.create(life.gbol.domain.TaxonomyRef.class,this.domain, this.xrefProv, "taxonomy","" + taxon.getTaxId(),null);
        this.annotResult.addTarget(taxonRef.getDb());
        dataset.addLinkedDataBases(taxonRef.getDb());
        life.gbol.domain.Organism organism = this.domain.make(life.gbol.domain.Organism.class,this.rootIRI + "organism");
        organism.setTaxonomy(taxonRef);
        this.dataset.addOrganisms(organism);
        this.dNASequence.setOrganism(organism);
        if(taxon.getScientificName() != null && !taxon.getScientificName().equals(""))
          organism.setScientificName(taxon.getScientificName());
        if(taxon.getLineage() != null)
        {
          String lineageParts[] = taxon.getLineage().split(";");
          for(int i = 0;i < lineageParts.length;i++)
          {
            String part = lineageParts[i].trim();
            if(part.equals(""))
              continue;
            life.gbol.domain.Taxon gbolTaxon = this.domain.make(life.gbol.domain.Taxon.class,this.rootIRI + "organism/taxon" + i);
            gbolTaxon.setTaxonName(part);
            gbolTaxon.setTaxonRank(life.gbol.domain.Rank.make("http://gbol.life/0.1/RankLevel" + (i + 1)));
            organism.addTaxonomyLineage(gbolTaxon);
          }
        }
      }
       
      for (XRef crossRef : entry.getPrimarySourceFeature().getXRefs()) {
        sample.addXref(this.createXRef(crossRef));
      }
    }
    return sample;
  }
  
  private void doSingleFeature(Feature feature, String name, Consumer<String> lambda) {
    List<Qualifier> quals = feature.getQualifiers(name);
    if (quals.size() > 1)
      App.logger.error("Qualifier: " + name + " only expected once but encountered it "
          + quals.size() + " times");
    if (quals.size() >= 1) {
      lambda.accept(quals.get(0).getValue());
    }
  }

  public String buildSubSequence(CompoundLocation<Location> locations, boolean complement)
      throws Exception {

    String subseq = "";
    // FROM GFF parser CDS were not always sorted
    for (Location loc : locations.getSortedLocations().getLocations()) {
//    for (Location loc : locations.getLocations()) {
      long begin = loc.getBeginPosition();
      long end = loc.getEndPosition();
      if (!complement) {
        subseq = subseq + new String(entry.getSequence().getSequenceByte(begin, end));
      }
      else {
//        System.err.println("DEBUG: "+begin+"\t"+end+"\t"+new String(entry.getSequence().getReverseComplementSequenceByte(begin, end)));
        subseq = new String(entry.getSequence().getReverseComplementSequenceByte(begin, end)) + subseq;
      }
    }
//    if (complement)
//     System.err.println("DEBUGGING: "+ subseq);
    return subseq;
  }

  public NASequence buildSequence() throws Exception {
    /*
     * taxonomy @:TaxonomyRef?; strandType type::StrandType; translTable xsd:PositiveInteger?;
     * integratedInto @:NASequence?; chromosome xsd:String?; plasmidName xsd:String?;
     */
    /*
     * Creation of the DNASequence...
     */


    // Couple NASequence to Sample
    String primAccession = entry.getPrimaryAccession();
    // MD5 for the URI (to keep it small, but as it is linked to the unique
    // genome ID it should be unique enough... Proteins will be sha384 by URI
    if (entry.getPrimaryAccession() == null) {
      primAccession = Generic.checksum(entry.getSequence().toString(), "md5");
      App.logger.warn("No primary accession available using: " + primAccession);
    }

    life.gbol.domain.NASequence naSequence = null;
    if (entry.getPrimarySourceFeature() != null) {
      if (!entry.getPrimarySourceFeature().getQualifiers("plasmid").isEmpty()) {
        naSequence = domain.make(life.gbol.domain.Plasmid.class,
            this.rootIRI + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(primAccession));
      } else if (!entry.getPrimarySourceFeature().getQualifiers("chromosome").isEmpty()) {
        naSequence = domain.make(life.gbol.domain.Chromosome.class,
            this.rootIRI + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(primAccession));
      } // TODO > scaffold (need example)
    }

    if (naSequence == null) {
      naSequence = domain.make(life.gbol.domain.Contig.class,
          this.rootIRI + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(primAccession));
    }
    // Updating the contigURI
    this.featureURI = naSequence.getResource().getURI() + "/";

    naSequence.addAccession(primAccession);

    // Setting sequence
    if (entry.getSequence().getSequenceByte() != null) {
      if (new String(entry.getSequence().getSequenceByte()).toUpperCase() != null) {
        String seq = new String(entry.getSequence().getSequenceByte()).toUpperCase();
        naSequence.setSequence(seq);
        naSequence.setSha384(Generic.checksum(seq, "SHA-384"));
        naSequence.setLength((long) seq.length());
      }
    }

    if (entry.getSequence().getAccession() != null) {
      naSequence.addAccession(entry.getSequence().getAccession());
    }

    if (entry.getSequence().getGIAccession() != null) {
      naSequence.addXref(this.createXRef("gi", entry.getSequence().getGIAccession()));
    }

    // TODO ? Is this still needed? This is allowed: "genomic DNA", "genomic RNA", "mRNA", "tRNA",
    // "rRNA", "other
    // RNA", "other DNA", "transcribed RNA", "viral cRNA", "unassigned
    // DNA", "unassigned RNA"
    if (entry.getSequence().getMoleculeType() != null) {
      // NASequence.setMolType(entry.getSequence().getMoleculeType());
    } else {
      // NASequence.setMolType("unassigned DNA");
    }

    if (entry.getSequence().getVersion() != null) {
      naSequence.setSequenceVersion(entry.getSequence().getVersion());
    }
    if (entry.getSequence().getTopology() != null) {
      if (entry.getSequence().getTopology() == Sequence.Topology.LINEAR) {
        naSequence.setTopology(Topology.Linear);
      } else if (entry.getSequence().getTopology() == Sequence.Topology.CIRCULAR) {
        naSequence.setTopology(Topology.Circular);
      } else {
        naSequence.setTopology(Topology.Linear);
        App.logger.warn("Topology is set to " + naSequence.getTopology());
      }
    } else {
      // Default it is linear...
      naSequence.setTopology(Topology.Linear);
    }

    if (entry.getDescription() != null && entry.getDescription().getText() != null) {
      naSequence.setDescription(entry.getDescription().getText());
    }

    return naSequence;
  }
  
  public life.gbol.domain.Citation buildCitation(Reference ref) throws Exception 
  {
    org.purl.ontology.bibo.domain.Document gbolRef = this.buildRef(ref);
    life.gbol.domain.Citation toRet = domain.make(life.gbol.domain.Citation.class,
        this.rootIRI + "citation/" + ref.getReferenceNumber());
    toRet.setProvenance(this.featureProv);
    toRet.setReference(gbolRef);
    return toRet;
  }

  public org.purl.ontology.bibo.domain.Document buildRef(Reference ref) throws Exception {
    Publication publication = ref.getPublication();
    org.purl.ontology.bibo.domain.Document toRet = null;
    if (publication instanceof Article) {
      Article article = (Article) publication;
      toRet = domain.make(org.purl.ontology.bibo.domain.AcademicArticle.class,
          this.rootIRI + "academic/article/" + ref.getReferenceNumber());
      if (article.getFirstPage() != null && !article.getFirstPage().trim().equals(""))
        toRet.setPageStart(article.getFirstPage());
      if (article.getLastPage() != null && !article.getLastPage().trim().equals(""))
        toRet.setPageEnd(article.getLastPage());
      if (article.getVolume() != null && !article.getVolume().trim().equals(""))
        toRet.setVolume(article.getVolume());
      String journalName = article.getJournal();
      Journal journal = null;
      if (journalName != null && !journalName.trim().equals("")) {
        // If we see more then 3 numbers then it can not be a valid journal name
        if (journalName.matches("\\d\\d\\d"))
          App.logger.error("Invalid journal name:" + journalName);
        journal = domain.make(Journal.class, this.rootIRI + "journal/"
            + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(article.getJournal()));
        journal.setTitle(article.getJournal());
      }

      String issueName = article.getIssue();
      if ((issueName == null || issueName.trim().equals("")) && journal != null)
        issueName = "unknown issue in journal " + article.getJournal();
      Issue issue = null;
      if (issueName != null && !issueName.trim().equals("")) {
        issue = domain.make(Issue.class, this.rootIRI + "issue/"
            + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(article.getJournal()));
        issue.setTitle(issueName);
        if (article.getYear() != null)
          issue.setIssued(
              article.getYear().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        issue.addHasPart(toRet);
        if (journal != null)
          journal.addHasPart(issue);
      }
    } else if (publication instanceof Book) {
      Book book = (Book) publication;
      toRet = domain.make(org.purl.ontology.bibo.domain.Book.class,
          this.rootIRI + "ref/" + ref.getReferenceNumber());
      if (book.getFirstPage() != null && !book.getFirstPage().trim().equals(""))
        toRet.setPageStart(book.getFirstPage());
      if (book.getLastPage() != null && !book.getLastPage().trim().equals(""))
        toRet.setPageEnd(book.getLastPage());
      if (book.getPublisher() != null && !book.getPublisher().trim().equals(""))
        toRet.setPublisher(this.makeOrganizatioin(book.getPublisher()));
      if (book.getYear() != null)
        toRet.setIssued(book.getYear().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
      if (book.getBookTitle() != null && !book.getBookTitle().trim().equals(""))
        toRet.setTitle(book.getBookTitle());
    } else if (publication instanceof ElectronicReference) {
      ElectronicReference elecRef = (ElectronicReference) publication;
      toRet = domain.make(org.purl.ontology.bibo.domain.Webpage.class,
          this.rootIRI + "ref/" + ref.getReferenceNumber());
      if (elecRef.getText() == null || elecRef.getText().trim().equals(""))
        ;
      App.logger.error("Electronic reference with no IRI");
      toRet.setUri(elecRef.getText());
    } else if (publication instanceof Patent) {
      Patent patent = (Patent) publication;
      toRet = domain.make(org.purl.ontology.bibo.domain.Patent.class,
          this.rootIRI + "ref/" + ref.getReferenceNumber());

      // paten sequeneNumber & patentType are not stored

      if (patent.getPatentOffice() != null && !patent.getPatentOffice().trim().equals(""))
        toRet.setIssuer(this.makeOrganizatioin(patent.getPatentOffice()));

      if (patent.getPatentNumber() != null && !patent.getPatentNumber().trim().equals(""))
        toRet.setNumber(patent.getPatentNumber());

      if (patent.getDay() != null)
        toRet.setIssued(patent.getDay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

      for (String author : patent.getApplicants()) {
        toRet.addAuthorList(makeAuthor(author));
      }
    } else if (publication instanceof Submission) {
      Submission submission = (Submission) publication;
      toRet = domain.make(org.purl.ontology.bibo.domain.Manuscript.class,
          this.rootIRI + "ref/" + ref.getReferenceNumber());

      if (submission.getDay() != null)
        toRet.setDateSubmitted(
            submission.getDay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

      // submitterAddress not stored no support in bibo for this :(
    } else if (publication instanceof Thesis) {
      Thesis thesis = (Thesis) publication;
      toRet = domain.make(org.purl.ontology.bibo.domain.Thesis.class,
          this.rootIRI + "ref/" + ref.getReferenceNumber());

      if (thesis.getYear() != null)
        toRet.setIssued(thesis.getYear().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

      if (thesis.getInstitute() != null && !thesis.getInstitute().trim().equals(""))
        toRet.setPublisher(this.makeOrganizatioin(thesis.getInstitute()));

    } else if (publication instanceof Unpublished) {
      Unpublished article = (Unpublished) publication;
      toRet = domain.make(org.purl.ontology.bibo.domain.Manuscript.class,
          this.rootIRI + "ref/" + ref.getReferenceNumber());
    }

    if (publication.getId() != null && !publication.getId().trim().equals(""))
      toRet.setIdentifier(publication.getId());
    if (publication.getTitle() != null && !publication.getTitle().trim().equals(""))
      toRet.setTitle(publication.getTitle());
    if (publication.getConsortium() != null && !publication.getConsortium().trim().equals(""))
      toRet.addContributor(this.makeOrganizatioin(publication.getConsortium()));

    for (Person author : publication.getAuthors()) {
      toRet.addAuthorList(makeAuthor(author));
    }

    for (XRef crossRef : publication.getXRefs()) {
      if (crossRef.getDatabase().equals("PUBMED")) {
        toRet.setPmid(crossRef.getPrimaryAccession());
      } else
        App.logger.debug("Detected xref other then PUBMED on reference: " + crossRef.getDatabase());
    }
    return toRet;
  }

  public org.w3.ns.prov.domain.Person makeAuthor(Person author) {
    String personName = "";
    if (author.getFirstName() != null)
      personName += author.getFirstName() + " ";
    if (author.getSurname() != null)
      personName += author.getSurname();
    personName = personName.trim();

    if (personName.equals(""))
      return null;

    org.w3.ns.prov.domain.Person toRet = domain.make(org.w3.ns.prov.domain.Person.class,
        this.rootIRI + "person/" + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(personName));
    if (author.getFirstName() != null)
      toRet.setFirstName(author.getFirstName());
    if (author.getSurname() != null)
      toRet.setSurName(author.getSurname());
    return toRet;
  }

  public org.w3.ns.prov.domain.Organization makeOrganizatioin(String name) {
    org.w3.ns.prov.domain.Organization toRet = domain.make(org.w3.ns.prov.domain.Organization.class,
        this.rootIRI + "org/" + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(name));
    toRet.setName(name);
    toRet.setLegalName(name);
    return toRet;
  }

  public org.w3.ns.prov.domain.Agent makeAgent(String name) {
    org.w3.ns.prov.domain.Agent toRet = domain.make(org.w3.ns.prov.domain.Agent.class,
        this.rootIRI + "org/" + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(name));
    toRet.setName(name);
    return toRet;
  }

  public org.w3.ns.prov.domain.Person makeAuthor(String name) {
    org.w3.ns.prov.domain.Person toRet = domain.make(org.w3.ns.prov.domain.Person.class,
        this.rootIRI + "org/" + nl.wur.ssb.SappGeneric.GBOL.GBOLUtil.cleanURI(name));
    toRet.setName(name);
    return toRet;
  }

  public boolean isAssociatedElem(Feature feature, GeneElement fitsInto) {
    long left = feature.getLocations().getMinPosition();
    long right = feature.getLocations().getMaxPosition();
    return left >= fitsInto.gbFeature.getLocations().getMinPosition()
        && right <= fitsInto.gbFeature.getLocations().getMaxPosition();
  }

  public GeneElement getAssociatedElem(LinkedList<? extends GeneElement> searchIn,
      Feature feature) {
    long left = feature.getLocations().getMinPosition();
    long right = feature.getLocations().getMaxPosition();
    for (GeneElement element : searchIn) {
      if (left >= element.gbFeature.getLocations().getMinPosition()
          && right <= element.gbFeature.getLocations().getMaxPosition())
        return element;
    }
    return null;
  }

  static Map<String, Exon> exonLookup = new HashMap<String, Exon>();

  public void buildFeatures() throws Exception {

    /*
     * For each feature... Creating a GENEOBJECT with (dNASequenceURI / MIN - MAX position)
     */
    LinkedList<Exon> exons = new LinkedList<Exon>();
    LinkedList<Intron> introns = new LinkedList<Intron>();
    LinkedList<Gene> genes = new LinkedList<Gene>();
    LinkedList<Transcript> transcripts = new LinkedList<Transcript>();
    LinkedList<CDS> cds = new LinkedList<CDS>();
    LinkedList<Thing> withDelayedProps = new LinkedList<Thing>();

    for (Feature feature : entry.getFeatures()) {
      // (mrna|ncrna|rrna|tmrna|trna|misc_rna|precursor_rna|prim_transcript)
      String name = feature.getName();
      String className = featureMap.get(name);
      
   
      // Only accepted class names for features?...
      if (className != null) {
        Object gbolInstance = null;
        
        if (name.matches("(enhancer|promoter|CAAT_signal|TATA_signal|-35_signal|-10_signal|RBS|GC_signal|polyA_signal|attenuator|terminator|misc_signal)")) {
          /*
           * This feature has replaced the following Feature Keys on 15-DEC-2014:
                        enhancer, promoter, CAAT_signal, TATA_signal, -35_signal, -10_signal,
                        RBS, GC_signal, polyA_signal, attenuator, terminator, misc_signal.
           */
          gbolInstance = domain.make(life.gbol.domain.RegulationSite.class, this.makeIRIFeature(feature));
          ((life.gbol.domain.RegulationSite)gbolInstance).setRegulatoryClass((life.gbol.domain.RegulatoryClass)GBOLUtil.getEnum(life.gbol.domain.RegulatoryClass.class, className));
          className = "RegulationSite";
        } else {       
          gbolInstance = domain.make(Class.forName("life.gbol.domain." + className), this.makeIRIFeature(feature));
        }
        
        Class clazz = null;
        try {
          clazz = Class.forName("nl.wur.ssb.gbolclasses.features." + className);
        } catch (ClassNotFoundException e) {
          clazz = Class.forName("nl.wur.ssb.gbolclasses.sequences." + className);
        }
  
        Thing visitor = (Thing) clazz
            .getConstructor(SequenceBuilder.class, gbolInstance.getClass().getInterfaces()[0])
            .newInstance(this, gbolInstance);
  
        if (!name.toLowerCase().equals("source"))
          visitor.parseQualifiers(feature);
        
        if(visitor.hasDelayedProps())
          withDelayedProps.add(visitor);
  
        if (className.toLowerCase().equals("cds")) {
          CDS cdsElem = new CDS(feature, (life.gbol.domain.CDS) gbolInstance);
          cds.add(cdsElem);
          if (!genes.isEmpty() && isAssociatedElem(feature, genes.getLast()))
            cdsElem.gene = genes.getLast();
          if (!transcripts.isEmpty() && isAssociatedElem(feature, transcripts.getLast()))
            cdsElem.transcript = transcripts.getLast();
        } else if (name.toLowerCase().matches("(misc_rna|miscrna|mrna|ncrna|precursorrna|rrna|tmrna|trna)")) {
          Transcript transcript = new Transcript(feature, (life.gbol.domain.Transcript) gbolInstance);
          transcripts.add(transcript);
          if (!genes.isEmpty() && isAssociatedElem(feature, genes.getLast()))
            transcript.setGene(genes.getLast());
        } else if (feature.getName().toLowerCase().equals("exon")) {
          exons.add(new Exon(feature, (life.gbol.domain.Exon) gbolInstance));
          finishFeature(feature, (NAFeature) gbolInstance);
        } else if (feature.getName().toLowerCase().equals("intron")) {
          introns.add(new Intron(feature, (life.gbol.domain.Intron) gbolInstance));
          finishFeature(feature, (NAFeature) gbolInstance);
        } else if (feature.getName().toLowerCase().equals("gene")) {
          genes.add(new Gene(feature, (life.gbol.domain.Gene) gbolInstance));
          finishFeature(feature, (NAFeature) gbolInstance);
        } else if (name.toLowerCase().matches(
            "(variableregion|variablesegment|constantregion|diversitysegment|switchregion|joiningsegment|gapregion)")) {
          throw new Exception("Conversion of protein properties not implemented yet");
        } else if (name.equals("source")) {
          // TODO ? something with source
        } else {
          finishFeature(feature, (life.gbol.domain.Feature) gbolInstance);
        }
  
  
      }
    }
    
    for (Exon exon : exons) {
      Gene gene = (Gene) getAssociatedElem(genes, exon.gbFeature);
      if (gene == null)
        App.logger.error("EXON could not be matched to gene: " + exon.gbFeature.getLocations());
      else
        gene.addExon(exon);
    }
    for (Intron intron : introns) {
      Gene gene = (Gene) getAssociatedElem(genes, intron.gbFeature);
      if (gene == null)
        App.logger.error("Intron could not be matched to gene: " + intron.gbFeature.getLocations());
      else
        gene.addIntron(intron);
    }
    for (CDS cdsElem : cds) {
      if (cdsElem.transcript == null) {
        // first try to find it
        Transcript transcript = (Transcript) this.getAssociatedElem(transcripts, cdsElem.gbFeature);
        if (transcript == null) {
          // create new one
          mRNA gbolTranscript = domain.make(life.gbol.domain.mRNA.class,
              this.makeIRIFeature(cdsElem.gbFeature, "generatedMRna"));
          transcript = new Transcript(cdsElem.gbFeature, gbolTranscript);
          transcripts.add(transcript);
          transcript.gene = cdsElem.gene;
        }
        // link the transcript the CDS
        cdsElem.transcript = transcript;
      }
    }

    for (Transcript transcript : transcripts) {
      if (transcript.gene == null) {
        // first try to find it
        Gene gene = (Gene) this.getAssociatedElem(genes, transcript.gbFeature);
        if (gene == null) {
          // create new one
          life.gbol.domain.Gene gbolGene = domain.make(life.gbol.domain.Gene.class,
              this.makeIRIFeature(transcript.gbFeature, "generatedGene"));
          gene = new Gene(transcript.gbFeature, gbolGene);
          genes.add(gene);
          gbolGene.setLocation(makeLocation(transcript.gbFeature, gbolGene));
          this.dNASequence.addFeature(gbolGene);
        }
        // link the transcript the CDS
        transcript.gene = gene;
      }
      // Attach the exon list to the transcript
      this.makeExonList(transcript.gbFeature, transcript.gene, transcript);
      // and the link the transcript to the gene
      transcript.gene.gbolFeature.addTranscript(transcript.gbolSequence);

      // transcript.createMissingExons(exons.size() > 0);
    }
    for (Gene gene : genes) {
      for (Intron intron : gene.getIntronList()) {
        gene.gbolFeature.addIntron(intron.gbolFeature);
      }
      for (Exon exon : gene.getExonList()) {
        gene.gbolFeature.addExon(exon.gbolFeature);
      }
      // TODO ! create missing introns based on the exon list
      // Sorted exon list and track orientation
      // exons = gene.getExonList();
      // for (int i = 0; i < exons.size() - 1; i ++) {
      // long currentExonEnd = exons.get(i).end;
      // long nextExonBegin = exons.get(i + 1).begin;
      // Intron intron =
      // domain.make(life.gbol.domain.Intron.class, this.dNASequence.getResource().getURI()
      // + "/intron/" + currentExonEnd + 1 + "-" + nextExonBegin);
      // }
    }
    for (CDS cdsElem : cds) {
      Region region = null;
      long begin;
      long end;
      long transcriptEnd = cdsElem.transcript.gbFeature.getLocations().getMaxPosition();
      long transcriptBegin = cdsElem.transcript.gbFeature.getLocations().getMinPosition();
      long dnaLength = transcriptEnd - transcriptBegin;
      long mrnaLength = cdsElem.transcript.gbolSequence.getLength();
      long intronLength = dnaLength - mrnaLength;
      long correction = 1;
      long cdsBegin = cdsElem.gbFeature.getLocations().getMinPosition();
      long cdsEnd = cdsElem.gbFeature.getLocations().getMaxPosition();

      if (cdsElem.transcript.isReverse) {
        long cdsOnTranscriptEnd =
            (transcriptEnd - (transcriptEnd - cdsEnd) - intronLength) - transcriptBegin;
        long cdsOnTranscriptBegin = transcriptBegin - cdsBegin + correction;

        begin = cdsOnTranscriptBegin;
        end = cdsOnTranscriptEnd;

      } else {
        begin = cdsBegin - transcriptBegin + 1;
        end = cdsEnd - transcriptBegin + 1;
      }
      // System.err.println(cdsElem.transcript.isReverse + "\t" + begin + "\t" + end);

      cdsElem.gbolFeature.setLocation(this.makeRegion(begin, end, "CDS", cdsElem.gbolFeature));
      if (begin > 1) {
        life.gbol.domain.FivePrimeUTR fiveUTR = domain.make(life.gbol.domain.FivePrimeUTR.class,
            cdsElem.gbolFeature.getResource().getURI() + "/fiveUTR");
        fiveUTR.setLocation(this.makeRegion(1, begin - 1, "fiveUTR/Loc", cdsElem.gbolFeature));
        cdsElem.transcript.gbolSequence.addFeature(fiveUTR);
      }
      
      long length = cdsElem.transcript.gbolSequence.getSequence().length();
      if (transcriptEnd != cdsEnd) {
        life.gbol.domain.ThreePrimeUTR threeUTR = domain.make(life.gbol.domain.ThreePrimeUTR.class,
            cdsElem.gbolFeature.getResource().getURI() + "/threeUTR");
        threeUTR.setLocation(this.makeRegion(end + 1, length, "threeUTR/Loc", cdsElem.gbolFeature));
        cdsElem.transcript.gbolSequence.addFeature(threeUTR);
      }

      CompoundLocation<Location> locs = cdsElem.gbFeature.getLocations();
      
      //TODO use codon start if present

      // This was a fix required for complemtation
      boolean complement = false;
      if (cdsElem.gbFeature.getLocations().isComplement() || cdsElem.gbFeature.getLocations().getLocations().get(0).isComplement())
        complement = true;

      String subseq = buildSubSequence(locs, complement);

      this.makeProtein(cdsElem.gbolFeature, cdsElem.gbFeature, subseq);
      cdsElem.transcript.gbolSequence.addFeature(cdsElem.gbolFeature);
      // TODO > Protein features should not be set on the proteins as the proteins are generic
      // identifiers
    }
    
    //All elements are connected to deal with props that need this information
    for(Thing elem : withDelayedProps)
    {
      elem.handleDelayedProps();
    }
  }

  public void makeExonList(Feature gbTranscript, Gene gene, Transcript transcript)
      throws Exception {
    CompoundLocation<Location> locObj = gbTranscript.getLocations();

    boolean isComplement = locObj.isComplement();
    List<Location> locations = locObj.getLocations();

    // LinkedList<life.gbol.domain.Region> regions = new LinkedList<life.gbol.domain.Region>();
    // LinkedList<String> iriParts = new LinkedList<String>();
    boolean isFirst = true;
    boolean complementVal = false;
    LinkedList<Exon> exons = new LinkedList<Exon>();
    for (int i = 0; i < locations.size(); i++) {
      Location loc = locations.get(i);
      if (isFirst) {
        complementVal = loc.isComplement();
        isFirst = false;
      } else if (complementVal != loc.isComplement()) {
        App.logger.error("Exon list with varying complementation: "
            + transcript.gbolSequence.getResource().getURI());
      }
      Exon exon = gene.getExon(loc);
      if (exon == null) {
        life.gbol.domain.Exon gbolExon = domain.make(life.gbol.domain.Exon.class,
            this.dNASequence.getResource().getURI() + "/generatedExon/"
                + (loc.isComplement() ^ isComplement ? "c" : "") + loc.getBeginPosition() + "-"
                + loc.getEndPosition());

        gbolExon.setLocation(
            makeRegion(locations.get(i), isComplement, i == 0 && locObj.isLeftPartial(),
                i == locations.size() - 1 && locObj.isRightPartial(), transcript.gbolSequence));
        this.dNASequence.addFeature(gbolExon);
        exon = new Exon(null, gbolExon);
        exon.begin = locations.get(i).getBeginPosition();
        exon.end = locations.get(i).getEndPosition();
        gene.gbolFeature.addExon(exon.gbolFeature);
      }
      exons.add(exon);
    }
    transcript.isReverse = isComplement ^ complementVal;
    if (transcript.isReverse)
      Collections.reverse(exons);

    // There is only one exon list per transcript
    life.gbol.domain.ExonList exonList = domain.make(life.gbol.domain.ExonList.class,
        transcript.gbolSequence.getResource().getURI() + "/exonlist");
    String sequence = "";

    for (Exon exon : exons) {
      // gbFeature == null
      long begin = exon.begin;
      long end = exon.end;
      if (transcript.isReverse)
        sequence += new String(entry.getSequence().getReverseComplementSequenceByte(begin, end));
      else
        sequence += new String(entry.getSequence().getSequenceByte(begin, end));
      exonList.addExon(exon.gbolFeature);
    }
    setSequence(transcript.gbolSequence, sequence);
    transcript.gbolSequence.setExonList(exonList);
  }
  
  private String getOtherLocus(Domain domain, String locus, int begin, int end) throws Exception {
    RDFSimpleCon simplecon = domain.getRDFSimpleCon();
    if (locus.contains("TEST")) {
      for (ResultLine result : simplecon.runQuery("getLocusTagBeginEnds.txt", true, locus)) {
        int beginpos = result.getLitInt("beginpos");
        int endpos = result.getLitInt("endpos");
        if (beginpos != begin && beginpos != end) {
          if (endpos != begin && endpos != end) {
            // These are completely different but same locus tag?...
            App.logger.error("Locus tag already in use by other gene... with as value: " + locus);
            // System.err.println(beginpos + "\t" + begin);
            // System.err.println(endpos + "\t" + end);
            locus = locus + "_" + this.args.locus;
            App.logger.error("Now setting to: " + locus);
            this.args.locus++;
            return locus;
          }
        }
      }
    }
    return locus;
  }
}
