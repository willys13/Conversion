package nl.wur.ssb.conversion.togtf;

import java.io.PrintWriter;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.conversion.options.CommandOptionsToGTF;

import org.rdfhdt.hdt.hdt.HDTManager;

public class ToGTF {
  static RDFSimpleCon RDFResource;

  public static void app(CommandOptionsToGTF arguments) throws Exception {
    RDFResource = new RDFSimpleCon("");

    // LOAD HDT file...
    org.rdfhdt.hdt.hdt.HDT hdt = HDTManager.mapIndexedHDT(arguments.gbol.getAbsolutePath(), null);

    PrintWriter fastaWriter = new PrintWriter(arguments.fasta, "UTF-8");
    PrintWriter gtfWriter = new PrintWriter(arguments.gtf, "UTF-8");

    gtfWriter.write("##description: Annotation through GBOL - SAPP\n");
    gtfWriter.write("##provider: SAPP\n");
    gtfWriter.write("##contact: jasperkoehorst@gmail.com\n");
    gtfWriter.write("##format: gtf\n");
    gtfWriter.write("##date: 0000-00-00\n");
    gtfWriter.write("##Created by us...\n");
    // SELECT DISTINCT ?naobject ?sequence ?accession ?description ?version ?scientificname ?lineage
    // ?dataclass ?division ?topology ?moleculetype
    for (ResultLine contig : RDFResource.runQuery(hdt, "getContigs.txt")) {
      String sequence = contig.getLitString("sequence");
      String accession = contig.getLitString("accession");
      String FASTA = ">" + accession + "\n" + sequence + "\n";
      // Write to FASTA file...
      fastaWriter.print(FASTA);

      // SELECT DISTINCT ?feature ?ftype
      String iri = contig.getIRI("naobject");

      for (ResultLine feat : RDFResource.runQuery(hdt, "getFeatures.txt", iri)) {

        String feature = feat.getIRI("ftype");
        feature = feature.replaceAll("http://gbol.life/0.1/", "");

        // SELECT DISTINCT ?begin ?end ?strand
        for (ResultLine geneloc : RDFResource.runQuery(hdt, "getGenePositions.txt",
            feat.getIRI("feature"))) {
          int begin = geneloc.getLitInt("begin");
          int end = geneloc.getLitInt("end");
          String strand = geneloc.getIRI("strand");
          if (strand.contains("ForwardStrandPosition"))
            strand = "+";
          if (strand.contains("ReverseStrandPosition"))
            strand = "-";

          String score = ".";
          String frame = ".";

          String suffix = accession + "_" + begin + "_" + end + ";";
          String attribute = "";

          if (feature.toLowerCase().contains("gene"))
            attribute = "gene_id gene_" + suffix + " transcript_id transcript_" + suffix;

          gtfWriter.print(accession + "\tGBOL\t" + feature + "\t" + begin + "\t" + end + "\t"
              + score + "\t" + strand + "\t" + frame + "\t" + attribute + "\n");


          // Currently the transcript is identical to the gene positions
          gtfWriter.print(accession + "\tGBOL\t" + "transcript" + "\t" + begin + "\t" + end + "\t"
              + score + "\t" + strand + "\t" + frame + "\n");

        }
        for (ResultLine transcript : RDFResource.runQuery(hdt, "getTranscript.txt",
            feat.getIRI("feature"))) {

          // For each transcript get the exonlist
          for (ResultLine exon : RDFResource.runQuery(hdt, "getExonLocations2.txt",
              transcript.getIRI("transcript"))) {
            int begin = exon.getLitInt("begin");
            int end = exon.getLitInt("end");
            String strand = exon.getIRI("strand");
            if (strand.contains("ForwardStrandPosition"))
              strand = "+";
            if (strand.contains("ReverseStrandPosition"))
              strand = "-";

            String score = ".";
            String frame = ".";
            String suffix = accession + "_" + begin + "_" + end;
            String attribute = "gene_id gene_" + suffix + " transcript_id transcript_" + suffix;

            gtfWriter.print(accession + "\tGBOL\t" + "exon" + "\t" + begin + "\t" + end + "\t"
                + score + "\t" + strand + "\t" + frame + "\t" + attribute + "\n");

          }
        }
      }
    }
    fastaWriter.close();
    gtfWriter.close();
    // seqname - name of the chromosome or scaffold; chromosome names can be given with or without
    // the 'chr' prefix. Important note: the seqname must be one used within Ensembl, i.e. a
    // standard chromosome name or an Ensembl identifier such as a scaffold ID, without any
    // additional content such as species or assembly. See the example GFF output below.

    // source - name of the program that generated this feature, or the data source (database or
    // project name)

    // feature - feature type name, e.g. Gene, Variation, Similarity

    // start - Start position of the feature, with sequence numbering starting at 1.

    // end - End position of the feature, with sequence numbering starting at 1.

    // score - A floating point value.

    // strand - defined as + (forward) or - (reverse).

    // frame - One of '0', '1' or '2'. '0' indicates that the first base of the feature is the
    // first base of a codon, '1' that the second base is the first base of a codon, and so on..

    // attribute - A semicolon-separated list of tag-value pairs, providing additional information
    // about each feature.


  }
}
