package nl.wur.ssb.conversion.rdf2fasta;

import java.io.File;
import java.io.PrintWriter;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.conversion.App;
import nl.wur.ssb.conversion.options.CommandOptionsRDF2FASTA;

import org.rdfhdt.hdt.hdt.HDTManager;

public class RDF2Fasta {

  public static void app(Domain GBOL, CommandOptionsRDF2FASTA arguments) throws Exception {

    if (arguments.gene != null) {
      MakeFasta("getGenes.txt", arguments.gene, arguments.gbol);
    }
    if (arguments.genome != null) {
      MakeFasta("getGenomeFasta.txt", arguments.genome, arguments.gbol);
    }
    if (arguments.protein != null) {
      MakeFasta("getProteins.txt", arguments.protein, arguments.gbol);
    }
    if (arguments.rrna != null) {
      MakeFasta("getrRNAs.txt", arguments.rrna, arguments.gbol);
    }
    if (arguments.trna != null) {
      MakeFasta("gettRNAs.txt", arguments.trna, arguments.gbol);
    }
  }

  private static void MakeFasta(String query, File fastafile, File gbol) throws Exception {
    RDFSimpleCon RDFResource = new RDFSimpleCon("");

    // LOAD HDT file...
    org.rdfhdt.hdt.hdt.HDT hdt = HDTManager.mapIndexedHDT(gbol.getAbsolutePath(), null);
    PrintWriter writer = new PrintWriter(fastafile);
    int count = 0;
    for (ResultLine item : RDFResource.runQuery(hdt, query)) {
      count++;
      String header = item.getLitString("locus");
      String sequence = item.getLitString("sequence");
      writer.write(">" + header + "\n" + sequence + "\n");
    }
    App.logger.info("Created: " + gbol.getName() + " with " + count + " sequences of " + query);
    writer.close();
  }

}
