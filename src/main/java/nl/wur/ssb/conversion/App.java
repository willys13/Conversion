package nl.wur.ssb.conversion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;

import org.apache.commons.io.FileUtils;
import org.apache.jena.ext.com.google.common.io.Files;
import org.apache.log4j.Logger;

import nl.wur.ssb.GFF3.GFF3Mapper;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Save;
import nl.wur.ssb.conversion.fasta2rdf.Fasta;
import nl.wur.ssb.conversion.flatfile.Flatfile;
import nl.wur.ssb.conversion.jsonyaml.JsonYamlConverter;
import nl.wur.ssb.conversion.options.CommandOptionsBase;
import nl.wur.ssb.conversion.options.CommandOptionsFasta2RDF;
import nl.wur.ssb.conversion.options.CommandOptionsHDT2RDF;
import nl.wur.ssb.conversion.options.CommandOptionsJsonYaml;
import nl.wur.ssb.conversion.options.CommandOptionsMerge;
import nl.wur.ssb.conversion.options.CommandOptionsRDF2FASTA;
import nl.wur.ssb.conversion.options.CommandOptionsToEmbl;
import nl.wur.ssb.conversion.rdf2fasta.RDF2Fasta;
import nl.wur.ssb.conversion.toembl.ToEmbl;
import nl.wur.ssb.rdfconversion.HDT2RDF;
import nl.wur.ssb.rdfconversion.Merge;
import nl.wur.ssb.spark.Spark;

/**
 * SAPP conversion
 */

public class App {

  public static Domain domain;
  public static Logger logger = null;

  public static void main(String[] args) throws Exception {
    File diskstore = Files.createTempDir();
    diskstore.deleteOnExit();
    
    // domain = new Domain("file://" + diskstore.getAbsolutePath());
    // Just saving disk space for now as each time a jena dir is created and not deleted due to crashes of code
    domain = new Domain("");
    List<String> argsArray = Arrays.asList(args);
    if (argsArray.contains("-spark")) {
      logger = Generic.Logger(true);
      Spark.main(args);
    } else if (argsArray.contains("-genbank2rdf") || argsArray.contains("-embl2rdf")) {
        logger = Generic.Logger(false);
        new Flatfile(args);
    } else if (argsArray.contains("-fasta2rdf")) {
      CommandOptionsFasta2RDF arguments = new CommandOptionsFasta2RDF(args);
      logger = Generic.Logger(arguments.debug);
      logger.info("Now performing FASTA analysis");
      Fasta.app(domain, arguments);
      Save.save(domain, arguments.gbol,arguments.format);
    } else if (argsArray.contains("-gff2rdf")) {
      logger = Generic.Logger(false);
      new GFF3Mapper(args);
    } else if (argsArray.contains("-merge")) {
      CommandOptionsMerge arguments = new CommandOptionsMerge(args);
      logger = Generic.Logger(arguments.debug);
      logger.info("Now performing RDF merge");
      Merge.app(arguments);
      // save(arguments.output);
    } else if (argsArray.contains("-hdt2rdf")
        || argsArray.contains("-rdf2hdt")) {
      CommandOptionsHDT2RDF arguments = new CommandOptionsHDT2RDF(args);
      logger = Generic.Logger(arguments.debug);
      logger.info("Now performing RDF conversion");
      HDT2RDF.app(arguments);
      // save(arguments.output);
    }
    else if (argsArray.contains("-rdf2fasta")) {
      CommandOptionsRDF2FASTA arguments = new CommandOptionsRDF2FASTA(args);
      logger = Generic.Logger(arguments.debug);
      logger.info("Now performing FASTA analysis");
      RDF2Fasta.app(domain, arguments);
    }  else if (argsArray.contains("-rdf2embl")) {
      CommandOptionsToEmbl arguments = new CommandOptionsToEmbl(args);
      logger = Generic.Logger(arguments.debug);
      logger.info("Now performing ToEMBL analysis");
      ToEmbl.app(arguments);
      // save(arguments.output);
    } else if (argsArray.contains("-rdf2json") 
        || argsArray.contains("-rdf2yaml")
        || argsArray.contains("-yaml2rdf")
        || argsArray.contains("-json2rdf")) {
      CommandOptionsJsonYaml arguments = new CommandOptionsJsonYaml(args);
      Generic.Logger(arguments.debug);
      logger.info("Converting to/from json/yaml");
      JsonYamlConverter.convert(arguments);
    } else {
      String[] help = {"--help"};
      new CommandOptionsBase(help);
    }
    FileUtils.deleteDirectory(diskstore);
  }
  
  public static BufferedReader getStreamFromZip(File file) throws Exception
  {
    BufferedReader reader;
    try
    {
      InputStream fileStream = new FileInputStream(file);
      InputStream gzipStream = new GZIPInputStream(fileStream);
      Reader decoder = new InputStreamReader(gzipStream,"UTF-8");
      reader = new BufferedReader(decoder);
    }
    catch (ZipException e)
    {
      InputStream fileStream = new FileInputStream(file);
      Reader decoder = new InputStreamReader(fileStream,"UTF-8");
      reader = new BufferedReader(decoder);
    }
    return reader;
  }
  
}
