package nl.wur.ssb.conversion.options;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;


@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsFlatfile extends CommandOptionsConversion {
  
  public final String toolVersion = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsFlatfile.class)[0];
  public final String sappcommit = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsFlatfile.class)[1];

    @Parameter(names = { "-embl2rdf" }, description = "EMBL to RDF conversion")
    public boolean embl = false;

    @Parameter(names = { "-genbank2rdf" }, description = "GenBank to RDF conversion")
    public boolean genbank = false;

    @Parameter(names = { "-f", "-format" }, description = "output in ttl or hdt format")
    public String format = "hdt";

    @Parameter(names = { "-u",
            "-uniqueidentifier" }, description = "Unique identifier for your sample (e.g. GCA_000001", required = true)
    public String identifier;

    @Parameter(names = { "-locus" }, description = "Locus counter for genes missing a locus_tag")
    public int locus = 1;

    @Parameter(names = { "-agent" }, description = "IRI of the agent running the tool")
    public String userAgentIri;

    @Parameter(names = { "-if",
            "-importFiles" }, description = "Any ttl file that needs to include, aka a file describing the agent")
    public List<File> importFiles = new ArrayList<File>();
    
    
    @Parameter(names = {"-codon"},
        description = "Codon table to use when transl_table AND protein sequences are not availale")
    public int codon;

    public CommandOptionsFlatfile(String args[]) throws Exception {
        this.commandLine = StringUtils.join(args, " ");
        
        if (this.format != "ttl" && this.format != "hdt")
          throw new Exception("Format is neither hdt or ttl");
        
        try {
            new JCommander(this, args);
            if (this.help == true) {
                new JCommander(this).usage();
                System.exit(0);
            }
        } catch (ParameterException pe) {
            System.err.println(pe.getMessage());
            new JCommander(this).usage();
            System.err.println("  * required parameter");
            System.exit(1);
        }
    }
}
