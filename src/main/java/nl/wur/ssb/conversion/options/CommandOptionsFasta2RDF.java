package nl.wur.ssb.conversion.options;

import java.io.File;
import java.time.LocalDateTime;

import org.apache.commons.lang.StringUtils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsFasta2RDF {

  @Parameter(names = "--help")
  private boolean help;

  /*
   * Fasta2Rdf
   */
  
  @Parameter(names = {"-fasta2rdf"}, description = "Fasta to RDF conversion")
  boolean fasta2rdf = false;

  
  @Parameter(names = {"-i", "-input"}, description = "input FASTA file", required = true)
  public File input;

  
  @Parameter(names = {"-o", "-output"}, description = "output file in HDT GBOL format",
      required = true)
  public File gbol;

  
  @Parameter(names = {"-id", "-identifier"},
      description = "Unique identifier for your sample (e.g. GCA_000001", required = true)
  public String identifier;

  LocalDateTime starttime = LocalDateTime.now();
  String version = "1.0.0";

  @Parameter(names = "-debug", description = "Debug mode")
  public boolean debug = false;

  @Parameter(names = "-format", description = "hdt or ttl output format")
  public String format = "hdt";
  
  @Parameter(names = {"-org", "-organism"}, description = "Organism name", required = true)
  String organism;

  
  @Parameter(names = {"-codon"}, description = "Codon table used for translating the genes")
  public int codon;

  
  @Parameter(names = {"-gene"}, description = "Fasta file consists of genes")
  public boolean gene = false;

  
  @Parameter(names = {"-protein"}, description = "Fasta file consists of proteins")
  public boolean protein = false;

  
  @Parameter(names = {"-genome"}, description = "Fasta file consists of genome sequences")
  public boolean genome = false;

  
  @Parameter(names = {"-translate"}, description = "Translate the input sequence to protein")
  public boolean translate = false;

  
  @Parameter(names = {"-contig"}, description = "Genome fasta file consists of contigs")
  public boolean contig = false;

  
  @Parameter(names = {"-scaffold"}, description = "Genome fasta file consists of scaffolds")
  public boolean scaffold = false;

  
  @Parameter(names = {"-chromosome"},
      description = "Genome fasta file consists of complete chromosomes")
  public boolean chromosome = false;

  @Parameter(names = {"-topology"}, description = "Describing wether sequence is circular or linear (only applicable for genome fasta files)")
  public String topology = "linear";
  
  @Parameter(names = {"-stopcodon"},
      description = "Will not raise an exception when stop codons are detected during translation",
      required = false)
  public boolean stopcodon = false;

  
  public String commandline;


  public CommandOptionsFasta2RDF(String args[]) {
    try {
      JCommander jc = new JCommander(this, args);
       
      if (this.help == true) {
        new JCommander(this).usage();
        System.exit(0);
      }
    } catch (ParameterException pe) {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(1);
    }
    this.commandline = StringUtils.join(args);
  }
}
