package nl.wur.ssb.conversion.options;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsBase {

  @Parameter(names = "--help")
  private boolean help;

  
  @Parameter(names = {"-embl2rdf"}, description = "input format is embl", required = false)
  boolean embl2rdf = false;

  
  @Parameter(names = {"-genbank2rdf"}, description = "input format is genbank", required = false)
  boolean genbank2rdf = false;

  
  @Parameter(names = {"-fasta2rdf"}, description = "input format is fasta", required = false)
  boolean fasta2rdf = false;

  
  @Parameter(names = {"-rdf2fasta"}, description = "input format is fasta", required = false)
  boolean rdf2fasta = false;

  
  @Parameter(names = {"-rdf2embl"}, description = "input format is RDF to create EMBL files",
      required = false)
  boolean rdf2embl = false;

  
  @Parameter(names = {"-rdf2gtf"}, description = "input format is RDF to create GTF files",
      required = false)
  boolean rdf2gtf = false;

  
  @Parameter(names = {"-gtf2rdf"}, description = "input format is GTF to create GBOL files",
      required = false)
  boolean gtf2rdf = false;

  
  @Parameter(names = {"-gff2rdf"}, description = "input format is GFF3 to create GBOL files",
      required = false)
  boolean gff2rdf = false;

  
  @Parameter(names = {"-hdt2rdf", "-rdf2hdt"},
      description = "Conversion of RDF to HDT or HDT to RDF", required = false)
  boolean hdt2rdf = false;

  @Parameter(names = {"-rdf2json"}, description = "convert from RDF to framed json",
      required = false)
  boolean rdf2json = false;
  
  @Parameter(names = {"-json2rdf"}, description = "convert framed json to RDF",
      required = false)
  boolean json2rdf = false;
  
  @Parameter(names = {"-rdf2yaml"}, description = "convert from RDF to framed yaml (mimicks genbank format)",
      required = false)
  boolean rdf2yaml = false;
  
  @Parameter(names = {"-yaml2rdf"}, description = "convert from framed yaml (mimicks genbank format) to RDF",
      required = false)
  boolean yaml2rdf = false;
  
  
  @Parameter(names = {"-merge"}, description = "Merging of HDT files", required = false)
  boolean merge = false;

  public CommandOptionsBase(String args[]) {
    try {
      JCommander jc = new JCommander(this, args);

      if (this.help == true) {
        new JCommander(this).usage();
      }
    } catch (ParameterException pe) {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(0);
    }
  }
}
