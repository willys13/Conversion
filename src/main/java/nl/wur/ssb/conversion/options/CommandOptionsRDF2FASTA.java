package nl.wur.ssb.conversion.options;

import java.io.File;
import java.time.LocalDateTime;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsRDF2FASTA {
  @Parameter(names = "--help")
  private boolean help;


  /*
   * Fasta2Rdf
   */

  
  @Parameter(names = {"-rdf2fasta"}, description = "GBOL to FASTA conversion", required = false)
  boolean rdf2fasta = false;

  
  @Parameter(names = {"-i", "-input"}, description = "input GBOL file", required = true)
  public File gbol;

  
  @Parameter(names = "-starttime", description = "Start time of code", hidden = true)
  public LocalDateTime starttime = LocalDateTime.now();

  
  @Parameter(names = "-version", description = "SAPP version", hidden = true)
  public String version = "1.0.0";

  @Parameter(names = "-debug", description = "Debug mode", hidden = true)
  public boolean debug = false;

  
  @Parameter(names = {"-gene"}, description = "Gene file location", required = false)
  public File gene;

  
  @Parameter(names = {"-trna"}, description = "tRNA file location", required = false)
  public File trna;

  
  @Parameter(names = {"-rrna"}, description = "rRNA file location", required = false)
  public File rrna;

  
  @Parameter(names = {"-protein"}, description = "Protein file location", required = false)
  public File protein;

  
  @Parameter(names = {"-genome"}, description = "Genome file location", required = false)
  public File genome;

  public CommandOptionsRDF2FASTA(String args[]) {
    try {
      JCommander jc = new JCommander(this, args);

      if (this.help == true) {
        new JCommander(this).usage();
        System.exit(0);
      }
    } catch (ParameterException pe) {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(1);
    }
  }
}
