package nl.wur.ssb.conversion.options;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsGFF3 extends CommandOptionsConversion {

  @Parameter(names = {"-gff2rdf"}, description = "Input is GFF3")
  public boolean gff2rdf;

  @Parameter(names = {"-codon"}, description = "Codon table used (for protein translation)", required = true)
  public int codon;

  @Parameter(names = {"-f", "-fasta"}, description = "input corresponding fasta file", required = true)
  public File fasta;

  @Parameter(names = {"-format"}, description = "Output format (ttl or hdt)")
  public String format = "hdt";

  @Parameter(names = {"-id", "identifier"}, description = "Sample identifier", required = true)
  public String identifier;

  @Parameter(names = {"-agent"}, description = "IRI of the agent running the tool")
  public String userAgentIri;

  public final String toolVersion = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsGFF3.class)[0];
  public final String toolCommit = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsGFF3.class)[1];

  @Parameter(names = "-seqtype", description = "Sequence type (contig, scaffold, chromosome, plasmid, read)")
  public String seqtype = "contig";

  @Parameter(names = {"-topology"},
      description = "Describing wether sequence is circular or linear (only applicable for genome fasta files)")
  public String topology;

  public int locus = 1;

  public CommandOptionsGFF3(String args[]) throws IOException {
    try {
      JCommander jc = new JCommander(this, args);
      this.commandLine = StringUtils.join(args," ");
      if (this.help == true) {
        new JCommander(this).usage();
        System.exit(0);
      }
    } catch (ParameterException pe) {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(1);
    }
  }
}
