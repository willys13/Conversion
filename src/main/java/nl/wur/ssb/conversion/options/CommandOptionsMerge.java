package nl.wur.ssb.conversion.options;

import java.io.File;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsMerge {

  @Parameter(names = "--help")
  private boolean help;

  
  @Parameter(names = {"-m", "-merge"}, description = "Merge RDF files", required = true)
  public boolean merge;

  
  @Parameter(names = {"-i", "-input"}, description = "input files (comma separated)",
      required = true)
  public String input;

  
  @Parameter(names = {"-o", "-output"}, description = "output file", required = true)
  public File output;

  @Parameter(names = "-debug", description = "Debug mode")
  public boolean debug = false;

  public CommandOptionsMerge(String args[]) {
    try {
      new JCommander(this, args);
      if (this.help == true) {
        new JCommander(this).usage();
        System.exit(0);
      }

    } catch (ParameterException pe) {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(1);
    }
  }
}
