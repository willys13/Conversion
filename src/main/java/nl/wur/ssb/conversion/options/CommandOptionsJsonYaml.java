package nl.wur.ssb.conversion.options;

import java.io.File;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsJsonYaml extends CommandOptionsConversion
{   
  @Parameter(names = {"-rdf2json"}, description = "convert from RDF to framed json", required = false)
  public boolean rdf2json = false;
  
  @Parameter(names = {"-json2rdf"}, description = "convert framed json to RDF", required = false)
  public boolean json2rdf = false;
  
  @Parameter(names = {"-rdf2yaml"}, description = "convert from RDF to framed yaml (mimicks genbank format)", required = false)
  public boolean rdf2yaml = false;
  
  @Parameter(names = {"-yaml2rdf"}, description = "convert from framed yaml (mimicks genbank format) to RDF", required = false)
  public boolean yaml2rdf = false;
  
  public CommandOptionsJsonYaml(String args[])
  {
    try
    {
      JCommander jc = new JCommander(this,args);
      
      if (this.help == true)
      {
        new JCommander(this).usage();
      }
    }
    catch (ParameterException pe)
    {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(0);
    }
  }
}
