package nl.wur.ssb.conversion.options;

import java.io.File;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsToEmbl {
  @Parameter(names = "--help")
  private boolean help;

  
  @Parameter(names = {"-i", "-input"}, description = "input file (HDT - annotated by SAPP / GBOL).",
      required = true)
  public File gbol;

  
  @Parameter(names = {"-o", "-output"}, description = "output embl file", required = true)
  public File output;

  
  @Parameter(names = {"-rdf2embl"}, description = "Converting HDT/RDF to EMBL", required = true)
  public boolean rdf2embl;

  @Parameter(names = "-debug", description = "Debug mode", hidden = true)
  public boolean debug = false;

  public CommandOptionsToEmbl(String args[]) {
    try {
      JCommander jc = new JCommander(this, args);

      if (this.help == true) {
        new JCommander(this).usage();
        System.exit(0);
      }
    } catch (ParameterException pe) {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(1);
    }
  }
}
