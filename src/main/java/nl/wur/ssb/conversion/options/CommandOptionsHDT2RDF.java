package nl.wur.ssb.conversion.options;

import java.io.File;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")

public class CommandOptionsHDT2RDF {
  @Parameter(names = "--help")
  private boolean help;

  
  @Parameter(names = {"-i", "-input"}, description = "input file (HDT/RDF)", required = true)
  public File input;

  
  @Parameter(names = {"-o", "-output"}, description = "Output file (HDT/RDF)", required = true)
  public File output;

  
  @Parameter(names = {"-hdt2rdf"}, description = "Converting HDT to RDF", required = false)
  public static boolean hdt2rdf;

  
  @Parameter(names = {"-format"}, description = "RDF format (TURTLE/NT/...)", required = false)
  public static String format = "TURTLE";

  
  @Parameter(names = {"-rdf2hdt"}, description = "Converting RDF to HDT", required = false)
  public static boolean rdf2hdt;

  
  @Parameter(names = {"--prefix", "-p"}, description = "Prefix reducer: 'prefix@url prefi2x@url2'")
  public static String prefix =
      "gbol@http://gbol.life/0.1/ biopax@http://www.biopax.org/release/bp-level3.owl#";

  @Parameter(names = "-debug", description = "Debug mode", hidden = true)
  public boolean debug = false;

  public CommandOptionsHDT2RDF(String args[]) {
    try {
      JCommander jc = new JCommander(this, args);

      if (this.help == true) {
        new JCommander(this).usage();
        System.exit(0);
      }
    } catch (ParameterException pe) {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(1);
    }
  }
}
