package nl.wur.ssb.conversion.options;

import java.io.File;
import java.time.LocalDateTime;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsConversion {

  @Parameter(names = "--help")
  public boolean help;

  @Parameter(names = {"-i", "-input"}, description = "Input file", required = true)
  public File input;

  @Parameter(names = {"-o", "-output"}, description = "Output file", required = true)
  public File output;

  public LocalDateTime starttime = LocalDateTime.now();
  public String commandLine;
  public final String toolName = "SAPP - File Conversion";
  public final String repository = "http://gitlab.com/sapp/conversion/conversion";

  @Parameter(names = "-debug", description = "Debug mode", hidden = true)
  public boolean debug = false;
}
