package nl.wur.ssb.conversion.toembl;

import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import life.gbol.domain.AfterPosition;
import life.gbol.domain.AnnotationSoftware;
import life.gbol.domain.BeforePosition;
import life.gbol.domain.CDS;
import life.gbol.domain.Database;
import life.gbol.domain.ExactPosition;
import life.gbol.domain.Exon;
import life.gbol.domain.FeatureProvenance;
import life.gbol.domain.Gene;
import life.gbol.domain.Location;
import life.gbol.domain.NAFeature;
import life.gbol.domain.NASequence;
import life.gbol.domain.Position;
import life.gbol.domain.Protein;
import life.gbol.domain.ProteinFeature;
import life.gbol.domain.Region;
import life.gbol.domain.Sample;
import life.gbol.domain.Transcript;
import life.gbol.domain.TranscriptFeature;
import life.gbol.domain.mRNA;
import life.gbol.domain.rRNA;
import life.gbol.domain.tRNA;
import nl.systemsbiology.semantics.sapp.domain.Aragorn;
import nl.systemsbiology.semantics.sapp.domain.InterProScan;
import nl.systemsbiology.semantics.sapp.domain.Prodigal;
import nl.systemsbiology.semantics.sapp.domain.RNAmmer;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Store;
import nl.wur.ssb.conversion.App;
import nl.wur.ssb.conversion.options.CommandOptionsToEmbl;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.entry.EntryFactory;
import uk.ac.ebi.embl.api.entry.XRef;
import uk.ac.ebi.embl.api.entry.feature.CdsFeature;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.feature.FeatureFactory;
import uk.ac.ebi.embl.api.entry.feature.SourceFeature;
import uk.ac.ebi.embl.api.entry.location.LocationFactory;
import uk.ac.ebi.embl.api.entry.qualifier.QualifierFactory;
import uk.ac.ebi.embl.api.entry.sequence.SequenceFactory;
import uk.ac.ebi.embl.api.taxonomy.Taxon;
import uk.ac.ebi.embl.api.taxonomy.TaxonFactory;
import uk.ac.ebi.embl.flatfile.writer.embl.EmblEntryWriter;

public class ToEmbl {
  public static CommandOptionsToEmbl arguments;
  private static RDFSimpleCon RDFResource;
  // EMBL things
  private static FeatureFactory featureFactory = new FeatureFactory();
  private static QualifierFactory qualifierFactory = new QualifierFactory();
  private static LocationFactory locationFactory = new LocationFactory();
  private static SequenceFactory sequenceFactory = new SequenceFactory();

  public static void app(CommandOptionsToEmbl args) throws Exception {
    arguments = args;

    Domain domain = Store.createDiskStore();

    // LOAD HDT file...
    org.rdfhdt.hdt.hdt.HDT hdt = HDTManager.mapIndexedHDT(arguments.gbol.getAbsolutePath(), null);

    HDTGraph graph = new HDTGraph(hdt);
    Model model = ModelFactory.createModelForGraph(graph);

    // Putting the HDT into jena?
    domain.getRDFSimpleCon().getModel().add(model);

    // Create for each sample a EMBL file
    for (ResultLine item : domain.getRDFSimpleCon().runQuery(hdt, "getContigs.txt")) {
      Sample sample = domain.make(life.gbol.domain.Sample.class, item.getIRI("sample"));
      NASequence naSequence = domain.make(life.gbol.domain.NASequence.class, item.getIRI("naobject"));

      String sequence = naSequence.getSequence();

      // Creating the EMBL entry
      Entry entry = (new EntryFactory()).createEntry();

      // Setting the sequence
      entry.setSequence(sequenceFactory
          .createSequenceByte(naSequence.getSequence().toLowerCase().getBytes()));

      /*
      TODO Setting other stuff...
       */

      // Creating the features

      Iterator<? extends NAFeature> naSeqIter = naSequence.getAllFeature()
          .iterator();

      while (naSeqIter.hasNext()) {
        NAFeature naFeature = naSeqIter.next();
        String locType = null;

        if (naFeature.getClassTypeIri().endsWith("/Gene")) {
          // Gene part
          Gene gene = (Gene) naFeature;
          App.logger.info(gene.getResource().getURI());
          GeneCreator(gene, entry);

          // Transcript part
          List<? extends Transcript> transcripts = gene.getAllTranscript();
          Iterator<? extends Transcript> transIter = transcripts.iterator();
          while (transIter.hasNext())
            TranscriptCreator(gene, transIter.next(), entry);

        } else {
          throw new Exception("NAFeature unknown: " + naFeature.getClassTypeIri());
        }
      }

      StringWriter writer = new StringWriter();
      new EmblEntryWriter(entry).write(writer);

      /*
       * Writing to file
       */
      if (new File(arguments.output.getAbsolutePath()).exists())
        Files.write(Paths.get(arguments.output.getAbsolutePath()), writer.toString().getBytes(), StandardOpenOption.APPEND);
      else
        Files.write(Paths.get(arguments.output.getAbsolutePath()), writer.toString().getBytes());
    }


//
//
//    if (1==1) {
//      return;
//    }
//
//
//    for (ResultLine item : RDFResource.runQuery(hdt, "getContigs.txt")) {
//      Entry entry = (new EntryFactory()).createEntry();
//      // TODO no error, but also not adding to id
//      // TODO this is NOT the ID. (for id line)
//      if (item.getLitString("accession") != null)
//        entry.setId(item.getLitString("accession"));
//
//      // ADDING DIVISION NAME TO ID (for id line)
//      if (item.getLitString("division") != null)
//        entry.setDivision(item.getLitString("division"));
//
//      // ADDING DATACLASS NAME TO ID (for id line)
//      if (item.getLitString("dataclass") != null)
//        entry.setDataClass(item.getLitString("dataclass"));
//
//      // ADDING ACCESSION
//      if (item.getLitString("accession") != null)
//        entry.setPrimaryAccession(item.getLitString("accession"));
//
//      // ADDING DATES
//      SimpleDateFormat fromTtl = new SimpleDateFormat("yyyy-MM-dd");
//      SimpleDateFormat toEmbl = new SimpleDateFormat("dd-MMM-yyyy");
//
//      if (item.getLitString("fp") != null) {
//        String date = toEmbl.format(fromTtl.parse(item.getLitString("fp")));
//        entry.setFirstPublic(FlatFileUtils.getDay(date));
//        entry.setFirstPublicRelease(item.getLitInt("fpr"));
//      }
//
//      if (item.getLitString("lu") != null) {
//        String date = toEmbl.format(fromTtl.parse(item.getLitString("lu")));
//        entry.setLastUpdated(FlatFileUtils.getDay(date));
//        entry.setLastUpdatedRelease(item.getLitInt("lur"));
//      }
//      if (item.getLitString("version") != null)
//        entry.setVersion(item.getLitInt("version"));
//
//      // ADDING DESCRIPTION
//      if (item.getLitString("description") != null) {
//        Text description = new Text();
//        description.setText(item.getLitString("description"));
//        entry.setDescription(description);
//      } else {
//        Text description = new Text();
//        description.setText("No description available");
//        entry.setDescription(description);
//      }
//
//      // ADDING KEYWORDS
//      // TODO main collections is outside loop, will give errors??
//      Collection<Text> keywords = new ArrayList<Text>();
//      for (ResultLine key : RDFResource.runQuery(hdt, "getKeywords.txt", item.getIRI("naobject"))) {
//        Text keyword = new Text();
//        keyword.setText(key.getLitString("keyword"));
//        keywords.add(keyword);
//      }
//      entry.addKeywords(keywords);
//
//
//      // TODO ADDING CITATIONS (ARE PRESENT, NOT PARSABLE)
//      // only has authors and title (year is wrong)
//
//      // ADDING FEATURES
//      entry = Features.add(RDFResource, hdt, entry, item.getIRI("naobject"), featureFactory);
//
//      // ADDING SEQUENCE
//      entry.setSequence(sequenceFactory
//          .createSequenceByte(item.getLitString("sequence").toLowerCase().getBytes()));
//      // entry.setSequence(sequenceFactory.createSequenceByte("aaaccggtt".toLowerCase().getBytes()));
//
//      // Setting sequence version (for id line)
//      if (item.getLitString("version") != null)
//        entry.getSequence().setVersion(item.getLitInt("version"));
//
//      // Setting molecule type (for id line)
//      if (item.getLitString("moleculetype") != null)
//        entry.getSequence().setMoleculeType(item.getLitString("moleculetype"));
//
//      // Setting topology (for id line)
//      if (item.getIRI("topology") != null) {
//        if (item.getIRI("topology").contains("Linear")) {
//          entry.getSequence().setTopology(Topology.LINEAR);
//        } else if (item.getIRI("topology").contains("Circular")) {
//          entry.getSequence().setTopology(Topology.CIRCULAR);
//        } else {
//          throw new Exception("Topology is of..." + item.getIRI("topology"));
//        }
//      }
//      // CREATING THE SOURCE ENTRY...
//      entry = Source(RDFResource, hdt, item.getIRI("naobject"), entry, featureFactory,
//          locationFactory, qualifierFactory);
//
//      // TODO adding xrefs to gene?
//      entry = getGeneXrefs(entry, hdt, item.getIRI("naobject"));
//
//      StringWriter writer = new StringWriter();
//      new EmblEntryWriter(entry).write(writer);
//
//      /*
//       * Writing to file
//       */
//      if (new File(arguments.output.getAbsolutePath()).exists())
//        Files.write(Paths.get(arguments.output.getAbsolutePath()), writer.toString().getBytes(),
//            StandardOpenOption.APPEND);
//      else
//        Files.write(Paths.get(arguments.output.getAbsolutePath()), writer.toString().getBytes());
//    }
//
//    File filename = new File(arguments.output.getAbsolutePath());
//    String encoding = "UTF-8";
//    InputStream fileStream = new FileInputStream(filename);
//    Reader decoder = new InputStreamReader(fileStream, encoding);
//    BufferedReader reader = new BufferedReader(decoder);
//
//    App.logger.info("Validating embl file..");
//    EmblEntryReader emblReader = new EmblEntryReader(reader);
//    ValidationResult validations = emblReader.read();
//    if (!validations.getMessages().isEmpty()) {
//      Iterator<ValidationMessage<Origin>> valiter = validations.getMessages().iterator();
//      while (valiter.hasNext()) {
//        ValidationMessage<Origin> validation = valiter.next();
//        // TODO enable in the end...
//        App.logger.warn(validation.getMessage());
//      }
//    } else {
//      App.logger.warn("EMBL files seems to be good....");
//    }

  }

  private static void TranscriptCreator(Gene gene, Transcript transcript, Entry entry) throws Exception {
    Feature feature = null;
    CdsFeature cdsFeature = null;
    
    if (transcript.getClassTypeIri().endsWith("mRNA")) {
      mRNA mrna = (mRNA) transcript;
      feature = featureFactory.createFeature("mRNA", false);
      if (mrna.getFunction() != null)
        feature.addQualifier("function",mrna.getFunction());
      if (gene.getLocusTag() != null)
        feature.addQualifier("locus_tag",gene.getLocusTag());

      Iterator<? extends TranscriptFeature> featIter = mrna.getAllFeature().iterator();
      while (featIter.hasNext()) {
        TranscriptFeature transcriptFeature = featIter.next();
        if(transcriptFeature.getClassTypeIri().endsWith("/CDS")) {
          CDS cds = (CDS) transcriptFeature;
          cdsFeature = featureFactory.createCdsFeature();
          cdsFeature.setTranslation(cds.getProtein().getSequence());
          ProteinAnnotation(cds.getProtein(), cdsFeature);
        }
      }
    }
    else if (transcript.getClassTypeIri().endsWith("tRNA")) {
      tRNA trna = (tRNA) transcript;
      feature = featureFactory.createFeature("tRNA", false);
      if (trna.getFunction() != null)
        feature.addQualifier("function", trna.getFunction());
      if (gene.getLocusTag() != null)
        feature.addQualifier("locus_tag", gene.getLocusTag());
    } else if (transcript.getClassTypeIri().endsWith("rRNA")) {
      rRNA rrna = (rRNA) transcript;
      feature = featureFactory.createFeature("rRNA", false);
      if (rrna.getFunction() != null)
        feature.addQualifier("function",rrna.getFunction());
      if (gene.getLocusTag() != null)
        feature.addQualifier("locus_tag",gene.getLocusTag());
    } else {
      throw new Exception("Transcript type unknown: "+transcript.getClassTypeIri());
    }

    if (feature != null) {
      Iterator<? extends Exon> exonIter = transcript.getExonList().getAllExon().iterator();

      while (exonIter.hasNext()) {
        Exon exon = exonIter.next();
        SetPositions(feature, exon.getLocation());
        if (cdsFeature != null) {
          SetPositions(cdsFeature, exon.getLocation());
          entry.addFeature(cdsFeature);
        }
      }
      entry.addFeature(feature);
    }
//    String locType = transcript.getLocation().getClassTypeIri();
    
//    SetPositionsGene(feature, gene.getLocation());

//    entry.setPrimaryAccession("PRIMACC");
//    Provenance(feature, gene.getAllProvenance());

  }

  private static void ProteinAnnotation(Protein protein, CdsFeature cdsFeature) throws Exception {
    Iterator<? extends ProteinFeature> featIter = protein.getAllFeature().iterator();

    // Annotate the protein...

    //nl.wur.ssb.proteinAnnotation.App.Annotation(protein);

    // Setting the provenance

    HashSet<String> xrefs = new HashSet<String>();
    while (featIter.hasNext()) {
      ProteinFeature proteinFeature = featIter.next();
      Iterator<? extends FeatureProvenance> allProvIter = proteinFeature.getAllProvenance()
          .iterator();
      while (allProvIter.hasNext()) {
        FeatureProvenance featureProvenance = allProvIter.next();
        AnnotationSoftware annotationSoftware = (AnnotationSoftware) featureProvenance.getOrigin()
            .getWasAttributedTo();
        String name = annotationSoftware.getName();
        String version = annotationSoftware.getVersion();
        String annotType = featureProvenance.getAnnotation().getClassTypeIri();
        if (annotType.endsWith("InterProScan")) {
          InterProScan interProScan = (InterProScan) featureProvenance.getAnnotation();
//          String note = "Evalue:"+ interProScan.getEvalue();
//          cdsFeature.addQualifier("note","inference:"+name+":"+version + " " + note);
//          cdsFeature.addQualifier("inference", "protein motif:" + name + ":" + version);
        } else {
          throw new Exception("FeatureProvenance of protein not known: " + annotType);
        }
      }

      // Setting the DB / Accessions
      Iterator<? extends life.gbol.domain.XRef> xrefIter = proteinFeature.getAllXref().iterator();
      while (xrefIter.hasNext()) {
        life.gbol.domain.XRef xref = xrefIter.next();
        Database db = xref.getDb();
        String acc = xref.getAccession();
        String dbid = db.getId();
        if (dbid.endsWith("pfam"))
          cdsFeature.addQualifier("inference", "protein motif:pfam:"+acc);
        if (dbid.matches("go"))
          acc = acc.replaceAll("GO:","");

        if (!xrefs.contains(dbid + ":" + acc)) {
          XRef xRef = new XRef();
          xRef.setDatabase(dbid);
          xRef.setPrimaryAccession(acc);
          cdsFeature.addXRef(xRef);
        }
        xrefs.add(dbid + ":" + acc);
      }
    }
  }

  private static void GeneCreator(Gene gene, Entry entry) throws Exception {

    String locType = gene.getLocation().getClassTypeIri();
    Feature feature = featureFactory.createFeature("gene", false);
    feature.addQualifier("locus_tag", gene.getLocusTag());
    SetPositions(feature, gene.getLocation());
    entry.addFeature(feature);
    Provenance(feature, gene.getAllProvenance());
  }

  private static void Provenance(Feature feature, List<? extends FeatureProvenance> allProvenance)
      throws Exception {
    Iterator<? extends FeatureProvenance> allProvIter = allProvenance.iterator();
    while (allProvIter.hasNext()) {
      FeatureProvenance featureProvenance = allProvIter.next();
      AnnotationSoftware annotationSoftware = (AnnotationSoftware) featureProvenance.getOrigin().getWasAttributedTo();
      String name = annotationSoftware.getName();
      String version = annotationSoftware.getVersion();
      String annotType = featureProvenance.getAnnotation().getClassTypeIri();
      if (annotType.endsWith("Prodigal")) {
        Prodigal prodigal = (Prodigal) featureProvenance.getAnnotation();
        String note = "Conf:"+ prodigal.getConf();
        feature.addQualifier("note","ab initio prediction:"+name+":"+version + " " + note);
        feature.addQualifier("inference","ab initio prediction:"+name+":"+version);
      } else if (annotType.endsWith("Aragorn")) {
        Aragorn aragorn = (Aragorn) featureProvenance.getAnnotation();
        feature.addQualifier("inference", "ab initio prediction:" + name + ":" + version);
      } else if (annotType.endsWith("RNAmmer")) {
        RNAmmer rnammer = (RNAmmer) featureProvenance.getAnnotation();
        feature.addQualifier("inference", "ab initio prediction:" + name + ":" + version);
        String note = "Score:"+ rnammer.getScore();
        feature.addQualifier("note","ab initio prediction:"+name+":"+version + " " + note);
      } else {
        throw new Exception("Annotation type unknown: "+ annotType);
      }
    }
  }

  private static void SetPositions(Feature feature, Location location) throws Exception {

    // Setting the region of an object
    Position begin = null;
    Position end = null;
    long beginpos;
    long endpos;
    boolean complement = Boolean.parseBoolean(null);
    if (location.getClassTypeIri().endsWith("Region")) {
      Region region = (Region) location;
      begin = region.getBegin();
      end = region.getEnd();
      complement = true;
      if (region.getStrand().getIRI().endsWith("ForwardStrandPosition"))
        complement = false;
    } else {
      throw new Exception("Location type not known: " + location.getClassTypeIri());
    }

    if (begin.getClassTypeIri().endsWith("ExactPosition")) {
      ExactPosition exactpos = (ExactPosition) begin;
      beginpos = exactpos.getPosition();
    } else if (begin.getClassTypeIri().endsWith("BeforePosition")) {
      // TODO create <...
      BeforePosition beforepos = (BeforePosition) begin;
      beginpos = beforepos.getPosition();
    } else {
      throw new Exception("Different type detected: " + begin.getClassTypeIri());
    }

    if (end.getClassTypeIri().endsWith("ExactPosition")) {
      ExactPosition exactpos = (ExactPosition) end;
      endpos = exactpos.getPosition();
    } else if (end.getClassTypeIri().endsWith("AfterPosition")) {
      // TODO create >...
      AfterPosition afterpos = (AfterPosition) end;
      endpos = afterpos.getPosition();
    } else {
      throw new Exception("Different type detected: " + end.getClassTypeIri());
    }

    feature.getLocations().addLocation(
        locationFactory.createLocalRange(beginpos, endpos, complement));

  }

  private static Entry Source(RDFSimpleCon RDFResource, HDT hdt, String featureIRI, Entry entry,
      FeatureFactory featureFactory, LocationFactory locationFactory,
      QualifierFactory qualifierFactory) throws Exception {
    // Creating the source feature
    SourceFeature sourceFeature = featureFactory.createSourceFeature();
    // TODO ADDING TAXONOMY
    TaxonFactory taxonFactory = new TaxonFactory();
    Taxon taxon = taxonFactory.createTaxon();

    for (ResultLine item : RDFResource.runQuery(hdt, "getSource.txt", featureIRI)) {
      // Setting source range

      try {
        sourceFeature.getLocations().addLocation(locationFactory.createLocalRange(Long.valueOf(1l),
            Long.valueOf(item.getLitInt("length")), false));
      } catch (Exception e) {
        App.logger.info("Length not available, genome converted with older version");
      }

      // Setting scientific name
      String scientificName = item.getLitString("scientificname");
      sourceFeature.setScientificName(scientificName);
      // taxon.setScientificName(scientificName);

      // Setting lineage
      // primarySourceFeature.getTaxon().getLineage());
      String lineage = item.getLitString("lineage");
      sourceFeature.setTaxon(taxon);
      taxon.setLineage(lineage);

      // Setting host
      if (item.getLitString("host") != null)
        sourceFeature.setSingleQualifierValue("host", item.getLitString("host"));

      // Setting organism
      if (item.getLitString("organism") != null)
        sourceFeature.setSingleQualifierValue("organism", item.getLitString("organism"));

      // Setting country
      if (item.getLitString("country") != null) {
        sourceFeature.setSingleQualifierValue("country", item.getLitString("country"));
      }

      // Setting strain
      if (item.getLitString("strain") != null)
        sourceFeature.setSingleQualifierValue("strain", item.getLitString("strain"));

      // Setting segment
      if (item.getLitString("segment") != null)
        sourceFeature.setSingleQualifierValue("segment", item.getLitString("segment"));

      // Setting isolate
      if (item.getLitString("isolate") != null)
        sourceFeature.setSingleQualifierValue("isolate", item.getLitString("isolate"));

      // Setting tax id
      // sourceFeature.setSingleQualifierValue("db_xref","taxon:" + );
      if (item.getLitString("taxid") != null) {
        XRef xRef = new XRef();
        xRef.setDatabase("taxon");
        xRef.setPrimaryAccession(item.getLitString("taxid"));
        sourceFeature.addXRef(xRef);
      }
    }
    entry.addFeature(sourceFeature);

    return entry;
  }

  private static Entry getGeneXrefs(Entry entry, HDT hdt, String featureIRI) throws Exception {
    for (ResultLine xrefs : RDFResource.runQuery(hdt, "getGeneXrefs.txt", featureIRI)) {
      String xrefIRI = xrefs.getIRI("xrefs");
      for (ResultLine xref : RDFResource.runQuery(hdt, "getGeneXref.txt", xrefIRI)) {
        XRef xRef = new XRef();
        xRef.setDatabase(xref.getLitString("db"));
        xRef.setPrimaryAccession(xref.getLitString("pa"));
        if (xref.getLitString("sa") != null)
          xRef.setSecondaryAccession(xref.getLitString("sa"));
        entry.addXRef(xRef);
      }
    }
    return entry;
  }
}
