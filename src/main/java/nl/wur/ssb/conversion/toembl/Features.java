package nl.wur.ssb.conversion.toembl;

import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.conversion.App;
import org.rdfhdt.hdt.hdt.HDT;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.entry.XRef;
import uk.ac.ebi.embl.api.entry.feature.CdsFeature;
import uk.ac.ebi.embl.api.entry.feature.Feature;
import uk.ac.ebi.embl.api.entry.feature.FeatureFactory;
import uk.ac.ebi.embl.api.entry.location.LocationFactory;
import uk.ac.ebi.embl.api.entry.qualifier.QualifierFactory;

public class Features {

  public static Entry add(RDFSimpleCon RDFResource, HDT hdt, Entry entry, String iri,
      FeatureFactory featureFactory) throws Exception {
    int geneCounter = 0;
    int otherCounter = 0;
    for (ResultLine item : RDFResource.runQuery(hdt, "getFeatures.txt", iri)) {
      // TODO How do we hadd parameters to the query file...
      String featureIRI = item.getIRI("feature");
      String type = item.getIRI("ftype");
      App.logger.debug("TYPE: " + item.getIRI("ftype"));
      if (type.matches("^http://gbol.life/0.1/Gene$")) {
        entry = Gene(RDFResource, hdt, featureIRI, entry);
        geneCounter++;
        if ((geneCounter % 100) == 0)
          App.logger.info("Gene Feature: " + geneCounter);
      } else {
        entry = GenericFeature(RDFResource, hdt, featureIRI, entry);
        otherCounter++;
        if ((otherCounter % 100) == 0)
          App.logger.info("Other Feature: " + otherCounter);
      }
      App.logger.debug("FEATURE:" + featureIRI);
    }
    App.logger.info("Final gene counter: " + geneCounter);
    App.logger.info("Final other counter: " + otherCounter);
    return entry;
  }

  private static Entry GenericFeature(RDFSimpleCon RDFResource, HDT hdt, String featureIRI,
      Entry entry) throws Exception {
    FeatureFactory featureFactory = new FeatureFactory();
    LocationFactory locationFactory = new LocationFactory();
    Feature feature = null;

    for (ResultLine item : RDFResource.runQuery(hdt, "getGenePositions.txt", featureIRI)) {

      // Check what the type of feature it is
      String type = item.getIRI("type");

      if (type.matches("^http://gbol.life/0.1/TATASignal$")) {
        feature = featureFactory.createFeature("TATA_signal", false);
      } else if (type.matches("^http://gbol.life/0.1/PolyASignal$")) {
        feature = featureFactory.createFeature("polyA_signal", false);
      } else if (type.matches("^http://gbol.life/0.1/MiscFeature$")) {
        feature = featureFactory.createFeature("misc_feature", false);
      } else if (type.matches("^http://gbol.life/0.1/RepeatRegion$")) {
        feature = featureFactory.createFeature("repeat_region", false);
      } else if (type.matches("^http://gbol.life/0.1/SigPeptide$")) {
        feature = featureFactory.createFeature("sig_peptide", false);
      } else if (type.matches("^http://gbol.life/0.1/MobileElement$")) {
        feature = featureFactory.createFeature("mobile_element", false);
      } else if (type.matches("^http://gbol.life/0.1/ReplicationOrigin$")) {
        feature = featureFactory.createFeature("rep_origin", false);
      } else if (type.matches("^http://gbol.life/0.1/MiscDifference$")) {
        feature = featureFactory.createFeature("misc_difference", false);
      } else if (type.matches("^http://gbol.life/0.1/Gap$")) {
        feature = featureFactory.createFeature("gap", false);
      } else if (type.matches("^http://gbol.life/0.1/Regulatory$")) {
        feature = featureFactory.createFeature("regulatory", false);
      } else if (type.matches("^http://gbol.life/0.1/MiscBinding$")) {
        feature = featureFactory.createFeature("misc_binding", false);
      } else if (type.matches("^http://gbol.life/0.1/ProteinBind$")) {
        feature = featureFactory.createFeature("protein_bind", false);
      } else if (type.matches("^http://gbol.life/0.1/SequenceTaggedSite$")) {
        feature = featureFactory.createFeature("STS", false);
      } else if (type.matches("^http://gbol.life/0.1/AssemblyGap$")) {
        feature = featureFactory.createFeature("assembly_gap", false);
      } else if (type.matches("^http://gbol.life/0.1/MatPeptide$")) {
        feature = featureFactory.createFeature("mat_peptide", false);
      } else if (type.matches("^http://gbol.life/0.1/Variation$")) {
        feature = featureFactory.createFeature("variation", false);
      } else if (type.matches("^http://gbol.life/0.1/5'utr$")) {
        feature = featureFactory.createFeature("utr5", false);
      } else if (type.matches("^http://gbol.life/0.1/3'utr$")) {
        feature = featureFactory.createFeature("utr3", false);
      } else if (type.matches("^http://gbol.life/0.1/StemLoop$")) {
        feature = featureFactory.createFeature("stem_loop", false);
      } else if (type.matches("^http://gbol.life/0.1/Intron$")) {
        feature = featureFactory.createFeature("intron", false);
      } else if (type.matches("^http://gbol.life/0.1/Operon$")) {
        feature = featureFactory.createFeature("operon", false);
      } else if (type.matches("^http://gbol.life/0.1/Unsure$")) {
        feature = featureFactory.createFeature("unsure", false);
      } else if (type.matches("^http://gbol.life/0.1/MiscRecomb$")) {
        feature = featureFactory.createFeature("misc_recomb", false);
      } else if (type.matches("^http://gbol.life/0.1/Orit$")) {
        feature = featureFactory.createFeature("orit", false);
      } else if (type.matches("^http://gbol.life/0.1/PrimerBind$")) {
        feature = featureFactory.createFeature("primer_bind", false);
      } else if (type.matches("^http://gbol.life/0.1/MiscStructure$")) {
        feature = featureFactory.createFeature("misc_structure", false);
      } else if (type.matches("^http://gbol.life/0.1/ModifiedBase$")) {
        feature = featureFactory.createFeature("modified_base", false);
      } else {
        App.logger.warn("Not captured: " + type);
        break;
      }

      String strandIRI = item.getIRI("strand");
      boolean strand = false;
      if (strandIRI != null && strandIRI.contains("ReverseStrandPosition"))
        strand = true;

      int begin = item.getLitInt("begin");
      int end = item.getLitInt("end");

      feature = setLocation(feature, begin, end, strand, locationFactory);
      if (item.getLitString("gene") != null)
        feature.addQualifier("gene", item.getLitString("gene"));
      if (item.getLitString("note") != null)
        feature.addQualifier("note", item.getLitString("note"));
      if (item.getLitString("mobele") != null)
        feature.addQualifier("mobile_element", item.getLitString("mobele"));
    }
    entry.addFeature(feature);
    return entry;
  }

  private static Entry Gene(RDFSimpleCon RDFResource, HDT hdt, String featureIRI, Entry entry)
      throws Exception {
    // Get position
    FeatureFactory featureFactory = new FeatureFactory();
    QualifierFactory qualifierFactory = new QualifierFactory();
    LocationFactory locationFactory = new LocationFactory();
    Feature feature = null;
    for (ResultLine item : RDFResource.runQuery(hdt, "getGenePositions.txt", featureIRI)) {
      // String type = item.getIRI("type");
      int begin = item.getLitInt("begin");
      int end = item.getLitInt("end");

      // TODO Strand is not always present?
      String strandIRI = item.getIRI("strand");

      // TODO only forward is being printed
      boolean strand = false;
      if (strandIRI != null && strandIRI.contains("ReverseStrandPosition"))
        strand = true;

      feature = featureFactory.createFeature("gene", false); // ("GENE");
      // feature.getLocations().addLocation(
      // locationFactory.createLocalRange(Long.valueOf(left),
      // Long.valueOf(right), strand));
      feature = setLocation(feature, begin, end, strand, locationFactory);
      if (item.getLitString("gene") != null)
        feature.addQualifier("gene", item.getLitString("gene"));
      if (item.getLitString("note") != null) {
        for (ResultLine note : RDFResource.runQuery(hdt, "getNotes.txt", featureIRI)) {
          String i = note.getLitString("note");
          feature.addQualifier("note", i);
        }
      }
      if (item.getLitString("locus_tag") != null)
        feature.addQualifier(
            qualifierFactory.createQualifier("locus_tag", item.getLitString("locus_tag")));

      entry.addFeature(feature);

      // Check if it has a transcript
      entry = Transcript(RDFResource, hdt, featureIRI, entry, featureFactory, locationFactory);
      entry = Misc_rna(entry, featureFactory, locationFactory, featureIRI, RDFResource, hdt, begin,
          end);

    }
    return entry;
  }

  private static Entry Transcript(RDFSimpleCon RDFResource, HDT hdt, String featureIRI, Entry entry,
      FeatureFactory featureFactory, LocationFactory locationFactory) throws Exception {
    for (ResultLine item : RDFResource.runQuery(hdt, "getTranscript.txt", featureIRI)) {
      String transcriptIRI = item.getIRI("transcript");
      entry = Mrna(entry, featureFactory, locationFactory, transcriptIRI, RDFResource, hdt);
      entry = CDS(entry, featureFactory, locationFactory, transcriptIRI, RDFResource, hdt);
    }
    return entry;
  }

  private static Entry Misc_rna(Entry entry, FeatureFactory featureFactory,
      LocationFactory locationFactory, String featureIRI, RDFSimpleCon RDFResource, HDT hdt,
      int begin, int end) throws Exception {
    for (ResultLine item : RDFResource.runQuery(hdt, "getMiscRna.txt", featureIRI)) {
      // TODO in embl file, it will say 'order' instead of 'join'
      Feature feature = featureFactory.createFeature("misc_RNA", false);
      // TODO this is not correct.. there are more locations
      feature.getLocations().addLocation(
          locationFactory.createLocalRange(Long.valueOf(begin), Long.valueOf(end), false));
      if (item.getLitString("gene") != null)
        feature.addQualifier("gene", item.getLitString("gene"));
      if (item.getIRI("misc_rna") != null) {
        String noteIRI = item.getIRI("misc_rna");
        App.logger.debug("NOTEIRI: " + noteIRI);
        for (ResultLine note : RDFResource.runQuery(hdt, "getNotes.txt", noteIRI)) {
          String i = note.getLitString("note");
          feature.addQualifier("note", i);
        }
      }
      feature =
          getMiscRnaXrefs(feature, featureFactory, locationFactory, RDFResource, hdt, featureIRI);
      entry.addFeature(feature);
    }
    return entry;

  }

  private static Feature getMiscRnaXrefs(Feature feature, FeatureFactory featureFactory,
      LocationFactory locationFactory, RDFSimpleCon RDFResource, HDT hdt, String featureIRI)
      throws Exception {
    for (ResultLine xrefs : RDFResource.runQuery(hdt, "getMiscRnaXrefs.txt", featureIRI)) {
      String xrefIRI = xrefs.getIRI("xrefs");
      for (ResultLine xref : RDFResource.runQuery(hdt, "getMiscRnaXref.txt", xrefIRI)) {
        feature.addQualifier("db_xref", xref.getLitString("base") + ":" + xref.getLitString("id"));
      }
    }
    return feature;
  }

  private static Entry CDS(Entry entry, FeatureFactory featureFactory,
      LocationFactory locationFactory, String transcriptIRI, RDFSimpleCon RDFResource, HDT hdt)
      throws Exception {
    for (ResultLine item : RDFResource.runQuery(hdt, "getCDS.txt", transcriptIRI)) {
      CdsFeature cds = featureFactory.createCdsFeature();
      // Setting exon locations for each CDS
      cds = CdsExonLocations(locationFactory, cds, RDFResource, transcriptIRI, hdt);

      // Setting gene name
      if (item.getLitString("gene") != null)
        cds.addQualifier("gene", item.getLitString("gene"));

      // Setting translation
      cds.setTranslation(item.getLitString("sequence"));
      entry.addFeature(cds);
      String xrefIRI = item.getIRI("feature");
      for (ResultLine xref : RDFResource.runQuery(hdt, "getXrefs.txt", xrefIRI)) {
        if (xref.getLitString("db") != null) {
          XRef xRef = new XRef();
          xRef.setDatabase(xref.getLitString("db"));
          xRef.setPrimaryAccession(xref.getLitString("pa"));
          cds.addXRef(xRef);
        }
        /*
         * XREFS FROM THE PROTEIN LEVEL
         */
        if (xref.getLitString("db2") != null) {
          XRef xRef = new XRef();
          xRef.setDatabase(xref.getLitString("db2"));
          xRef.setPrimaryAccession(xref.getLitString("pa2"));
          cds.addXRef(xRef);
        }
      }

      if (item.getLitString("pid") != null)
        cds.addQualifier("protein_id", item.getLitString("pid"));

      for (ResultLine note : RDFResource.runQuery(hdt, "getNotes.txt", item.getIRI("feature"))) {
        String i = note.getLitString("note");
        cds.addQualifier("note", i);
      }

      // Setting codonstart position
      if (item.getLitString("cs") != null)
        cds.setStartCodon(item.getLitInt("cs"));

      // Setting locus tag
      if (item.getLitString("lt") != null)
        cds.addQualifier("locus_tag", item.getLitString("lt"));

      // Setting product
      if (item.getLitString("product") != null)
        cds.addQualifier("product", item.getLitString("product"));
      else
        cds.addQualifier("product", "hypothetical protein");

      // Setting translation table
      if (item.getLitString("transtable") != null)
        cds.setTranslationTable(item.getLitInt("transtable"));

      // Setting EC number
      if (item.getLitString("ecnum") != null)
        cds.addQualifier("EC_number", item.getLitString("ecnum"));

      return entry;
    }
    return entry;
  }

  private static CdsFeature CdsExonLocations(LocationFactory locationFactory, CdsFeature cds,
      RDFSimpleCon RDFResource, String transcriptIRI, HDT hdt) throws Exception {
    for (ResultLine item : RDFResource.runQuery(hdt, "getExon.txt", transcriptIRI)) {
      String exonIRI = item.getIRI("exon");
      // TODO Check if exactmatch type or fuzzy position type...
      for (ResultLine exon : RDFResource.runQuery(hdt, "getExonLocations.txt", exonIRI)) {
        int begin = exon.getLitInt("begin");
        int end = exon.getLitInt("end");

        boolean strand = false;
        // if (exon.getIRI("strand") != null
        // && exon.getIRI("strand").contains("ReverseStrandPosition")) {
        // strand = true;
        // end = exon.getLitInt("begin");
        // begin = exon.getLitInt("end");
        // }
        cds = setLocation(cds, begin, end, strand, locationFactory);
      }
    }
    return cds;
  }

  private static Entry Mrna(Entry entry, FeatureFactory featureFactory,
      LocationFactory locationFactory, String transcriptIRI, RDFSimpleCon RDFResource, HDT hdt)
      throws Exception {
    for (ResultLine item : RDFResource.runQuery(hdt, "getMrna.txt", transcriptIRI)) {
      // TODO in embl file, it will say 'order' instead of 'join'
      Feature feature = featureFactory.createFeature("mRNA", false);
      feature = MrnaExonLocations(locationFactory, feature, RDFResource, transcriptIRI, hdt);
      if (item.getLitString("gene") != null)
        feature.addQualifier("gene", item.getLitString("gene"));

      if (item.getLitString("note") != null) {
        for (ResultLine note : RDFResource.runQuery(hdt, "getNotes.txt", transcriptIRI)) {
          String i = note.getLitString("note");
          feature.addQualifier("note", i);
        }
      }
      entry.addFeature(feature);
    }
    return entry;
  }

  private static Feature MrnaExonLocations(LocationFactory locationFactory, Feature feature,
      RDFSimpleCon RDFResource, String transcriptIRI, HDT hdt) throws Exception {
    for (ResultLine item : RDFResource.runQuery(hdt, "getExon.txt", transcriptIRI)) {
      String exonIRI = item.getIRI("exon");
      // TODO Check if exactmatch type or fuzzy position type...
      for (ResultLine exon : RDFResource.runQuery(hdt, "getExonLocations.txt", exonIRI)) {
        int begin = exon.getLitInt("begin");
        int end = exon.getLitInt("end");

        boolean strand = false;
        // if (exon.getIRI("strand") != null
        // && exon.getIRI("strand").contains("ReverseStrandPosition")) {
        // strand = true;
        // end = exon.getLitInt("begin");
        // begin = exon.getLitInt("end");
        // }
        feature = setLocation(feature, begin, end, strand, locationFactory);
      }
    }
    return feature;
  }

  private static Feature setLocation(Feature feature, int begin, int end, boolean strand,
      LocationFactory locationFactory) {
    // left and right... when positioning does not correspond to strand
    int left;
    int right;
    if (end > begin) {
      left = begin;
      right = end;
    } else {
      left = end;
      right = begin;
    }
    feature.getLocations().addLocation(
        locationFactory.createLocalRange(Long.valueOf(left), Long.valueOf(right), strand));
    return feature;
  }

  private static CdsFeature setLocation(CdsFeature cds, int begin, int end, boolean strand,
      LocationFactory locationFactory) {
    // left and right... when positioning does not correspond to strand
    int left;
    int right;
    if (end > begin) {
      left = begin;
      right = end;
    } else {
      left = end;
      right = begin;
    }

    cds.getLocations().addLocation(
        locationFactory.createLocalRange(Long.valueOf(left), Long.valueOf(right), strand));

    return cds;
  }
}
