package nl.wur.ssb.conversion.fasta2rdf;

import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.RNASequence;
import org.biojava.nbio.core.sequence.transcription.Frame;
import org.biojava.nbio.core.sequence.transcription.TranscriptionEngine;

public class Translate {

  public static String make(String sequence, int codon, boolean stopcodon) throws Exception {

    TranscriptionEngine.Builder b = new TranscriptionEngine.Builder();
    b.table(codon);
    TranscriptionEngine engine = b.build();

    DNASequence DNAseq = new DNASequence(sequence);
    RNASequence rna = DNAseq.getRNASequence(Frame.ONE);

    ProteinSequence protein = rna.getProteinSequence(engine);

    String proteinSeq = protein.getSequenceAsString().replaceFirst("\\*$", "");
    if (proteinSeq.contains("*")) {
      if (stopcodon) {
        System.out.println(
            "Protein translation contains a stop codon, are you sure you are using the right codon table or input sequence?\n"
                + DNAseq + "\n" + protein);
      } else {
        throw new Exception(
            "Protein translation contains a stop codon, are you sure you are using the right codon table or input sequence?\n"
                + DNAseq + "\n" + protein);
      }
    }
    return proteinSeq;
  }
  
}
