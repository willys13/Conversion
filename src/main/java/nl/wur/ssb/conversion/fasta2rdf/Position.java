package nl.wur.ssb.conversion.fasta2rdf;

import life.gbol.domain.NASequence;
import life.gbol.domain.Region;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

public class Position {


  public static life.gbol.domain.Region Region(Domain domain, NASequence naobject, long beginpos,
      long endpos, String type) {

    // Creating the gene object locations of begin and end...

    /*
     * Should I create a region with or without the type?
     */

    // Domain.Region region = domain.make(life.gbol.domain.Region.class,
    // dnaobject.getIRI() + "/Region/" + type + "/" + beginpos + "-" + endpos);

    life.gbol.domain.Region region = domain.make(life.gbol.domain.Region.class,
        naobject.getResource().getURI() + "/region/" + beginpos + "-" + endpos);

    life.gbol.domain.ExactPosition setbegin = domain.make(life.gbol.domain.ExactPosition.class,
        naobject.getResource().getURI() + "/exactbegin/" + beginpos);

    life.gbol.domain.ExactPosition setend = domain.make(life.gbol.domain.ExactPosition.class,
        naobject.getResource().getURI() + "/exactend/" + endpos);

    setbegin.setPosition(beginpos);

    region.setBegin(setbegin);

    setend.setPosition(endpos);
    region.setEnd(setend);

    return region;
  }

  public static Region RegionLeftPartial(Domain domain, NASequence naobject, long beginpos,
      long endpos, String type, NASequence dnaobject) {
    /*
     * This module creates the Left partial entry complement(join(<85..350...
     * 
     * But currently you cannot set a location to a fuzzy position...
     */

    life.gbol.domain.Region region = domain.make(life.gbol.domain.Region.class,
        dnaobject.getResource().getURI() + "/region/" + beginpos + "-" + endpos);

    life.gbol.domain.BeforePosition setbegin = domain.make(life.gbol.domain.BeforePosition.class,
        dnaobject.getResource().getURI() + "/beforebegin/" + beginpos);

    life.gbol.domain.ExactPosition setend = domain.make(life.gbol.domain.ExactPosition.class,
        dnaobject.getResource().getURI() + "/exactend/" + endpos);

    setbegin.setPosition(beginpos);

    region.setBegin(setbegin);

    setend.setPosition(endpos);
    region.setEnd(setend);

    return region;
  }

  public static Region RegionRightPartial(Domain domain, NASequence naobject, long beginpos,
      long endpos, String type, NASequence dnaobject) {
    /*
     * This module creates the right partial entry ... FT 1160..>1261))
     * 
     * But currently you cannot set a location to a fuzzy position...
     */


    life.gbol.domain.Region region = domain.make(life.gbol.domain.Region.class,
        dnaobject.getResource().getURI() + "/region/" + beginpos + "-" + endpos);

    life.gbol.domain.ExactPosition setbegin = domain.make(life.gbol.domain.ExactPosition.class,
        dnaobject.getResource().getURI() + "/exactbegin/" + beginpos);

    life.gbol.domain.AfterPosition setend = domain.make(life.gbol.domain.AfterPosition.class,
        dnaobject.getResource().getURI() + "/afterend/" + endpos);

    setbegin.setPosition(beginpos);

    region.setBegin(setbegin);

    setend.setPosition(endpos);

    region.setEnd(setend);

    return region;
  }

}
