package nl.wur.ssb.conversion.fasta2rdf;

import life.gbol.domain.Note;
import life.gbol.domain.Sample;
import life.gbol.domain.StrandType;
import life.gbol.domain.Topology;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.conversion.App;
import uk.ac.ebi.embl.api.entry.Entry;

public class Genome {

  public static void build(String topologytype, Domain domain, Sample sample, String dnatype,
      Entry entry, String sequenceIRI) throws Exception {

    String header = entry.getComment().getText();

    String sequence = new String(entry.getSequence().getSequenceByte()).toUpperCase();
    String sac = entry.getSubmitterAccession();

    Topology topology;

    // TODO allow user to change StrandType
    if (topologytype == "linear")
      topology = life.gbol.domain.Topology.Linear;
    else if (topologytype == "circular")
      topology = life.gbol.domain.Topology.Circular;
    else
      throw new Exception(
          "Topology not given please indicate whether topology is linear or circular");

    Note note = Fasta.setHeaderNote(domain, sequenceIRI, header);

    if (dnatype.contains("scaffold")) {
      life.gbol.domain.Scaffold CM = domain.make(life.gbol.domain.Scaffold.class, sequenceIRI);
      CM.setSequence(sequence);
      CM.setSample(sample);
      CM.addNote(note);
      CM.setLength(entry.getSequence().getLength());
      CM.setTopology(topology);
      CM.addAccession(sac);
      StrandType type = life.gbol.domain.StrandType.DoubleStrandedDNA;
      CM.setStrandType(type);
      CM.setSha384(Generic.checksum(sequence, "SHA-384"));
    } else if (dnatype.contains("chromosome")) {
      life.gbol.domain.Chromosome CM = domain.make(life.gbol.domain.Chromosome.class, sequenceIRI);
      CM.setSequence(sequence);
      CM.setSample(sample);
      CM.addNote(note);
      CM.setLength(entry.getSequence().getLength());
      CM.addAccession(sac);
      CM.setTopology(life.gbol.domain.Topology.Linear);
      StrandType type = life.gbol.domain.StrandType.DoubleStrandedDNA;
      CM.setStrandType(type);
      CM.setSha384(Generic.checksum(sequence, "SHA-384"));
    } else {
      life.gbol.domain.Contig CM = domain.make(life.gbol.domain.Contig.class, sequenceIRI);
      CM.setSequence(sequence);
      CM.setSample(sample);
      CM.addNote(note);
      CM.addAccession(sac);
      CM.setLength(entry.getSequence().getLength());
      CM.setTopology(life.gbol.domain.Topology.Linear);
      StrandType type = life.gbol.domain.StrandType.DoubleStrandedDNA;
      CM.setStrandType(type);
      CM.setSha384(Generic.checksum(sequence, "SHA-384"));
      if (!dnatype.matches("contig"))
        App.logger
            .warn("contig, scaffold or chromosome option not given... using contig as default");
    }
  }

  public static void build(String topologytype, Domain domain, Sample sample, String dnatype,
      Entry entry) throws Exception {
    String sac = entry.getComment().getText().split(" ")[0].replaceAll("\\|","/");
    App.logger.debug("Submitter Accession: "+sac);
    String sequenceIRI = sample.getResource().getURI() + "/" + sac;
    build(topologytype, domain, sample, dnatype, entry, sequenceIRI);
  }
}
