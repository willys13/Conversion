package nl.wur.ssb.conversion.fasta2rdf;

import life.gbol.domain.Region;
import life.gbol.domain.Sample;
import life.gbol.domain.StrandPosition;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.conversion.options.CommandOptionsFasta2RDF;
import uk.ac.ebi.embl.api.entry.Entry;

public class Mrna {

  public static void build(Domain domain, Sample sample, CommandOptionsFasta2RDF arguments,
      Entry entry) throws Exception {

    String header = entry.getComment().getText();
    String sequence = new String(entry.getSequence().getSequenceByte()).toUpperCase();
    String sac = entry.getSubmitterAccession();

    String md5 = Generic.checksum(sequence, "MD5");


    /*
     * TODO Right type? CodingTranscript... CodingSequence?
     */

    life.gbol.domain.mRNA mrna =
        domain.make(life.gbol.domain.mRNA.class, sample.getResource().getURI() + "/mrna/" + md5);

    mrna.setSequence(sequence);
    mrna.setSample(sample);
    mrna.addNote(Fasta.setHeaderNote(domain, mrna.getResource().getURI(), header));
    mrna.addAccession(sac);
    life.gbol.domain.CDS cds =
        domain.make(life.gbol.domain.CDS.class, sample.getResource().getURI() + "/cds/" + md5);

    Region region = Position.Region(domain, mrna, 1, sequence.length(), "Exon");

    region.setStrand(StrandPosition.ForwardStrandPosition);

    cds.setLocation(region);

    /*
     * Translating the sequence to a protein...
     */
    if (arguments.translate) {
      String AAsequence = Translate.make(sequence, arguments.codon, arguments.stopcodon);
      String sha384 = Generic.checksum(AAsequence, "SHA-384");

      life.gbol.domain.Protein protein =
          domain.make(life.gbol.domain.Protein.class, sample.getResource().getURI() + sha384);
      protein.setSequence(AAsequence);
      protein.setSha384(sha384);
      cds.setProtein(protein);
    }

  }

}
