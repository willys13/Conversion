package nl.wur.ssb.conversion.fasta2rdf;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.conversion.App;
import nl.wur.ssb.conversion.options.CommandOptionsFasta2RDF;

public class Protein {

  public static void build(Domain domain, CommandOptionsFasta2RDF arguments)
      throws Exception {

    life.gbol.domain.Sample sample =
        domain.make(life.gbol.domain.Sample.class, "http://gbol.life/0.1/" + arguments.identifier);

    Scanner scanner = new Scanner(arguments.input);

    Map<String, String> fasta = new HashMap<String, String>();
    String header = null;
    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();
      if (line.startsWith(">")) {
        header = line;
        fasta.put(header, "");
      } else {
        fasta.put(header, fasta.get(header) + line);
      }
    }

    scanner.close();

    for (String key : fasta.keySet()) {
      String sequence = fasta.get(key).replaceAll("\\*$", "");
      if (sequence.contains("*"))
        App.logger.warn("* detected...");
      String sha384 = Generic.checksum(sequence, "SHA-384");

      life.gbol.domain.Protein protein =
          domain.make(life.gbol.domain.Protein.class, "http://gbol.life/0.1/protein/" + sha384);
      protein.setSequence(sequence);
      protein.setSample(sample);
      protein.setSha384(sha384);
      
    }
  }
}
