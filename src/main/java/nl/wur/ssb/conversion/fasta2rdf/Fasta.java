package nl.wur.ssb.conversion.fasta2rdf;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collection;
import life.gbol.domain.AnnotationLinkSet;
import life.gbol.domain.AnnotationResult;
import life.gbol.domain.Note;
import life.gbol.domain.Provenance;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.conversion.App;
import nl.wur.ssb.conversion.options.CommandOptionsFasta2RDF;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.validation.Origin;
import uk.ac.ebi.embl.api.validation.ValidationMessage;
import uk.ac.ebi.embl.api.validation.ValidationResult;
import uk.ac.ebi.embl.fasta.reader.FastaFileReader;
import uk.ac.ebi.embl.fasta.reader.FastaLineReader;

public class Fasta {

  public static void app(Domain domain, CommandOptionsFasta2RDF arguments) throws Exception {

    if (arguments.protein) {
      // Treat me as a protein entry
      App.logger.info("Parsing protein fasta file");
      nl.wur.ssb.conversion.fasta2rdf.Protein.build(domain, arguments);
    } else {
      // Fasta reader...
      life.gbol.domain.Sample sample = domain.make(life.gbol.domain.Sample.class,
          "http://gbol.life/0.1/" + arguments.identifier);

      BufferedReader bufreader = new BufferedReader(new FileReader(arguments.input));
      FastaFileReader fastaReader = new FastaFileReader(new FastaLineReader(bufreader));
      ValidationResult result = fastaReader.read();
      Collection<ValidationMessage<Origin>> messages = result.getMessages();
      for (ValidationMessage<Origin> message : messages) {
        App.logger.info(message.getMessage());
      }

      while (fastaReader.isEntry()) {
        Entry entry = fastaReader.getEntry();
        App.logger.debug(entry.getComment().getText());
        if (arguments.gene) {
          // Treat me as a gene entry
          nl.wur.ssb.conversion.fasta2rdf.Mrna.build(domain, sample, arguments, entry);
        } else if (arguments.genome) {
          String dnatype = null;
          // Treat me as a genome
          if (arguments.chromosome) {
            dnatype = "chromosome";
          } else if (arguments.scaffold) {
            dnatype = "scaffold";
          } else {
            dnatype = "contig";
          }
          nl.wur.ssb.conversion.fasta2rdf.Genome.build(arguments.topology, domain, sample, dnatype,
              entry);
          App.logger.debug("Size: " + domain.getRDFSimpleCon().getModel().size());
        } else {
          throw new Exception("Missing parameter, -gene, -protein or -genome");
        }
        fastaReader.read();
      }
    }
  }

  public static Note setHeaderNote(Domain domain, String sequenceIRI, String content) {
    Note note = domain.make(life.gbol.domain.Note.class, sequenceIRI + "Note");
    note.setText(content);
    Provenance noteProv = domain.make(Provenance.class, note.getResource().getURI() + "/noteprov");
    AnnotationResult annotResult = domain.make(AnnotationLinkSet.class,
        note.getResource().getURI() + "/noteprov/" + "AnnotationResult");
    noteProv.setOrigin(annotResult);
    note.setText(content);
    note.setProvenance(noteProv);
    return note;

  }
}
