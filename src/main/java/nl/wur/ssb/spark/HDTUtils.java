package nl.wur.ssb.spark;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaRDDLike;
import org.apache.spark.input.PortableDataStream;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdtjena.HDTGraph;
import scala.Tuple2;

public class HDTUtils {

  public static List<File> inputStream2Sequences(Tuple2<String, PortableDataStream> bytes)
      throws Exception {

    org.rdfhdt.hdt.hdt.HDT hdt = HDTManager.loadHDT(new BufferedInputStream(bytes._2.open()), null);
    Domain domain = new Domain("");

    Iterator<ResultLine> resultsIter = domain.getRDFSimpleCon().runQuery(hdt, "getGenomes.txt")
        .iterator();

    List<String> sequences = new ArrayList();

    // Creates an array of sequences
    while (resultsIter.hasNext()) {
      ResultLine resultLine = resultsIter.next();
      String header = resultLine.getIRI("contig");
      String type = resultLine.getIRI("type");
      String seq = resultLine.getLitString("sequence");
      String res = type +"#_#" + header  + "\n" + seq;
      sequences.add(res);
    }

    JavaRDDLike<String, JavaRDD<String>> sequencesRDD = Spark.sc
        .parallelize(sequences);

    List<File> resultRDFFiles = sequencesRDD.flatMap(seq -> prepareForAragorn(seq)).collect();


    // Merge all the result files into a new file?
    File finalRDFFile = File.createTempFile(bytes._1+"___###___", ".result");


    HDTGraph graph = new HDTGraph(hdt);
    Model model = ModelFactory.createModelForGraph(graph);
    OutputStream os = new FileOutputStream(finalRDFFile);
    model.write(os, "ntriples");
    model.close();


    for (File rdfFile : resultRDFFiles) {
      Scanner scanner = new Scanner(rdfFile);
      while ( scanner.hasNextLine() ) {
        String line = scanner.nextLine() + "\n";
        os.write(line.getBytes());
      }
    }
    os.close();

    domain.close();

    List<File> finalRDFFiles = new ArrayList();
    finalRDFFiles.add(finalRDFFile);
    return finalRDFFiles;
  }

  /*
   * TODO When running on my laptop with a few cores and giving 10+ files two files get
   * submitted but here they wait for eachother as no threads are most likely available?
   */

  private static Iterator<File> prepareForAragorn(String sequence) throws Exception {
    File tempSeqFile = File.createTempFile("temp-file-name", ".input");
    File resultFile = File.createTempFile("temp-file-name", ".output");

    // Writing the sequence to a tempFile
    PrintWriter writer = new PrintWriter(tempSeqFile.getAbsolutePath());
    writer.write(sequence);
    writer.close();

    String[] args = {"-input",tempSeqFile.getAbsolutePath(), "-o",resultFile.getAbsolutePath()};

//    new nl.wur.ssb.aragorn.Aragorn(args);

    // Cleaning up
    tempSeqFile.delete();

    // Getting the triple files
    List<File> triples = new ArrayList();
    triples.add(resultFile);

    return triples.iterator();
  }
}