package nl.wur.ssb.spark;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.beust.jcommander.Parameters;
import java.io.File;
import java.time.LocalDateTime;
import org.apache.commons.lang.StringUtils;


@Parameters(separators = " ", commandDescription = "Available options: ")
public class CommandOptionsSpark {

  @Parameter(names = "--help")
  public boolean help;

  @Parameter(names = {"-i", "-input"}, description = "Input file, usage of regex is allowed to get more files", required = true)
  public File input;

//  @Parameter(names = {"-o", "-output"}, description = "Output file", required = true)
//  public File output;

  public LocalDateTime starttime = LocalDateTime.now();
  public String commandLine;
  public final String toolName = "SAPP - File Conversion";
  public final String repository = "http://gitlab.com/sapp/conversion/conversion";

  @Parameter(names = "-debug", description = "Debug mode")
  public boolean debug = false;

  public final String toolVersion = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsSpark.class)[0];
  public final String sappcommit = nl.wur.ssb.SappGeneric.Generic.getVersion(CommandOptionsSpark.class)[1];

  @Parameter(names = { "-embl2rdf" }, description = "EMBL to RDF conversion")
  public boolean embl = false;

  @Parameter(names = { "-spark" }, description = "SPARK enabled")
  public boolean spark = false;

//  @Parameter(names = { "-f", "-format" }, description = "output in ttl or hdt format")
//  public String format = "hdt";

//  public String identifier;

  public int locus = 1;

  @Parameter(names = { "-jobs",
      "-j" }, description = "Number of parallel jobs to run")
  public int jobs = 2;

//  @Parameter(names = { "-agent" }, description = "IRI of the agent running the tool")
//  public String userAgentIri;

//  @Parameter(names = { "-if",
//      "-importFiles" }, description = "Any ttl file that needs to include, aka a file describing the agent")
//  public List<File> importFiles = new ArrayList<File>();


//  @Parameter(names = {"-codon"},
//      description = "Codon table to use when transl_table AND protein sequences are not availale")
//  public int codon;

  public CommandOptionsSpark(String args[]) throws Exception {
    this.commandLine = StringUtils.join(args, " ");

//    if (this.format != "ttl" && this.format != "hdt")
//      throw new Exception("Format is neither hdt or ttl");

    try {
      new JCommander(this, args);
      if (this.help == true) {
        new JCommander(this).usage();
        System.exit(0);
      }
    } catch (ParameterException pe) {
      System.err.println(pe.getMessage());
      new JCommander(this).usage();
      System.err.println("  * required parameter");
      System.exit(1);
    }
  }
}
