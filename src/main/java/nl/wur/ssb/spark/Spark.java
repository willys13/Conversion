package nl.wur.ssb.spark;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import nl.wur.ssb.conversion.App;
import org.apache.hadoop.mapreduce.lib.input.InvalidInputException;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.input.PortableDataStream;
import scala.Tuple2;

public class Spark {

  final static Logger logger = Logger.getLogger(Spark.class);
  static JavaSparkContext sc;

  public static void main(String[] args) throws Exception {

    CommandOptionsSpark arguments = new CommandOptionsSpark(args);

    SparkConf sConf = new SparkConf();

    sConf.setMaster("local["+arguments.jobs+"]");
    sConf.setAppName("SAPP Conversion");
    sConf.set("spark.executor.memory", "5g");
    sConf.set("","");

    sc = new JavaSparkContext(sConf);

    logger.info("inPath: " + arguments.input);

    // Create RDD where each element is a tuple: (filename, bytestream)
    try {
      JavaPairRDD<String, PortableDataStream> bytesRDD = sc.binaryFiles("file://"+ arguments.input.getAbsolutePath());
      bytesRDD.isEmpty();
      // This is now calling the function
      bytesRDD.flatMap(bytes -> prepare4Conversion(bytes)).count();
    } catch (Exception e) {
      App.logger.warn("No input files found... Is your input regex ok?");
      App.logger.warn(e);
      return;
    }
  }

  /**
   *
   * @param bytes
   * @param <U>
   * @return
   * @throws Exception
   */
  private static <U> Iterator<String> prepare4Conversion(Tuple2<String, PortableDataStream> bytes)
      throws Exception {

    String name = new File(bytes._1).getName();
    name = name.substring(0, name.indexOf("."));

    // Step 1, convert each file as a whole, break down can be done later...
    String args = "-embl2rdf -input " + bytes._1 + " -output "+bytes._1 + ".hdt -u "+name;
    if (new File(bytes._1.split("file:")[1] +".hdt").exists()) {
//      App.logger.warn("Output file already exists");
    } else {
      App.logger.info(args);
      new FlatfileSpark(bytes, args.split(" "));
//      try {
//        Thread.sleep(5000);
//         new FlatfileSpark(bytes, args.split(" "));
//      } catch (Exception e) {
//        App.logger.warn("This one failed: "+bytes._1);
//        FileSystem fs = FileSystem.get(Spark.sc.hadoopConfiguration());
//        FSDataOutputStream output = fs.create(new Path(bytes._1 + ".hdt"));
//        output.write("I failed... :(".getBytes());
//        output.close();
//
//        Writer writer = new PrintWriter(bytes._1+".hdt");
//        writer.write("I failed... : (");
//        writer.close();
//      }
    }

    List<String> temp = new ArrayList<String>();
    temp.add("something");

    return temp.iterator();
  }
}
