package nl.wur.ssb.spark;

import static org.apache.jena.ext.com.google.common.hash.Hashing.sha384;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.SappGeneric.Generic;
import nl.wur.ssb.SappGeneric.Store;
import nl.wur.ssb.conversion.App;
import nl.wur.ssb.conversion.flatfile.FlatFileEntry;
import nl.wur.ssb.conversion.options.CommandOptionsFlatfile;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.jena.ext.com.google.common.hash.HashingInputStream;
import org.apache.log4j.Logger;
import org.apache.spark.input.PortableDataStream;
import org.rdfhdt.hdt.enums.RDFNotation;
import org.rdfhdt.hdt.hdt.HDTManager;
import org.rdfhdt.hdt.listener.ProgressListener;
import org.rdfhdt.hdt.options.HDTSpecification;
import scala.Tuple2;
import uk.ac.ebi.embl.api.entry.Entry;
import uk.ac.ebi.embl.api.validation.Origin;
import uk.ac.ebi.embl.api.validation.ValidationMessage;
import uk.ac.ebi.embl.api.validation.ValidationResult;
import uk.ac.ebi.embl.flatfile.reader.EntryReader;
import uk.ac.ebi.embl.flatfile.reader.embl.EmblEntryReader;
import uk.ac.ebi.embl.flatfile.reader.genbank.GenbankEntryReader;

public class FlatfileSpark
{
  private CommandOptionsFlatfile args;
  private Domain domain;
  // private ConversionImportProv importProv;

  public FlatfileSpark(Tuple2<String, PortableDataStream> inputStream, String[] args) throws Exception
  {
    this.domain = Store.createMemoryStore();

    this.args = new CommandOptionsFlatfile(args);

    for(File file : this.args.importFiles)
    {
      RDFSimpleCon con = new RDFSimpleCon("file://" + file.getAbsolutePath());
      this.domain.getRDFSimpleCon().addAll(con);
      con.close();
    }

    Logger logger = Generic.Logger(this.args.debug);
    File[] inputs = {this.args.input};
    HashingInputStream hashingInputStream = new HashingInputStream(sha384(), inputStream._2.open());
//    logger.info("TESTING: " +hashingInputStream.hash().toString());

    nl.wur.ssb.SappGeneric.ImportProv importProv = new nl.wur.ssb.SappGeneric.ImportProv(domain, inputs,  this.args.output, this.args.commandLine, this.args.toolName, this.args.toolVersion, this.args.repository, this.args.userAgentIri, this.args.starttime,
        LocalDateTime.now());

    BufferedReader reader;
    try
    {
      InputStream fileStream = inputStream._2().open();
      InputStream gzipStream = new GZIPInputStream(fileStream);
      Reader decoder = new InputStreamReader(gzipStream,"UTF-8");
      reader = new BufferedReader(decoder);
    }
    catch (ZipException e)
    {
      DataInputStream in = new DataInputStream(inputStream._2().open());
       reader = new BufferedReader(new InputStreamReader(in));
    }

    EntryReader emblReader = null;
    if(this.args.genbank)
      emblReader = new GenbankEntryReader(reader);
    else if(this.args.embl)
      emblReader = new EmblEntryReader(reader);
    else
      throw new Exception("define either genbank or embl");

    ValidationResult validations = emblReader.read();

    // IF validation is empty.. good? (so far always been the case)
    if (!validations.getMessages().isEmpty())
    {
      Iterator<ValidationMessage<Origin>> valiter = validations.getMessages().iterator();
      int count = 0;
      while (valiter.hasNext())
      {
        count++;
        ValidationMessage<Origin> validation = valiter.next();
        App.logger.warn(validation.getMessage());
        if (count > 2)
        {
          App.logger.warn("And the list continues...");
          break;
        }
      }
    }

    // Starting the parser
    while (emblReader.isEntry())
    {
      logger.debug("Reading new entry");
      Entry entry = emblReader.getEntry();
      logger.debug(entry.getFeatures().size());
      new FlatFileEntry(domain, this.args.identifier,entry,importProv);
      emblReader.read();
    }

    importProv.finished();

    // Saving using jena but HDT could maybe work as well..
    String outputFile = this.args.output.getAbsolutePath().split("file:")[1];
    App.logger.info("Saving to:.... "+outputFile);
    App.logger.debug("Triples: "+ domain.getRDFSimpleCon().getModel().size());
    FileSystem fs = FileSystem.get(Spark.sc.hadoopConfiguration());
    FSDataOutputStream output = fs.create(new Path(outputFile));
    domain.getRDFSimpleCon().getModel().write(output,"TURTLE");
    output.close();
    domain.getRDFSimpleCon().close();
    domain.close();

    String baseURI = "http://gbol.life/0.1/";
    String rdfInput = outputFile;
    String inputType = "turtle";
    org.rdfhdt.hdt.hdt.HDT hdt = HDTManager.generateHDT(rdfInput, baseURI, RDFNotation.parse(inputType), new HDTSpecification(), (ProgressListener)null);
    hdt.saveToHDT(outputFile, (ProgressListener)null);
    hdt.close();
  }

  public Domain getDomain()
  {
    return domain;
  }

  public CommandOptionsFlatfile getArgs()
  {
    return args;
  }

}
