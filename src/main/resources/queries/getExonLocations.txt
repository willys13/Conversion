SELECT DISTINCT ?begin ?end ?strand
WHERE {
    <%1$s> gbol:location ?location .
    ?location gbol:begin ?beginiri .
    ?beginiri gbol:position ?begin .
    ?location gbol:end ?endiri .
    ?endiri gbol:position ?end .
    ?location gbol:strand ?strand .
}