SELECT DISTINCT ?locus ?sequence
WHERE { 
	?cds gbol:locusTag ?locus .
	?cds gbol:protein ?protein .
	?protein gbol:sequence ?sequence .
}