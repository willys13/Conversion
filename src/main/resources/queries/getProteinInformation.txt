PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX gbol: <http://gbol.life#>
PREFIX ssb: <http://csb.wur.nl/genome/>
SELECT ?cds ?interpro ?idesc ?sdesc ?bdesc ?pdesc ?score (((xsd:float(?end)-xsd:float(?begin))/STRLEN(?sequence)*100) AS ?coverage) 
WHERE {
	OPTIONAL {
	?cds gbol:protein ?protein .
	?cds gbol:product ?pdesc .
	}
	?protein gbol:sequence ?sequence .
	?protein a gbol:Protein .
	OPTIONAL { 	
		?protein gbol:feature ?interpro .
		?interpro gbol:origin ?origin .
        FILTER(REGEX(STR(?origin),"interproscan","i"))
		?interpro gbol:location ?interpro_location .
        ?interpro_location gbol:begin ?domain_begin .
        ?domain_begin gbol:position ?begin .
        ?interpro_location gbol:end ?domain_end .        
		?domain_end gbol:position ?end .
	}
	OPTIONAL { ?protein gbol:feature ?interpro . ?interpro gbol:provenance ?prov . ?prov gbol:interpro_description ?idesc . }
	OPTIONAL { ?protein gbol:feature ?interpro . ?interpro gbol:provenance ?prov . ?prov gbol:signature_description ?sdesc . }
	OPTIONAL {
		?protein ssb:feature ?blast .
		?blast a ssb:Blast .
		?blast ssb:evalue ?score .
		?blast ssb:sstart ?domain_start .
		?blast ssb:send ?domain_stop .
		?blast ssb:subjectname ?id .
		BIND(REPLACE(?id, "\\[.+\\]", "", "i") AS ?bdesc)
		#BIND(REPLACE(?id2, ".*#", "", "i") AS ?idesc)
		}
	OPTIONAL { 
		?orf ssb:protein ?protein .
		?orf ssb:product ?product .
		?orf ssb:domain_start 1 .
		?orf ssb:domain_stop 2 .
		}
}ORDER BY ?protein DESC(?score) DESC (?coverage)