SELECT DISTINCT ?length ?taxid ?segment ?country ?organism ?host ?isolate ?scientificname ?lineage ?strain
WHERE { 
	OPTIONAL { <%1$s> gbol:length ?length . }
	OPTIONAL { <%1$s> gbol:taxid ?taxid . }
	OPTIONAL { <%1$s> gbol:segment ?segment .}
	OPTIONAL { <%1$s> gbol:country ?country .}
	OPTIONAL { <%1$s> gbol:organism ?organism .}
	OPTIONAL { <%1$s> gbol:host ?host .}
	OPTIONAL { <%1$s> gbol:isolate ?isolate .}
	OPTIONAL { <%1$s> gbol:scientificname ?scientificname . }
	OPTIONAL { <%1$s> gbol:lineage ?lineage . }
	OPTIONAL { <%1$s> gbol:strain ?strain . }
}